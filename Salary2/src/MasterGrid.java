import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.StyledEditorKit.BoldAction;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.JFormattedTextField.AbstractFormatter;

import com.f4.dao.MasterGridDAO;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.TrayIcon.MessageType;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class MasterGrid extends AbstractFormatter implements FocusListener, ActionListener {

	static Vector headers = new Vector();

	static Vector dataimp = new Vector();
	static JButton jbClick;
	static JFileChooser jChooser;
	static int tableWidth = 0; // set the tableWidth
	static int tableHeight = 0;

	ArrayList columnNames, columnNamesSalary;
	ArrayList data, dataSalary;
	int finalpt = 0;
	JFrame frame;
	JTable table;
	JComboBox monthcombo, yearcombo;
	JLabel selectmonth, imageLabelLogo,dateOfSalary;
	JButton savencalculate, EmpList, printReport, importExcel, sendMail;
	int errCheck = 0; // For validating successful calculations
	int Valyear, valMonth; // For Validating month of salary
	JCheckBox selectAll;
	JDatePickerImpl datePicker;
		
	String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
	String userid = "root";
	String password = "yungry";
	String monthofSalary;
	
	

	String colheaders[] = { "", "EMPLOYEE ID", "EMPLOYEE NAME", "PAYABLE DAYS",
			"LEAVES TAKEN", "LEAVES ADJUSTED", "PAID DAYS", "NET SALARY",
			"BASIC EARN", "HRA", "MEDICAL ALLOWANCE",
			"MEDICAL ALLOWANCE ARREAR", "CONVEYANCE", "SPECIAL ALLOWANCE",
			"OTHER ALLOWANCE", "EXECUTIVE ALLOWANCE",
			"EXECUTIVE ALLOWANCE ARREAR", "INTRESS SUBSIDY",
			"PERFORMANCE INCENTIVE", "LTA PAID", "RETAINERS BONUS",
			"PROVIDANT FUND", "PROVIDENT FUND ARREAR", "PROFESSIONAL TAX",
			"INCOME TAX", "D1 MISC", "D2 MISC", "CANTEEN ALLOWANCE",
			"SPORT CLUB", "TELEPHONE DEDUCTION","MEDICAL INSURANCE", "TOTAL INCOME",
			"TOTAL DEDUCTION" };

	String months[] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
			"SEP", "OCT", "NOV", "DEC" };

	String years[] = { "2014", "2015", "2016", "2017", "2018", "2019", "2020",
			"2021", "2022", "2023", "2024", "2025" };

	float payableDays = 0;
	float leavesTaken = 0;
	float leavesAdjusted = 0;
	float paidDays = 0, basicSalary, basicEarn, basicEarnArrear, hra,
			hraArrear, medicalAllowance, medicalAllowanceArrear, conveyance,
			specialAllowance, otherAllowance, executiveAllowance,
			executiveAllowanceArrear, intressSubsidy, performanceIncentive,
			ltaPaid, retainesBonus, pf, pfArrear, professionalTax, incometax,
			d1Misc, d2Misc, canteenDeduction, sportClub, telephoneDeduction,medicalInsurance;
	
	Vector columnNamesVector;
	Vector dataVector;

	public MasterGrid() {
		
		UtilDateModel model = new UtilDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel, this);
		
		jChooser = new JFileChooser();

		// Initilising Controls
		frame = new JFrame("MasterData");
		frame.setLayout(null);
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);

		monthcombo = new JComboBox(months);
		yearcombo = new JComboBox(years);
		selectmonth = new JLabel("Select Month of Salary");
		dateOfSalary = new JLabel("Select Date of Salary");
		savencalculate = new JButton("CALCULATE");
		savencalculate.setActionCommand("CALCULATE");
		EmpList = new JButton("EMPLOYEE LIST");
		EmpList.setActionCommand("LIST");
		printReport = new JButton("PRINT SALARY SLIPS");
		printReport.setActionCommand("PRINT");
		importExcel = new JButton("IMPORT LEAVES");
		importExcel.setActionCommand("IMPORT");
		sendMail = new JButton("SEND MAIL");
		sendMail.setActionCommand("MAIL");

		selectAll = new JCheckBox("All");
		selectAll.setBounds(35, 105, 60, 15);
		selectAll.addActionListener(this);
		selectAll.setActionCommand("CHECK");

		columnNames = new ArrayList();
		data = new ArrayList();

		Calendar calendar = Calendar.getInstance();
		int dd = 2;// calendar.get(Calendar.DAY_OF_MONTH);
		int mm = calendar.get(Calendar.MONTH) + 1;
		int yy = calendar.get(Calendar.YEAR);
		String date = yy + "-" + mm + "-" + dd;
		System.out.println("****" + date);
		
		int mmInactive=mm-1;
	/*	int YER=Integer.parseInt(yearcombo.getSelectedItem().toString());
		int MNTH=monthcombo.getSelectedIndex()+1;
*/
		String sql = "SELECT employee_code,employee_name FROM employee_details where date_of_joining < '"
				+ date + "' AND (status='Active' OR (status='Inactive' AND (YEAR(Inactive_date)='"+yy+"' AND MONTH(Inactive_date)='"+mmInactive+"' )))";

		try (Connection connection = DriverManager.getConnection(url, userid,
				password);
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();

			/*
			 * for (int i = 1; i <= columns; i++) { columnNames.add(
			 * md.getColumnName(i) ); }
			 */

			for (int i = 0; i < colheaders.length; i++) {
				columnNames.add(colheaders[i]);
			}

			while (rs.next()) {
				ArrayList row = new ArrayList(columns);
				row.add(false);
				for (int i = 1; i <= columns; i++) {
					row.add(rs.getObject(i));

				}

				data.add(row);

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		columnNamesVector = new Vector();
		dataVector = new Vector();

		for (int i = 0; i < data.size(); i++) {
			ArrayList subArray = (ArrayList) data.get(i);
			Vector subVector = new Vector();
			for (int j = 0; j < subArray.size(); j++) {
				subVector.add(subArray.get(j));
			}
			dataVector.add(subVector);
		}

		for (int i = 0; i < columnNames.size(); i++)
			columnNamesVector.add(columnNames.get(i));

		table = new JTable(dataVector, columnNamesVector) {
			public JTextField editor;
			private static final long serialVersionUID = 1L;

			public Class getColumnClass(int column) {

				for (int row = 0; row < getRowCount(); row++) {
					Object o = getValueAt(row, column);

					if (o != null) {
						if (column == 4 || column == 5) {
							return Float.class;
						} else {
							return o.getClass();
						}
					}
				}

				return Object.class;

			}

			
			public boolean isCellEditable(int row, int column) {
				if (column == 1 || column == 2 || column == 3)
					return false;
				return true;
			}

			
			public Component getTableCellEditorComponent(JTable table,
					Object value, boolean isSelected, int row, int column) {
				if (value instanceof String) {
					editor.setText((String) value);
				} else {
					editor.setText(null);
				}
				return editor;
			}

		};

		new JtableColor(table);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		JScrollPane scrollPane = new JScrollPane(table,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		// Set Bounds of controls
		scrollPane.setBounds(30, 100, 950, 400);
		selectmonth.setBounds(410, 50, 150, 20);
		yearcombo.setBounds(550, 50, 80, 20);
		dateOfSalary.setBounds(720, 50, 150, 20);
		datePicker.setBounds(845,50,120,20);
		monthcombo.setBounds(630, 50, 80, 20);
		savencalculate.setBounds(640, 600, 150, 40);
		EmpList.setBounds(820, 600, 150, 40);
		printReport.setBounds(450, 600, 160, 40);
		importExcel.setBounds(250, 600, 160, 40);
		sendMail.setBounds(50, 600, 160, 40);

		// code for logo & Co.Name
		imageLabelLogo = new JLabel(new ImageIcon("new/logo.png"));
		imageLabelLogo.setBounds(35, 0, 200, 100);
		frame.add(imageLabelLogo);

		// Panel for Button background color
		JPanel jp = new JPanel();
		jp.setBackground(Color.DARK_GRAY);
		jp.setBounds(30, 590, 950, 60);

		// Panel for background color
		JPanel jpmonth = new JPanel();
		jpmonth.setBackground(Color.GRAY);
		jpmonth.setBounds(400, 40, 580, 40);

		// Adding Controls to frame
		frame.add(selectAll);
		frame.add(scrollPane);
		frame.add(selectmonth);
		frame.add(monthcombo);
		frame.add(yearcombo);
		frame.add(savencalculate);
		frame.add(EmpList);
		frame.add(printReport);
		frame.add(importExcel);
		//frame.add(sendMail);
		frame.add(dateOfSalary);
		frame.add(datePicker);
		frame.add(jp);
		frame.add(jpmonth);
		
		

		// frame.add(jp);

		// Add action command to specific controls

		// Adding listeners to specific controls
		monthcombo.addFocusListener(this);
		yearcombo.addFocusListener(this);

		savencalculate.addActionListener(this);
		EmpList.addActionListener(this);
		printReport.addActionListener(this);
		importExcel.addActionListener(this);
		sendMail.addActionListener(this);

		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent event) {
						// ------------------------Code for validating
						// month-----------------//
						System.out.println("*************DONE***********");
						Calendar cal = Calendar.getInstance();
						int currentYear = cal.get(cal.YEAR);
						int currentMonth = cal.get(cal.MONTH);

					

					}

				});

		frame.setSize(1024, 730);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JOptionPane.showMessageDialog(null, "Please select month of salary !",
				"Warning..!!!", JOptionPane.WARNING_MESSAGE);

	}

	public static void main(String a[]) {
		new MasterGrid();
	}

	// Method to set payable days
	void setPayableDays(int i) {

		int count = table.getRowCount();

		for (int j = 0; j < count; j++) {
			table.setValueAt(i, j, 3);
		}

	}

	// This method find the number of rows on the grid &
	// call the fill salary method in loop accordingly
	void CalltofillSalary() {
		StringBuffer monthOfSal = new StringBuffer();
		String mon = monthcombo.getSelectedItem().toString();
		String yer = yearcombo.getSelectedItem().toString();
		monthOfSal.append(yer);
		monthOfSal.append("-");
		monthOfSal.append(mon);

		String monthofSalary2 = monthOfSal.toString();
		int i, count;
		count = table.getRowCount();
		for (i = 0; i < count; i++) {
			String empid = table.getValueAt(i, 1).toString();
			fillSalaryDetails(empid, monthofSalary2, i);
		}
	}

	// Method to set Professional Tax
	void setProfessionalTax(int value, int row) {

		// int cnt=table.getRowCount();
		// for(int j=0;j<cnt;j++){
		// finalpt=value;
		table.setValueAt(value, row, 23);
		// }

	}

	// this method checks the check mark on every row and call the methods
	// & fill the calculated salary details in the grid

	void setCheckedStatus() {

		int count = table.getRowCount();
		StringBuffer monthOfSal = new StringBuffer();
		String mon = monthcombo.getSelectedItem().toString();
		String yer = yearcombo.getSelectedItem().toString();
		monthOfSal.append(yer);
		monthOfSal.append("-");
		monthOfSal.append(mon);

		monthofSalary = monthOfSal.toString();
		// System.out.println(monthOfSal);

		for (int i = 0; i < count; i++) {

			boolean x = Boolean.parseBoolean(table.getValueAt(i, 0).toString());
			String empid = table.getValueAt(i, 1).toString();

			if (x) {

				try {
					int currentMonth = monthcombo.getSelectedIndex() + 1;
					int currentYear = Integer.parseInt(yearcombo
							.getSelectedItem().toString());

					System.out.println(currentMonth + "" + currentYear);
			
					
					String date = new MasterGridDAO().getJoiningDate(empid);

					System.out.println("Rceived " + date);
					int joinigYear = Integer.parseInt(date.substring(0, 4));
					int joiningMonth = Integer.parseInt(date.substring(5, 7));
					int joiningdate = Integer.parseInt(date.substring(8, 10));

				

					

					payableDays = Float.parseFloat(table.getValueAt(i, 3)
							.toString());
					leavesTaken = Float.parseFloat(table.getValueAt(i, 4)
							.toString());
					leavesAdjusted = Float.parseFloat(table.getValueAt(i, 5)
							.toString());
					paidDays = (payableDays + leavesAdjusted) - leavesTaken;

					if (joinigYear == currentYear
							&& joiningMonth == currentMonth) {

						paidDays = paidDays - joiningdate + 1;
					}

					table.setValueAt(paidDays, i, 6);

					new MasterGridDAO().saveLeaves(monthofSalary, empid,
							payableDays, leavesTaken, leavesAdjusted, paidDays);
					
					
					//---------------------------Inactive between month paid days caclulation-------------------//
		
					
					String status=new MasterGridDAO().getStatus(empid);
					
					if(status.equals("Inactive")){
						String inactiveDate=new MasterGridDAO().getInactiveDate(empid);
						
						System.out.println("Rceived " + inactiveDate);
						
						int inactiveYear = Integer.parseInt(inactiveDate.substring(0, 4));
						int inactiveMonth = Integer.parseInt(inactiveDate.substring(5, 7));
						int inactiveDay = Integer.parseInt(inactiveDate.substring(8, 10));
						
						System.out.println("Inactive Year="+inactiveYear);
						System.out.println("Inactive Month="+inactiveMonth);
						System.out.println("Inactive Day="+inactiveDay);
						
						Calendar calendar=Calendar.getInstance();
						int currentYearinactive=calendar.get(Calendar.YEAR);
						int currentMonthinactive=calendar.get(Calendar.MONTH);
						// int currentDate=calendar.get(Calendar.DAY_OF_MONTH);
						
						
						if (inactiveYear == currentYearinactive
								&& inactiveMonth == currentMonthinactive) {

							paidDays = (inactiveDay-leavesTaken+leavesAdjusted)-1;
							System.out.println("Paid Days="+paidDays);
						}
						table.setValueAt(paidDays, i, 6);
						
					}
					//END---------------------------Inactive between month paid days caclulation-------------------//
					

					ArrayList monthdata = calculateBasicEarn(empid);

					basicEarn = Float.parseFloat(monthdata.get(0).toString());
					table.setValueAt(basicEarn, i, 8);

					basicEarnArrear = 0;

					hra = Float.parseFloat(monthdata.get(1).toString());
					table.setValueAt(hra, i, 9);

					hraArrear = 0;

					medicalAllowance = Float.parseFloat(monthdata.get(2)
							.toString());

					table.setValueAt(medicalAllowance, i, 10);

					medicalAllowanceArrear = Float.parseFloat(monthdata.get(3)
							.toString());
					table.setValueAt(medicalAllowanceArrear, i, 11);

					conveyance = Float.parseFloat(monthdata.get(4).toString());
					table.setValueAt(conveyance, i, 12);

					specialAllowance = Float.parseFloat(monthdata.get(5)
							.toString());
					table.setValueAt(specialAllowance, i, 13);

					otherAllowance = Float.parseFloat(monthdata.get(6)
							.toString());
					table.setValueAt(otherAllowance, i, 14);

					executiveAllowance = Float.parseFloat(monthdata.get(7)
							.toString());
					table.setValueAt(executiveAllowance, i, 15);

					executiveAllowanceArrear = Float.parseFloat(monthdata
							.get(8).toString());
					table.setValueAt(executiveAllowanceArrear, i, 16);

					intressSubsidy = Float.parseFloat(monthdata.get(9)
							.toString());
					table.setValueAt(intressSubsidy, i, 17);

					performanceIncentive = Float.parseFloat(monthdata.get(10)
							.toString());
					table.setValueAt(performanceIncentive, i, 18);

					ltaPaid = Float.parseFloat(monthdata.get(11).toString());
					table.setValueAt(ltaPaid, i, 19);

					retainesBonus = Float.parseFloat(monthdata.get(12)
							.toString());
					table.setValueAt(retainesBonus, i, 20);

					pf = Float.parseFloat(monthdata.get(13).toString());
					table.setValueAt(pf, i, 21);

					pfArrear = Float.parseFloat(monthdata.get(14).toString());
					table.setValueAt(pfArrear, i, 22);

					professionalTax = finalpt;// Float.parseFloat(monthdata.get(15)
					// .toString());
					table.setValueAt(professionalTax, i, 23);

					incometax = Float.parseFloat(monthdata.get(16).toString());
					table.setValueAt(incometax, i, 24);

					d1Misc = Float.parseFloat(monthdata.get(17).toString());
					table.setValueAt(d1Misc, i, 25);

					d2Misc = Float.parseFloat(monthdata.get(18).toString());
					table.setValueAt(d2Misc, i, 26);

					canteenDeduction = Float.parseFloat(monthdata.get(19)
							.toString());
					table.setValueAt(canteenDeduction, i, 27);

					sportClub = Float.parseFloat(monthdata.get(20).toString());
					table.setValueAt(sportClub, i, 28);

					telephoneDeduction = Float.parseFloat(monthdata.get(21)
							.toString());
					table.setValueAt(telephoneDeduction, i, 29);
					
					medicalInsurance=Float.parseFloat(monthdata.get(22)
							.toString());
					table.setValueAt(medicalInsurance, i, 30);
					

					// method call to calculate total salary
					calculatenet(empid, i);

					// ----------------------------Sending data to
					// Database--------------------------------//

					int totalIncome = Integer.parseInt(table.getValueAt(i, 31)
							.toString());
					int totalDeduction = Integer.parseInt(table.getValueAt(i,
							32).toString());
					int netSal = Integer.parseInt(table.getValueAt(i, 7)
							.toString());

					//
					NumberToWord obj = new NumberToWord();
					String SalInWord = obj.convert(netSal);
					System.out.println(SalInWord);

					// Adding data into ArrayList to
					ArrayList dataToSave = new ArrayList();

					dataToSave.add(empid);
					dataToSave.add(monthofSalary);
					dataToSave.add(basicEarn);
					dataToSave.add(basicEarnArrear);
					dataToSave.add(payableDays);
					dataToSave.add(paidDays);
					dataToSave.add(hra);
					dataToSave.add(hraArrear);
					dataToSave.add(specialAllowance);
					dataToSave.add(otherAllowance);
					dataToSave.add(medicalAllowance);
					dataToSave.add(medicalAllowanceArrear);
					dataToSave.add(executiveAllowance);
					dataToSave.add(executiveAllowanceArrear);
					dataToSave.add(intressSubsidy);
					dataToSave.add(performanceIncentive);
					dataToSave.add(ltaPaid);
					dataToSave.add(retainesBonus);
					dataToSave.add(pf);
					dataToSave.add(pfArrear);
					dataToSave.add(finalpt);
					dataToSave.add(incometax);
					dataToSave.add(d1Misc);
					dataToSave.add(d2Misc);
					dataToSave.add(canteenDeduction);
					dataToSave.add(sportClub);
					dataToSave.add(telephoneDeduction);
					dataToSave.add(totalIncome);
					dataToSave.add(totalDeduction);
					dataToSave.add(netSal);
					dataToSave.add(conveyance);
					dataToSave.add(SalInWord);
					dataToSave.add(medicalInsurance);

					SaveSalaryDeatails(dataToSave);
					checkYtdDetails(empid, monthofSalary);
					SaveYtdDetails(empid, monthofSalary);

				}

				catch (Exception e) {
					e.printStackTrace();
					errCheck++;
					JOptionPane
							.showMessageDialog(
									null,
									"Please Enter Leaves data for selected employees !",
									"Alert..!!!", JOptionPane.OK_CANCEL_OPTION);
				}

			}

		}
		if (errCheck == 0) {
			int countofSelected = 0;

			for (int i = 0; i < table.getRowCount(); i++) {

				if (Boolean.parseBoolean(table.getValueAt(i, 0).toString())) {
					countofSelected++;
				}

			}
			if (countofSelected > 0) {
				JOptionPane.showMessageDialog(null,
						"Salary has been Calculated &\n Saved Successfully!!!",
						"Information..!!!", JOptionPane.INFORMATION_MESSAGE);
			}

			else {
				JOptionPane.showMessageDialog(null,
						"Please select the employees to calculate salary!!!",
						"warning..!!!", JOptionPane.WARNING_MESSAGE);
			}
		}

	}

	// this method fetch current month salary & ytd salary details
	// to calculate ytd details of current month & save it to database
	public void checkYtdDetails(String empid, String monthofSalary2) {

		new MasterGridDAO().getYtdSalaryStatus(empid, monthofSalary2);

	}

	// this method fetch current month salary & ytd salary details
	// to calculate ytd details of current month & save it to database
	public void SaveYtdDetails(String empid, String monthofSalary2) {

		StringBuffer currentmon = new StringBuffer();
		currentmon.append(monthofSalary2);

		String monarr[] = { "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT",
				"NOV", "DEC", "JAN", "FEB", "MAR", };
		int year = Integer.parseInt(monthofSalary2.substring(0, 4));
		StringBuffer sbmonthofsal = new StringBuffer();

		String mon = monthofSalary2.substring(5, 8);

		ArrayList ytdsalary = new ArrayList();
		ArrayList salofmon = new ArrayList();
		int ytdbasicearn = 0, ytdbasicearnarear = 0, ytdhra = 0, ytdhraarrear = 0, ytdspecialallowance = 0, ytdotherallowance = 0, ytdmedicalallowance = 0, ytdmedicalallowancearrear = 0, ytdperformanceincentive = 0, ytdprovidenetFund = 0, ytdprovidenetFundarrear = 0, ytdproftax = 0, ytdincometax = 0, ytdd1misc = 0, ytdd2misc = 0, ytdcanteen = 0, ytdconveyance = 0, ytdtotal = 0, ytdtotaldeduction = 0, ytdexecutiveallowance = 0, ytdexecutiveallowancearrear = 0, ytdintressSubsidy = 0, ytdltapaid = 0, ytdretainersbonus = 0, ytdsportclubdeduction = 0, ytdtelephonededuction = 0,ytdmedicalinsurance=0;

		int basicearn = 0, basicearnarear = 0, hra = 0, hraarrear = 0, specialallowance = 0, otherallowance = 0, medicalallowance = 0, medicalallowancearrear = 0, performanceincentive = 0, providenetFund = 0, providenetFundarrear = 0, proftax = 0, incometax = 0, d1misc = 0, d2misc = 0, canteen = 0, conveyance = 0, total = 0, totaldeduction = 0, executiveallowance = 0, executiveallowancearrear = 0, intressSubsidy = 0, ltapaid = 0, retainersbonus = 0, sportclubdeduction = 0, telephonededuction = 0,medicalinsurancededuction=0;

		if (mon.equals("APR") || mon.equals("MAY") || mon.equals("JUN")
				|| mon.equals("JUL") || mon.equals("AUG") || mon.equals("SEP")
				|| mon.equals("OCT") || mon.equals("NOV") || mon.equals("DEC")) {
			for (int i = 0; i < 12; i++) {

				if (i >= 0 && i <= 8) {
					sbmonthofsal.append(year);
					sbmonthofsal.append("-");
					sbmonthofsal.append(monarr[i]);
					salofmon = new MasterGridDAO().getSal(sbmonthofsal, empid);

					basicearn = (int) salofmon.get(0);
					basicearnarear = (int) salofmon.get(1);
					hra = (int) salofmon.get(2);
					hraarrear = (int) salofmon.get(3);
					specialallowance = (int) salofmon.get(4);
					otherallowance = (int) salofmon.get(5);
					medicalallowance = (int) salofmon.get(6);
					medicalallowancearrear = (int) salofmon.get(7);
					performanceincentive = (int) salofmon.get(8);
					providenetFund = (int) salofmon.get(9);
					providenetFundarrear = (int) salofmon.get(10);
					proftax = (int) salofmon.get(11);
					incometax = (int) salofmon.get(12);
					d1misc = (int) salofmon.get(13);
					d2misc = (int) salofmon.get(14);
					canteen = (int) salofmon.get(15);
					conveyance = (int) salofmon.get(16);
					total = (int) salofmon.get(17);
					totaldeduction = (int) salofmon.get(18);
					executiveallowance = (int) salofmon.get(19);
					executiveallowancearrear = (int) salofmon.get(20);
					intressSubsidy = (int) salofmon.get(21);
					ltapaid = (int) salofmon.get(22);
					retainersbonus = (int) salofmon.get(23);
					sportclubdeduction = (int) salofmon.get(24);
					telephonededuction = (int) salofmon.get(25);
					medicalinsurancededuction=(int)salofmon.get(26);
					
					

					System.out.println(sbmonthofsal + " " + basicearn);
				}

				if (i >= 9 && i <= 11) {
					int cyear = year + 1;
					sbmonthofsal.append(cyear);
					sbmonthofsal.append("-");
					sbmonthofsal.append(monarr[i]);
					salofmon = new MasterGridDAO().getSal(sbmonthofsal, empid);

					basicearn = (int) salofmon.get(0);
					basicearnarear = (int) salofmon.get(1);
					hra = (int) salofmon.get(2);
					hraarrear = (int) salofmon.get(3);
					specialallowance = (int) salofmon.get(4);
					otherallowance = (int) salofmon.get(5);
					medicalallowance = (int) salofmon.get(6);
					medicalallowancearrear = (int) salofmon.get(7);
					performanceincentive = (int) salofmon.get(8);
					providenetFund = (int) salofmon.get(9);
					providenetFundarrear = (int) salofmon.get(10);
					proftax = (int) salofmon.get(11);
					incometax = (int) salofmon.get(12);
					d1misc = (int) salofmon.get(13);
					d2misc = (int) salofmon.get(14);
					canteen = (int) salofmon.get(15);
					conveyance = (int) salofmon.get(16);
					total = (int) salofmon.get(17);
					totaldeduction = (int) salofmon.get(18);
					executiveallowance = (int) salofmon.get(19);
					executiveallowancearrear = (int) salofmon.get(20);
					intressSubsidy = (int) salofmon.get(21);
					ltapaid = (int) salofmon.get(22);
					retainersbonus = (int) salofmon.get(23);
					sportclubdeduction = (int) salofmon.get(24);
					telephonededuction = (int) salofmon.get(25);
					medicalinsurancededuction=(int)salofmon.get(26);
					
					System.out.println(sbmonthofsal + " " + basicearn);

				}

				ytdbasicearn = ytdbasicearn + basicearn;
				ytdbasicearnarear = ytdbasicearnarear + basicearnarear;
				ytdhra = ytdhra + hra;
				ytdhraarrear = ytdhraarrear + hraarrear;
				ytdspecialallowance = ytdspecialallowance + specialallowance;
				ytdotherallowance = ytdotherallowance + otherallowance;
				ytdmedicalallowance = ytdmedicalallowance + medicalallowance;
				ytdmedicalallowancearrear = ytdmedicalallowancearrear
						+ medicalallowancearrear;
				ytdperformanceincentive = ytdperformanceincentive
						+ performanceincentive;
				ytdprovidenetFund = ytdprovidenetFund + providenetFund;
				ytdprovidenetFundarrear = ytdprovidenetFundarrear
						+ providenetFundarrear;
				ytdproftax = ytdproftax + proftax;
				ytdincometax = ytdincometax + incometax;
				ytdd1misc = ytdd1misc + d1misc;
				ytdd2misc = ytdd2misc + d2misc;
				ytdcanteen = ytdcanteen + canteen;
				ytdconveyance = ytdconveyance + conveyance;
				ytdtotal = ytdtotal + total;
				ytdtotaldeduction = ytdtotaldeduction + totaldeduction;
				ytdexecutiveallowance = ytdexecutiveallowance
						+ executiveallowance;
				ytdexecutiveallowancearrear = ytdexecutiveallowancearrear
						+ executiveallowancearrear;
				ytdintressSubsidy = ytdintressSubsidy + intressSubsidy;
				ytdltapaid = ytdltapaid + ltapaid;
				ytdretainersbonus = ytdretainersbonus + retainersbonus;
				ytdsportclubdeduction = ytdsportclubdeduction
						+ sportclubdeduction;
				ytdtelephonededuction = ytdtelephonededuction
						+ telephonededuction;
				ytdmedicalinsurance=ytdmedicalinsurance+medicalinsurancededuction;
				
				if (sbmonthofsal.toString().equals(currentmon.toString())) {
					break;
				}
				sbmonthofsal.delete(0, sbmonthofsal.length());
			}

		}

		else if (mon.equals("JAN") || mon.equals("FEB") || mon.equals("MAR")) {
			for (int i = 0; i < 12; i++) {

				if (i >= 0 && i <= 8) {
					int cyear = year - 1;
					sbmonthofsal.append(cyear);
					sbmonthofsal.append("-");
					sbmonthofsal.append(monarr[i]);
					salofmon = new MasterGridDAO().getSal(sbmonthofsal, empid);
					basicearn = (int) salofmon.get(0);
					basicearnarear = (int) salofmon.get(1);
					hra = (int) salofmon.get(2);
					hraarrear = (int) salofmon.get(3);
					specialallowance = (int) salofmon.get(4);
					otherallowance = (int) salofmon.get(5);
					medicalallowance = (int) salofmon.get(6);
					medicalallowancearrear = (int) salofmon.get(7);
					performanceincentive = (int) salofmon.get(8);
					providenetFund = (int) salofmon.get(9);
					providenetFundarrear = (int) salofmon.get(10);
					proftax = (int) salofmon.get(11);
					incometax = (int) salofmon.get(12);
					d1misc = (int) salofmon.get(13);
					d2misc = (int) salofmon.get(14);
					canteen = (int) salofmon.get(15);
					conveyance = (int) salofmon.get(16);
					total = (int) salofmon.get(17);
					totaldeduction = (int) salofmon.get(18);
					executiveallowance = (int) salofmon.get(19);
					executiveallowancearrear = (int) salofmon.get(20);
					intressSubsidy = (int) salofmon.get(21);
					ltapaid = (int) salofmon.get(22);
					retainersbonus = (int) salofmon.get(23);
					sportclubdeduction = (int) salofmon.get(24);
					telephonededuction = (int) salofmon.get(25);
					medicalinsurancededuction=(int)salofmon.get(26);
					
					System.out.println(sbmonthofsal + " " + basicearn);

				}

				if (i >= 9 && i <= 11) {
					sbmonthofsal.append(year);
					sbmonthofsal.append("-");
					sbmonthofsal.append(monarr[i]);
					salofmon = new MasterGridDAO().getSal(sbmonthofsal, empid);
					basicearn = (int) salofmon.get(0);
					basicearnarear = (int) salofmon.get(1);
					hra = (int) salofmon.get(2);
					hraarrear = (int) salofmon.get(3);
					specialallowance = (int) salofmon.get(4);
					otherallowance = (int) salofmon.get(5);
					medicalallowance = (int) salofmon.get(6);
					medicalallowancearrear = (int) salofmon.get(7);
					performanceincentive = (int) salofmon.get(8);
					providenetFund = (int) salofmon.get(9);
					providenetFundarrear = (int) salofmon.get(10);
					proftax = (int) salofmon.get(11);
					incometax = (int) salofmon.get(12);
					d1misc = (int) salofmon.get(13);
					d2misc = (int) salofmon.get(14);
					canteen = (int) salofmon.get(15);
					conveyance = (int) salofmon.get(16);
					total = (int) salofmon.get(17);
					totaldeduction = (int) salofmon.get(18);
					executiveallowance = (int) salofmon.get(19);
					executiveallowancearrear = (int) salofmon.get(20);
					intressSubsidy = (int) salofmon.get(21);
					ltapaid = (int) salofmon.get(22);
					retainersbonus = (int) salofmon.get(23);
					sportclubdeduction = (int) salofmon.get(24);
					telephonededuction = (int) salofmon.get(25);
					medicalinsurancededuction=(int)salofmon.get(26);
					
					System.out.println(sbmonthofsal + " " + basicearn);

				}
				ytdbasicearn = ytdbasicearn + basicearn;
				ytdbasicearnarear = ytdbasicearnarear + basicearnarear;
				ytdhra = ytdhra + hra;
				ytdhraarrear = ytdhraarrear + hraarrear;
				ytdspecialallowance = ytdspecialallowance + specialallowance;
				ytdotherallowance = ytdotherallowance + otherallowance;
				ytdmedicalallowance = ytdmedicalallowance + medicalallowance;
				ytdmedicalallowancearrear = ytdmedicalallowancearrear
						+ medicalallowancearrear;
				ytdperformanceincentive = ytdperformanceincentive
						+ performanceincentive;
				ytdprovidenetFund = ytdprovidenetFund + providenetFund;
				ytdprovidenetFundarrear = ytdprovidenetFundarrear
						+ providenetFundarrear;
				ytdproftax = ytdproftax + proftax;
				ytdincometax = ytdincometax + incometax;
				ytdd1misc = ytdd1misc + d1misc;
				ytdd2misc = ytdd2misc + d2misc;
				ytdcanteen = ytdcanteen + canteen;
				ytdconveyance = ytdconveyance + conveyance;
				ytdtotal = ytdtotal + total;
				ytdtotaldeduction = ytdtotaldeduction + totaldeduction;
				ytdexecutiveallowance = ytdexecutiveallowance
						+ executiveallowance;
				ytdexecutiveallowancearrear = ytdexecutiveallowancearrear
						+ executiveallowancearrear;
				ytdintressSubsidy = ytdintressSubsidy + intressSubsidy;
				ytdltapaid = ytdltapaid + ltapaid;
				ytdretainersbonus = ytdretainersbonus + retainersbonus;
				ytdsportclubdeduction = ytdsportclubdeduction
						+ sportclubdeduction;
				ytdtelephonededuction = ytdtelephonededuction
						+ telephonededuction;
				ytdmedicalinsurance=ytdmedicalinsurance+medicalinsurancededuction;

				if (sbmonthofsal.toString().equals(currentmon.toString())) {
					break;
				}

				sbmonthofsal.delete(0, sbmonthofsal.length());
			}
		}
		ytdsalary.add(empid);
		ytdsalary.add(monthofSalary2);
		ytdsalary.add(ytdbasicearn);
		ytdsalary.add(ytdbasicearnarear);
		ytdsalary.add(ytdhra);
		ytdsalary.add(ytdhraarrear);
		ytdsalary.add(ytdspecialallowance);
		ytdsalary.add(ytdotherallowance);
		ytdsalary.add(ytdmedicalallowance);
		ytdsalary.add(ytdmedicalallowancearrear);
		ytdsalary.add(ytdperformanceincentive);
		ytdsalary.add(ytdprovidenetFund);
		ytdsalary.add(ytdprovidenetFundarrear);
		ytdsalary.add(ytdproftax);
		ytdsalary.add(ytdincometax);
		ytdsalary.add(ytdd1misc);
		ytdsalary.add(ytdd2misc);
		ytdsalary.add(ytdcanteen);
		ytdsalary.add(ytdconveyance);
		ytdsalary.add(ytdtotal);
		ytdsalary.add(ytdtotaldeduction);
		ytdsalary.add(ytdexecutiveallowance);
		ytdsalary.add(ytdexecutiveallowancearrear);
		ytdsalary.add(ytdintressSubsidy);
		ytdsalary.add(ytdltapaid);
		ytdsalary.add(ytdretainersbonus);
		ytdsalary.add(ytdsportclubdeduction);
		ytdsalary.add(ytdtelephonededuction);
		ytdsalary.add(ytdmedicalinsurance);
		
		

		// ----------------------------------------------------------

		new MasterGridDAO().saveNewSalary(ytdsalary);
	}

	// This method fetch & fill the already saved salary details for current
	// month

	public void fillSalaryDetails(String empid, String monthofSalary2, int i) {
		// TODO Auto-generated method stub

		/* System.out.println("*****Method Call to Fill Salary****"); */
		ArrayList oldSalary = new ArrayList();
		oldSalary = null;
		oldSalary = new MasterGridDAO().getOldSalary(empid, monthofSalary2);

		if (oldSalary.size() > 0) {
			// table.setValueAt(true, i, 0);

			basicEarn = Float.parseFloat(oldSalary.get(0).toString());
			table.setValueAt(basicEarn, i, 8);

			basicEarnArrear = Float.parseFloat(oldSalary.get(1).toString());

			// payableDays=Float.parseFloat(oldSalary.get(2).toString());
			// table.setValueAt(hra, i, 8);

			paidDays = Float.parseFloat(oldSalary.get(3).toString());
			table.setValueAt(paidDays, i, 7);

			hra = Float.parseFloat(oldSalary.get(4).toString());
			table.setValueAt(hra, i, 9);

			hraArrear = Float.parseFloat(oldSalary.get(5).toString());
			;

			medicalAllowance = Float.parseFloat(oldSalary.get(10).toString());
			table.setValueAt(medicalAllowance, i, 10);

			medicalAllowanceArrear = Float.parseFloat(oldSalary.get(11)
					.toString());
			table.setValueAt(medicalAllowanceArrear, i, 11);

			conveyance = Float.parseFloat(oldSalary.get(28).toString());
			table.setValueAt(conveyance, i, 12);

			specialAllowance = Float.parseFloat(oldSalary.get(6).toString());
			table.setValueAt(specialAllowance, i, 13);

			otherAllowance = Float.parseFloat(oldSalary.get(7).toString());
			table.setValueAt(otherAllowance, i, 14);

			executiveAllowance = Float.parseFloat(oldSalary.get(8).toString());
			table.setValueAt(executiveAllowance, i, 15);

			executiveAllowanceArrear = Float.parseFloat(oldSalary.get(9)
					.toString());
			table.setValueAt(executiveAllowanceArrear, i, 16);

			intressSubsidy = Float.parseFloat(oldSalary.get(12).toString());
			table.setValueAt(intressSubsidy, i, 17);

			performanceIncentive = Float.parseFloat(oldSalary.get(13)
					.toString());
			table.setValueAt(performanceIncentive, i, 18);

			ltaPaid = Float.parseFloat(oldSalary.get(14).toString());
			table.setValueAt(ltaPaid, i, 19);

			retainesBonus = Float.parseFloat(oldSalary.get(15).toString());
			table.setValueAt(retainesBonus, i, 20);

			pf = Float.parseFloat(oldSalary.get(16).toString());
			table.setValueAt(pf, i, 21);

			pfArrear = Float.parseFloat(oldSalary.get(17).toString());
			table.setValueAt(pfArrear, i, 22);

			professionalTax = Float.parseFloat(oldSalary.get(18).toString());
			table.setValueAt(professionalTax, i, 23);

			incometax = Float.parseFloat(oldSalary.get(19).toString());
			table.setValueAt(incometax, i, 24);

			d1Misc = Float.parseFloat(oldSalary.get(20).toString());
			table.setValueAt(d1Misc, i, 25);

			d2Misc = Float.parseFloat(oldSalary.get(21).toString());
			table.setValueAt(d2Misc, i, 26);

			canteenDeduction = Float.parseFloat(oldSalary.get(22).toString());
			table.setValueAt(canteenDeduction, i, 27);

			sportClub = Float.parseFloat(oldSalary.get(23).toString());
			table.setValueAt(sportClub, i, 28);

			telephoneDeduction = Float.parseFloat(oldSalary.get(24).toString());
			table.setValueAt(telephoneDeduction, i, 29);
			
			medicalInsurance=Float.parseFloat(oldSalary.get(29).toString());
			table.setValueAt(medicalInsurance, i, 30);
			

			float totalIncome = Float.parseFloat(oldSalary.get(25).toString());
			table.setValueAt(totalIncome, i, 31);

			float totalDeduction = Float.parseFloat(oldSalary.get(26)
					.toString());
			table.setValueAt(totalDeduction, i, 32);

			float netSalary = Float.parseFloat(oldSalary.get(27).toString());
			table.setValueAt(netSalary, i, 7);

		}

		else {
			System.out.println("else Part");
			table.setValueAt(false, i, 0);
			table.setValueAt(0, i, 4);
			table.setValueAt(0, i, 5);
			table.setValueAt(0, i, 6);
			table.setValueAt(0, i, 7);
			table.setValueAt(0, i, 8);
			table.setValueAt(0, i, 8);

			table.setValueAt(0, i, 8);

			table.setValueAt(0, i, 9);

			table.setValueAt(0, i, 10);

			table.setValueAt(0, i, 11);

			table.setValueAt(0, i, 12);

			table.setValueAt(0, i, 13);

			table.setValueAt(0, i, 14);

			table.setValueAt(0, i, 15);

			table.setValueAt(0, i, 16);

			table.setValueAt(0, i, 17);

			table.setValueAt(0, i, 18);

			table.setValueAt(0, i, 19);

			table.setValueAt(0, i, 20);

			table.setValueAt(0, i, 21);

			table.setValueAt(0, i, 22);

			// table.setValueAt(0, i, 23);

			table.setValueAt(0, i, 24);

			table.setValueAt(0, i, 25);

			table.setValueAt(0, i, 26);

			table.setValueAt(0, i, 27);

			table.setValueAt(0, i, 28);

			table.setValueAt(0, i, 29);

			table.setValueAt(0, i, 30);

			table.setValueAt(0, i, 31);
			
			table.setValueAt(0, i, 32);
		}

	}

	// This method calculate the the salary on the basis of paid days & basic
	// details
	public ArrayList calculateBasicEarn(String empid) {

		float baseEarn;
		float baseSal, medicalAllowance, medicalAllowanceArrear, Convayance, specialAllowance, 
		otherAllowance, executiveallowance, executiveallowanceArrear, intressSubsidy, performanceIncentive, 
		ltaPaid, retainersBonus, providantFund, providantFundArrear, professionalTax, incomeTax, d1Misc, d2Misc,
		canteenDeduction, sportClub, telephoneDeduction,medicalInsurance;

		ArrayList basicData = new MasterGridDAO().getBaseData(empid);

		baseSal = Float.parseFloat(basicData.get(0).toString());

		medicalAllowance = Float.parseFloat(basicData.get(1).toString());
		medicalAllowanceArrear = Float.parseFloat(basicData.get(2).toString());
		Convayance = Float.parseFloat(basicData.get(3).toString());
		specialAllowance = Float.parseFloat(basicData.get(4).toString());
		otherAllowance = Float.parseFloat(basicData.get(5).toString());
		executiveallowance = Float.parseFloat(basicData.get(6).toString());
		executiveallowanceArrear = Float
				.parseFloat(basicData.get(7).toString());
		intressSubsidy = Float.parseFloat(basicData.get(8).toString());
		performanceIncentive = Float.parseFloat(basicData.get(9).toString());
		ltaPaid = Float.parseFloat(basicData.get(10).toString());
		retainersBonus = Float.parseFloat(basicData.get(11).toString());
		providantFund = Float.parseFloat(basicData.get(12).toString());
		providantFundArrear = Float.parseFloat(basicData.get(13).toString());
		professionalTax = Float.parseFloat(basicData.get(14).toString());
		incomeTax = Float.parseFloat(basicData.get(15).toString());
		d1Misc = Float.parseFloat(basicData.get(16).toString());
		d2Misc = Float.parseFloat(basicData.get(17).toString());
		canteenDeduction = Float.parseFloat(basicData.get(18).toString());
		sportClub = Float.parseFloat(basicData.get(19).toString());
		telephoneDeduction = Float.parseFloat(basicData.get(20).toString());
		medicalInsurance=Float.parseFloat(basicData.get(21).toString());

		baseEarn = ((baseSal / payableDays) * paidDays);
		baseEarn = roundof(baseEarn);

		float hra = (float) (baseEarn * 0.4);
		hra = roundof(hra);

		float medicalAllowance_paid = (float) (medicalAllowance / payableDays)
				* paidDays;

		float diffrence = medicalAllowance - medicalAllowance_paid; /*
																	 * medical
																	 * allowance
																	 * adjustment
																	 */

		medicalAllowance_paid = roundof(medicalAllowance); // roundof(medicalAllowance_paid);

		float medicalAllowanceArrear_paid = medicalAllowanceArrear;

		float convence_paid = (float) (Convayance / payableDays) * paidDays;
		convence_paid = roundof(convence_paid);

		float special_allowance_paid = (specialAllowance / payableDays)
				* paidDays;
		special_allowance_paid = roundof(special_allowance_paid);

		float other_allowance_piad = (otherAllowance / payableDays) * paidDays;
		other_allowance_piad = other_allowance_piad - diffrence; /*
																 * medical
																 * allowance
																 * adjustment
																 */
		other_allowance_piad = roundof(other_allowance_piad);

		float executiveallowance_paid = executiveallowance;
		float executiveallowanceArrear_paid = executiveallowanceArrear;
		float intressSubsidy_paid = intressSubsidy;
		float performanceIncentive_paid = performanceIncentive;
		float ltaPaid_paid = ltaPaid;
		float retainersBonus_paid = retainersBonus;

		float pf = (float) (baseEarn * 0.12);
		pf = roundof(pf);

		float providantFundArrear_deducted = providantFundArrear;
		float professionalTax_deduction = professionalTax;
		float incomeTax_deducted = incomeTax;
		float d1Misc_deducted = d1Misc;
		float d2Misc_deducted = d2Misc;
		float canteenDeduction_deducted = canteenDeduction;
		float sportClub_deducted = sportClub;
		float telephoneDeduction_deducted = telephoneDeduction;

		ArrayList monthData = new ArrayList();
		monthData.add(baseEarn);
		monthData.add(hra);
		monthData.add(medicalAllowance_paid);
		monthData.add(medicalAllowanceArrear_paid);
		monthData.add(convence_paid);
		monthData.add(special_allowance_paid);
		monthData.add(other_allowance_piad);
		monthData.add(executiveallowance_paid);
		monthData.add(executiveallowanceArrear_paid);
		monthData.add(intressSubsidy_paid);
		monthData.add(performanceIncentive_paid);
		monthData.add(ltaPaid_paid);
		monthData.add(retainersBonus_paid);
		monthData.add(pf);
		monthData.add(providantFundArrear_deducted);
		monthData.add(professionalTax_deduction);
		monthData.add(incomeTax_deducted);
		monthData.add(d1Misc_deducted);
		monthData.add(d2Misc_deducted);
		monthData.add(canteenDeduction_deducted);
		monthData.add(sportClub_deducted);
		monthData.add(telephoneDeduction_deducted);
		monthData.add(medicalInsurance);

		return monthData;
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void focusLost(FocusEvent fe) {
		// TODO Auto-generated method stub

		
		if (fe.getComponent() == monthcombo || fe.getComponent() == yearcombo) {
		
			Valyear = Integer.parseInt(yearcombo.getSelectedItem().toString());
			valMonth = monthcombo.getSelectedIndex();
			System.out.println("Year" + Valyear + "  Month" + valMonth);

			String selectedMon = monthcombo.getSelectedItem().toString();
			int selectedYear = Integer.parseInt(yearcombo.getSelectedItem()
					.toString());

			if (selectedYear % 4 == 0) {

				if (selectedMon == "JAN" || selectedMon == "MAR"
						|| selectedMon == "MAY" || selectedMon == "JUL"
						|| selectedMon == "AUG" || selectedMon == "OCT"
						|| selectedMon == "DEC") {
					setPayableDays(31);
				}
				if (selectedMon == "APR" || selectedMon == "JUN"
						|| selectedMon == "SEP" || selectedMon == "NOV") {
					setPayableDays(30);
				}
				if (selectedMon == "FEB") {
					setPayableDays(29);
				}
			}

			else {

				if (selectedMon == "JAN" || selectedMon == "MAR"
						|| selectedMon == "MAY" || selectedMon == "JUL"
						|| selectedMon == "AUG" || selectedMon == "OCT"
						|| selectedMon == "DEC") {
					setPayableDays(31);
				}
				if (selectedMon == "APR" || selectedMon == "JUN"
						|| selectedMon == "SEP" || selectedMon == "NOV") {
					setPayableDays(30);
				}
				if (selectedMon == "FEB") {
					setPayableDays(28);
				}
			}

			/*
			 * if(selectedMon=="FEB"){ setProfessionalTax(300); } else {
			 * setProfessionalTax(200); }
			 */

			CalltofillSalary();
		}

	}

	int roundof(double d) {
		int x = 0;
		double rem = d % 1;

		if (rem >= 0.5) {
			d = Math.ceil(d);
		} else {
			d = Math.floor(d);
		}

		x = (int) d;
		return x;
	}

	void calculatenet(String empid, int i) {

		// float pt=Float.parseFloat(table.getValueAt(i, 23).toString());
		int totalIncome = (int) (basicEarn + hra + medicalAllowance
				+ conveyance + specialAllowance + otherAllowance+canteenDeduction);
		System.out.println("***Other*****" + otherAllowance);
		System.out.println("***Total*****" + totalIncome);
	
		int totalDeduction = (int) (pf + incometax + medicalInsurance);
		

		StringBuffer monthOfSal = new StringBuffer();
		String mon = monthcombo.getSelectedItem().toString();
		String yer = yearcombo.getSelectedItem().toString();
		monthOfSal.append(yer);
		monthOfSal.append("-");
		monthOfSal.append(mon);
		String monthofsalary = monthOfSal.toString();
		String id = table.getValueAt(i, 1).toString();
		String gen = new MasterGridDAO().getGender(id);

		int netIncome = totalIncome - totalDeduction;

		System.out.println("###########" + mon);
		System.out.println("###########" + gen);
		System.out.println("###########" + netIncome);

		if (mon == "JAN" || mon == "MAR" || mon == "APR" || mon == "MAY"
				|| mon == "JUN" || mon == "JUL" || mon == "AUG" || mon == "SEP"
				|| mon == "OCT" || mon == "NOV" || mon == "DEC") {
			System.out.println("Month pass");
			System.out.println(netIncome);

			if (netIncome < 7500) {
				setProfessionalTax(0, i);

			}

			if (netIncome >= 7500 && netIncome <= 10000 && gen.equals("Female")) {
				setProfessionalTax(0, i);

			}

			if (netIncome >= 7500 && netIncome <= 10000 && gen.equals("Male")) {
				setProfessionalTax(175, i);

			}

			if (netIncome > 10000) {
				setProfessionalTax(200, i);

			}

		}

		if (mon == "FEB") {

			if (netIncome < 7500) {
				setProfessionalTax(0, i);

			}

			if (netIncome >= 7500 && netIncome <= 10000 && gen.equals("Female")) {
				setProfessionalTax(0, i);

			}

			if (netIncome >= 7500 && netIncome <= 10000 && gen.equals("Male")) {
				setProfessionalTax(300, i);

			}

			if (netIncome > 10000) {
				setProfessionalTax(300, i);

			}
		}
		// float pt=0;
		float pt = Float.parseFloat(table.getValueAt(i, 23).toString());
		finalpt = (int) pt;
		// int totalIncome=(int)
		// (basicEarn+hra+medicalAllowance+conveyance+specialAllowance+otherAllowance);
		totalDeduction = (int) (pf + incometax + pt+medicalInsurance);
		System.out.println("***Deduction*****" + totalDeduction);

		netIncome = totalIncome - totalDeduction;
		System.out.println("***Net*****" + netIncome);
		table.setValueAt(totalIncome, i, 31);
		table.setValueAt(totalDeduction, i, 32);
		table.setValueAt(netIncome, i, 7);
 
	}

	private void SaveSalaryDeatails(ArrayList salaryOfMonth) {

		new MasterGridDAO().saveSalaryData(salaryOfMonth);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getActionCommand() == "CALCULATE") {
			errCheck = 0;
			setCheckedStatus();
		}

		if (e.getActionCommand() == "CHECK") {
			if (selectAll.isSelected()) {
				for (int i = 0; i < table.getRowCount(); i++) {
					table.setValueAt(true, i, 0);
				}
			} else {
				for (int i = 0; i < table.getRowCount(); i++) {
					table.setValueAt(false, i, 0);
				}
			}
		}

		if (e.getActionCommand() == "LIST") {
			frame.setVisible(false);
			new menu(0);
		}

		if (e.getActionCommand() == "PRINT") {
			
			String dateofsal="";
			 java.util.Date selectedDate = (java.util.Date) datePicker.getModel().getValue();
		       
		  		
		  		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		  		try
		  		{
		  		dateofsal= sdf.format(selectedDate);
		  		}
		  		catch(Exception e1)
		  		{
		  			JOptionPane.showMessageDialog(null,
	                new JLabel("please select the date of salary", JLabel.LEFT),
	                "Select Date!!", JOptionPane.WARNING_MESSAGE);
		  			dateofsal=null;
		  		}
		  		
		  if(dateofsal!=null) 
		  {
			calculateBeforePrint();
			// frame.setVisible(false);
			int rwcnt = table.getRowCount();
			StringBuffer monthOfSal = new StringBuffer();
			String mon = monthcombo.getSelectedItem().toString();
			String yer = yearcombo.getSelectedItem().toString();
			monthOfSal.append(yer);
			monthOfSal.append("-");
			monthOfSal.append(mon);

			String monthofSalary2 = monthOfSal.toString();

			for (int i = 0; i < rwcnt; i++) {
				boolean x = Boolean.parseBoolean(table.getValueAt(i, 0)
						.toString());
				String empid = table.getValueAt(i, 1).toString();

				if (x) {
					String empcode = table.getValueAt(i, 1).toString();
					double salVal = Double.parseDouble(table.getValueAt(i, 7)
							.toString());

					if (salVal > 0) {
						reportGenerate(empcode, monthofSalary2);
					}
				}
			}

			JOptionPane.showMessageDialog(null, "Report Generated",
					"Information", JOptionPane.OK_CANCEL_OPTION);
			
		 }
		}

		if (e.getActionCommand() == "MAIL") {
			// frame.setVisible(false);
			int rwcnt = table.getRowCount();
			StringBuffer monthOfSal = new StringBuffer();
			String mon = monthcombo.getSelectedItem().toString();
			String yer = yearcombo.getSelectedItem().toString();
			monthOfSal.append(yer);
			monthOfSal.append("-");
			monthOfSal.append(mon);

			String monthofSalary2 = monthOfSal.toString();

			for (int i = 0; i < rwcnt; i++) {
				boolean x = Boolean.parseBoolean(table.getValueAt(i, 0)
						.toString());
				String empid = table.getValueAt(i, 1).toString();

				if (x) {
					String empcode = table.getValueAt(i, 1).toString();
					double salVal = Double.parseDouble(table.getValueAt(i, 7)
							.toString());

					if (salVal > 0) {
						String mailid = new MasterGridDAO().getEmail(empcode);
						sendmail(mailid, monthofSalary2, empcode);

					}
				}
			}
			// new menu(0);
			JOptionPane
					.showMessageDialog(null, "Finished Sending Mail!!!",
							"Mail's Send Sussfully!!!",
							JOptionPane.INFORMATION_MESSAGE);
		}

		if (e.getActionCommand() == "IMPORT") {

			jChooser.showOpenDialog(null);
			File file = jChooser.getSelectedFile();
			String filename = jChooser.getSelectedFile().getAbsolutePath();
			String employeeid = "";

			if (file.getName().endsWith("xls")) {

				fillData(file);
				System.out.println("******************" + dataimp.size());

				try {
					for (int j = 0; j < dataimp.size(); j++) {

						for (int i = 0; i < headers.size(); i++) {
							Vector d2 = (Vector) dataimp.get(j);
							//System.out.println(d2.get(i));
							if (i == 0) {
								employeeid = (String) d2.get(i);

							}

							else {
								for (int r = 0; r < table.getRowCount(); r++) {
									if (table.getValueAt(r, 1).toString()
											.equals(employeeid)) {
										double x = Double.parseDouble((String) d2.get(i));
										System.out.println("Emp"+employeeid+" Col="+r+" val="+x);
										table.setValueAt(x, r, i + 3);
									}
								}
							}

						}

					}

				} catch (ArrayIndexOutOfBoundsException ex) {
					JOptionPane.showMessageDialog(null,
							"Employee Count dosent match in Excel file",
							"Error", JOptionPane.ERROR_MESSAGE);

				} catch (Exception ex) {
					ex.printStackTrace();
					JOptionPane
							.showMessageDialog(
									null,
									"There is an Error in Excel file \n Please check the format of excel file",
									"Error", JOptionPane.ERROR_MESSAGE);

				}

			}

			else if (file.getName().endsWith("xlsx")) {
				setData(filename);

			}

			else {
				JOptionPane.showMessageDialog(null,
						"Please select only Excel file.", "Error",
						JOptionPane.ERROR_MESSAGE);

			}

		}

	}

	public void reportGenerate(String empcode, String mnth) {
		String reportdemo = "E:/Salary Executable_v2.0/salary2.0.jrxml";
		
		
		try {
			
			String reportname = new MasterGridDAO().getName(empcode);
			String  lnameletter="";
			String[] tokens= reportname.split(" ");
			for(int i=0;i<tokens.length;i++)
			{
				System.out.println(tokens[i]);
				if(tokens[1] != null)
				{
					String lname=tokens[1];
					 lnameletter=String.valueOf(lname.charAt(0));
					System.out.println(lnameletter);
				}
			}

			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/salarySlipDatabase", "root", "yungry");

			InputStream in = new FileInputStream(reportdemo);
			System.out.println(in);
			JasperDesign jasperdesign = JRXmlLoader.load(in);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperdesign);
			StringBuffer monthOfSal = new StringBuffer();
			String mon = monthcombo.getSelectedItem().toString();
			String yer = yearcombo.getSelectedItem().toString();
			monthOfSal.append(yer);
			monthOfSal.append("-");
			monthOfSal.append(mon);

			Map<String, Object> map = new HashMap();

			map.put("employee_code", empcode);
			map.put("month_of_salary", mnth);
			monthofSalary = monthOfSal.toString();
			String reportFilename = "Salary_Slips";
			File reportFile = new File("E:/" + reportFilename);
			if (reportFile.mkdir() || true) {
				File f = new File(reportFile + "/" + monthofSalary);
				String filepath = f.getAbsolutePath();
				System.out.println(filepath.toString());

				JasperPrint jsprint = JasperFillManager.fillReport(
						jasperReport, map, con);

				try {
					if (f.mkdir() || true) {

						/**
						 * 1- export to PDF
						 */
						// JasperExportManager.exportReportToPdfFile(jsprint,filepath);
						// JRExporter exporter = null;

						OutputStream output = new FileOutputStream(new File(
								filepath + "/" +tokens[0]+"_"+lnameletter+"_"+ empcode + "_" + mnth + ".pdf"));
						JasperExportManager.exportReportToPdfStream(jsprint,
								output);

						System.out.println("Done");
						// jsprint;

					} else {
						System.out.println("Directory is not created");
						/**
						 * 1- export to PDF
						 */
						// JasperExportManager.exportReportToPdfFile(jsprint,filepath);
						// JRExporter exporter = null;

						OutputStream output = new FileOutputStream(new File(
								filepath + "/" + empcode + "_" + mnth + ".pdf"));
						JasperExportManager.exportReportToPdfStream(jsprint,
								output);

						System.out.println("Done");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {

				System.out.println("Could not create Report File");
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}// -----------------------generate report Function Ends
		// Here-----------------------//

	void fillData(File file) {
		Workbook workbook = null;
		try {
			try {
				workbook = Workbook.getWorkbook(file);
			} catch (IOException ex) {
				Logger.getLogger(MasterGrid.class.getName()).log(Level.SEVERE,
						null, ex);
			}
			Sheet sheet = workbook.getSheet(0);
			headers.clear();

			for (int i = 0; i < sheet.getColumns(); i++) {
				jxl.Cell cell1 = sheet.getCell(i, 0);
				headers.add(cell1.getContents());
			}

			dataimp.clear();
			for (int j = 1; j < sheet.getRows(); j++) {
				Vector d = new Vector();

				for (int i = 0; i < sheet.getColumns(); i++) {

					jxl.Cell cell = sheet.getCell(i, j);
					System.out.println("***" + cell.getContents());
					d.add(cell.getContents());
				}

				d.add("\n");
				dataimp.add(d);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		}
	}

	void setData(String file) {
		try {

			// FileInputStream file = new FileInputStream( new
			// File("D:\\Book2.xlsx"));
			// jChooser=new JFileChooser();
			// jChooser.showOpenDialog(null);
			// String file = jChooser.getSelectedFile().getAbsolutePath();

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			// FileInputStream file = new FileInputStream( new
			// File("D:\\new.xlsx"));
			// XSSFWorkbook workbook = new XSSFWorkbook(file);
			// XSSFSheet sheet = workbook.getSheetAt(0);

			for (Row row : sheet) {
				for (Cell cell : row) {
					CellReference cellRef = new CellReference(row.getRowNum(),
							cell.getColumnIndex());
					// System.out.print(((Object) cellRef).formatAsString());

					if (row.getRowNum() == 0)
						continue;
					if (cell.getColumnIndex() == 0)
						continue;

					int rownum = row.getRowNum();
					int colnum = cell.getColumnIndex();

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							System.out.println(cell.getDateCellValue());
							table.setValueAt(1, rownum, colnum);
						} else {

							double val = cell.getNumericCellValue();

							try {

								rownum--;
								colnum--;
								colnum = colnum + 4;
								table.setValueAt(val, rownum, colnum);
							} catch (Exception e) {
								System.out.println("Exception Occured");
								e.printStackTrace();

							}
						}
						break;

					default:
						System.out.println();
						break;
					}

				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("File Not found");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void sendmail(String sendToMailId, String monthOfSalary, String empcode) {

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("hr@finitefour.com",
								"");
					}
				});

		String name = new MasterGridDAO().getName(empcode);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress("finite4"));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					sendToMailId));

			// Set Subject: header field
			message.setSubject("Salary Slip for " + monthOfSalary);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart
					.setText("Hello "
							+ name
							+ ",\n\t Please find the attached salary slip for the month of"
							+ monthOfSalary
							+ ".\n\n\nNote:This is a system Generated Email,\nFor any queries please mail at hr@finitefour.com\n\n\nRegards\nHR Finite 4");

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			String filename = "E:/Report/" + monthOfSalary + "/" + empcode
					+ "_" + monthOfSalary + ".pdf";
			DataSource source = new FileDataSource(filename);

			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);

		}

		
		catch (javax.mail.SendFailedException e) {
			e.printStackTrace();
			JOptionPane
					.showMessageDialog(
							null,
							" Mail Not sent.\n Please Check that the salary slip was printed & You have active Internate Connection",
							"Error!!! (Sending Mail) ",
							JOptionPane.ERROR_MESSAGE);
		}

		
		catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"Please check your configurations! \n Mail Not sent.",
					"Error!!! (Sending Mail)", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void calculateBeforePrint(){
		
		String dateofsal="";
		 java.util.Date selectedDate = (java.util.Date) datePicker.getModel().getValue();
	       
	  		
	  		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	  		try
	  		{
	  		dateofsal= sdf.format(selectedDate);
	  		}
	  		catch(Exception e)
	  		{
	  			JOptionPane.showMessageDialog(null,
                        new JLabel("please select the date of salary", JLabel.LEFT),
                        "Select Date!!", JOptionPane.WARNING_MESSAGE);
	  			//System.out.println("Please select the date of salary");
	  		}
	  		
	  		System.out.println("Date of Salary"+dateofsal);
	  		
		int rows=table.getRowCount();
		
		ArrayList newSalary=new ArrayList();
		
		StringBuffer monthOfSal = new StringBuffer();
		String mon = monthcombo.getSelectedItem().toString();
		String yer = yearcombo.getSelectedItem().toString();
		monthOfSal.append(yer);
		monthOfSal.append("-");
		monthOfSal.append(mon);

		String monthofSalary2 = monthOfSal.toString();
		
		for(int i=0;i<rows;i++){
			
			boolean x = Boolean.parseBoolean(table.getValueAt(i, 0)
					.toString());
			if(x){
			
			String empid=table.getValueAt(i, 1).toString();
			float basicearn=Float.parseFloat(table.getValueAt(i, 8).toString());
			float hra=Float.parseFloat(table.getValueAt(i, 9).toString());
			float medicalAllowance=Float.parseFloat(table.getValueAt(i, 10).toString());
			float medicalAllowanceArrear=Float.parseFloat(table.getValueAt(i, 11).toString());
			float conveyance=Float.parseFloat(table.getValueAt(i, 12).toString());
			float specialAllowance=Float.parseFloat(table.getValueAt(i, 13).toString());
			float otherAllowance=Float.parseFloat(table.getValueAt(i, 14).toString());
			float executiveAllowance=Float.parseFloat(table.getValueAt(i, 15).toString());
			float executiveAllowanceArrear=Float.parseFloat(table.getValueAt(i, 16).toString());
			float intressSubsidy=Float.parseFloat(table.getValueAt(i, 17).toString());
			float performanceIncentive=Float.parseFloat(table.getValueAt(i, 18).toString());
			float ltaPaid=Float.parseFloat(table.getValueAt(i, 19).toString());
			float retainersBonus=Float.parseFloat(table.getValueAt(i, 20).toString());
			
			float pf=Float.parseFloat(table.getValueAt(i, 21).toString());
			float pfArrear=Float.parseFloat(table.getValueAt(i, 22).toString());
			float professionalTax=Float.parseFloat(table.getValueAt(i, 23).toString());
			float incomeTax=Float.parseFloat(table.getValueAt(i, 24).toString());
			float d1Misc=Float.parseFloat(table.getValueAt(i, 25).toString());
			float d2Misc=Float.parseFloat(table.getValueAt(i, 26).toString());
			float canteen=Float.parseFloat(table.getValueAt(i, 27).toString());
			float sportClub=Float.parseFloat(table.getValueAt(i, 28).toString());
			float telephoneDeduction=Float.parseFloat(table.getValueAt(i, 29).toString());
			float medicalInsurance=Float.parseFloat(table.getValueAt(i, 30).toString());
			
			float totalIncome=basicearn+hra+medicalAllowance+medicalAllowanceArrear+conveyance+specialAllowance+
					otherAllowance+executiveAllowance+executiveAllowanceArrear+intressSubsidy+performanceIncentive+
					ltaPaid+retainersBonus+canteen;
			
			float totalDeduction=pf+pfArrear+professionalTax+incomeTax+d1Misc+d2Misc+sportClub+telephoneDeduction+medicalInsurance;
			
			float netIncome=totalIncome-totalDeduction;
			
			int net=(int)netIncome;
			String salaryInWords=new NumberToWord().convert(net);
			
			table.setValueAt(totalIncome, i, 31);
			table.setValueAt(totalDeduction, i, 32);
			table.setValueAt(netIncome, i, 7);
			
						
			newSalary.add(empid);
			newSalary.add(basicearn);
			newSalary.add(hra);
			newSalary.add(medicalAllowance);
			newSalary.add(medicalAllowanceArrear);
			newSalary.add(conveyance);
			newSalary.add(specialAllowance);
			newSalary.add(otherAllowance);
			newSalary.add(executiveAllowance);
			newSalary.add(executiveAllowanceArrear);
			newSalary.add(intressSubsidy);
			newSalary.add(performanceIncentive);
			newSalary.add(ltaPaid);
			newSalary.add(retainersBonus);
			
			newSalary.add(pf);
			newSalary.add(pfArrear);
			newSalary.add(professionalTax);
			newSalary.add(incomeTax);
			newSalary.add(d1Misc);
			newSalary.add(d2Misc);
			newSalary.add(canteen);
			newSalary.add(sportClub);
			newSalary.add(telephoneDeduction);
					
			newSalary.add(totalIncome);
			newSalary.add(totalDeduction);
			newSalary.add(netIncome);
						
			newSalary.add(monthofSalary2);
			newSalary.add(medicalInsurance);
			newSalary.add(salaryInWords);
			newSalary.add(dateofsal);
			
			new MasterGridDAO().saveBeforePrint(newSalary);
			checkYtdDetails(empid, monthofSalary2);
			SaveYtdDetails(empid, monthofSalary2);
			}
			
		}
	}

	 private String datePattern = "yyyy-MM-dd";
	 private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
	
	@Override
	public Object stringToValue(String text) throws ParseException {
		return dateFormatter.parseObject(text);
	}

	@Override
	public String valueToString(Object value) throws ParseException {
		 if (value != null) {
	            Calendar cal = (Calendar) value;
	            return dateFormatter.format(cal.getTime());
	        }
	         
	        return "";
		
	}
}
