import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

















import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import com.f4.dao.MasterGridDAO;
import com.mysql.jdbc.PreparedStatement;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class menu implements FocusListener,ActionListener{
	
	ArrayList columnNames,columnNamesSalary;
    ArrayList data,dataSalary ;
    JFrame frame;
    JButton btfind,btEdit,btEmployeeeList;
    JLabel selectMonth,imageLabelLogo,imageLabelLogo2;
    JTextField jtxt;
    JTable table,table2;
    String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
    String userid = "root";
    String password = "yungry";
    JScrollPane scrollPane2;
    JScrollPane scroll;
    JPanel jp3,jpButton;
    JFileChooser jChooser;
    
    
   // final JButton nextSalary,prevSalary;
    JComboBox jmonth,jyear;
    
    String []month={"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEPT","OCT","NOV","DEC"};
	String []year={"2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025"};
	int activetabindex=0;
	
	//----------------------------For Searching-----------------
	
	 private TableRowSorter<TableModel> rowSorter;

     private JTextField jtfFilter = new JTextField();
     private JTextField jtfFilterForReport = new JTextField();
   //-----------------------------For Searching-----------------
     
     
    public menu(int tabindexreceived) {
    	
    	jChooser=new JFileChooser();
    	
    	activetabindex=tabindexreceived;
    	
    	columnNames= new ArrayList();
        data= new ArrayList();
        columnNamesSalary=new ArrayList();
        dataSalary=new ArrayList();
        
        
        frame=new JFrame("Emplyee Details");
        btfind=new JButton("SEARCH");
        jtxt=new JTextField();
      
    
        
        
        
        selectMonth=new JLabel("Please Select Year & Month");
        
        jmonth=new JComboBox(month);
        jyear=new JComboBox(year);
        
        Calendar calendar = Calendar.getInstance();
		int dd = 2;// calendar.get(Calendar.DAY_OF_MONTH);
		int mm = calendar.get(Calendar.MONTH) + 1;
		int yy = calendar.get(Calendar.YEAR);
		int mmInactive=mm-1;
     
        String sql = "SELECT sr_no,employee_code,employee_name,bank_name,bank_account,designation,pan_number,"
        		+ "pf_number,date_of_joining,company_name,gender,email,contact FROM employee_details where status='Active' OR (status='Inactive' AND (YEAR(Inactive_date)='"+yy+"' AND MONTH(Inactive_date)='"+mmInactive+"' )) order by sr_no";

       
       
        try (Connection connection = DriverManager.getConnection( url, userid, password );
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery( sql ))
        {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            
            for (int i = 1; i <= columns; i++)
            {
                columnNames.add( md.getColumnName(i) );
            }

            
            while (rs.next())
            {
                ArrayList row = new ArrayList(columns);

                for (int i = 1; i <= columns; i++)
                {
                    row.add( rs.getObject(i) );
                }

                data.add( row );
            }
        }
        catch (SQLException e)
        {
            System.out.println( e.getMessage() );
        }

       
        Vector columnNamesVector = new Vector();
        Vector dataVector = new Vector();

        for (int i = 0; i < data.size(); i++)
        {
            ArrayList subArray = (ArrayList)data.get(i);
            Vector subVector = new Vector();
            for (int j = 0; j < subArray.size(); j++)
            {
                subVector.add(subArray.get(j));
            }
            dataVector.add(subVector);
        }

        for (int i = 0; i < columnNames.size(); i++ )
            columnNamesVector.add(columnNames.get(i));

         
        table = new JTable(dataVector, columnNamesVector)
        {
            public Class getColumnClass(int column)
            {
                for (int row = 0; row < getRowCount(); row++)
                {
                    Object o = getValueAt(row, column);

                    if (o != null)
                    {
                        return o.getClass();
                    }
                }
                

                return Object.class;
            }
            
            public boolean isCellEditable(int row, int column){ 
            	
	            return false;  
	        }
            
            
           
        };
       // table.getColumn("Button").setCellRenderer(new ButtonRenderer());
       // table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
      

       // bt1.setBounds(200,450,100,40);
     
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane scrollPane = new JScrollPane( table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        
        
        /*Pagination Commented
         * 
         * 
         * scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    	final JButton next = new JButton("next");
    	final JButton prev = new JButton("prev");
    	
    	ActionListener nav = new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			Rectangle rect = scrollPane.getVisibleRect();
    			JScrollBar  bar = scrollPane.getVerticalScrollBar();
    			int blockIncr = scrollPane.getViewport().getViewRect().height;
    			if (e.getSource() == next) {
    				bar.setValue(bar.getValue() + blockIncr);
    			} else if (e.getSource() == prev) {
    				bar.setValue(bar.getValue() - blockIncr);
    			}
    			scrollPane.scrollRectToVisible(rect);
    		}
    	};
    	
    	next.addActionListener(nav);
    	prev.addActionListener(nav);
    	prev.setBounds(30, 430, 100, 30);
    	next.setBounds(150, 430,100, 30);*/
    	scrollPane.setBounds(30,80,950,400);
    	
        
        
       // scrollPane.setBounds(30,40,900,400);
        
        //frame.add(bt1);
      
        //frame.add( scrollPane );
         
       
       frame.setSize(1024,730);
       frame.setVisible(true);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
     //  bt1.addActionListener(new ButtonClickListener());
    	
    	
    //////////////////////////////////////////////////////////////////////////////////////
    	ImageIcon icon = new ImageIcon("images/AMBIANCE.gif","this is a caption");
       
    	//This will create the title you see in the upper left of the window    
        frame.setTitle("Tabbed Pane"); 
       // frame.setSize(900,700); //set size so the user can "see" it
        
        //Here we are creating the object
        JTabbedPane jtp = new JTabbedPane();
       
        //This creates the template on the windowed application that we will be using
       frame.getContentPane().add(jtp);
       JPanel jp1 = new JPanel();//This will create the first tab
       JPanel jp2 = new JPanel();//This will create the second tab
       jp3=new JPanel();
       jp1.setLayout(null);
       jp2.setLayout(null);
       jp3.setLayout(null);
       
       jp1.setBackground(Color.LIGHT_GRAY);
       
     //code for logo & Co.Name
       imageLabelLogo=new JLabel(new ImageIcon("new/logo.png"));
       imageLabelLogo.setBounds(35, 0, 200, 100);
       
       imageLabelLogo2=new JLabel(new ImageIcon("new/logo.png"));
       imageLabelLogo2.setBounds(35, 0, 200, 100);
       jp1.add(imageLabelLogo2);
       
       
      
       
         //This creates a non-editable label, sets what the label will read
        //and adds the label to the first tab
      
      JLabel label1 = new JLabel();
      JButton addEmployee=new JButton("Add Employee");
      addEmployee.setActionCommand("ADD");
      JButton editEmployee=new JButton("Edit Employee");
      editEmployee.setActionCommand("EDIT");
      JButton salaryDetails=new JButton("Salary Details");
      salaryDetails.setActionCommand("SALARY");
      JButton editBase = new JButton("Edit Base Data");
      editBase.setActionCommand("EDITBASE");
      JButton leavesData = new JButton("Leaves Details");
      leavesData.setActionCommand("VIEWLEAVES");
      
      JButton salaryMaster = new JButton("Salary Master ");
      salaryMaster.setActionCommand("MASTERGRID");      
      
      JButton importData = new JButton("Import Employees");
      importData.setActionCommand("IMPORT");
      
      JButton importBaseData = new JButton("Import Base Data");
      importBaseData.setActionCommand("IMPORTBASE");
     
      addEmployee.setBounds(280, 500, 150, 40);
      editEmployee.setBounds(280, 580, 150, 40);
      salaryDetails.setBounds(640, 580, 150, 40);			
      editBase.setBounds(820, 500, 150, 40);				
      leavesData.setBounds(640, 500, 150, 40);
      
      salaryMaster.setBounds(460, 540, 150, 40);			
      importData.setBounds(100, 500, 150, 40);				
      importBaseData.setBounds(100, 580, 150, 40);			
      //panel for Button Background
      jpButton=new JPanel();
      jpButton.setBackground(Color.DARK_GRAY);
      jpButton.setBounds(30, 485, 950, 150);
      
      
      // label1.setText("This is Tab 1");
      	jp1.add(addEmployee);
      	jp1.add(editEmployee);
      	jp1.add(salaryDetails);
      	jp1.add(editBase);
      	jp1.add(leavesData);
      	jp1.add(salaryMaster);
      	jp1.add(importData);
      	jp1.add(importBaseData);
      	jp1.add(jpButton);
      
      //	jp1.add(prev);
    //  	jp1.add(next);
       	//jp1.add(label1);
       	jp1.add(scrollPane);
        jp1.add(imageLabelLogo);
      	

       //This adds the first and second tab to our tabbed pane object and names it
       jtp.addTab("Employee",icon, jp1);
       jtp.addTab("Admin Tools",icon, jp2);
     //  jtp.addTab("Monthly Reports",icon, jp3);
       
       //
       //scroll=new JScrollPane();
      // scroll.setBounds(30,40,900,400);
       //jp3.add(scrollPane2);
       jyear.setBounds(50, 110, 60, 20);
       jmonth.setBounds(110, 110, 50, 20);
       btfind.setBounds(170, 110, 100, 20);
       btfind.setActionCommand("find");
       btfind.addActionListener(this);
       
       
       // nextSalary = new JButton("next");
		//prevSalary = new JButton("prev");
		
		
		
		//--------------------Searching------------------
		JLabel jlsearchForReport=new JLabel("Search:");
		jlsearchForReport.setBounds(750, 110, 80, 20);
        jtfFilterForReport.setBounds(800,110, 100, 20);
        jp3.add(jtfFilterForReport);
        jp3.add(jlsearchForReport);
        jp3.add(imageLabelLogo);
      //--------------------Searching end------------------
		
		
		btEdit=new JButton("Add Salary Details");
		btEmployeeeList=new JButton("Employee List");
		
		btEdit.setActionCommand("editSalary");
	    btEmployeeeList.setActionCommand("list");
	    
	  //----------------Database Import Export Panel------------------  
	    ImageIcon imp = new ImageIcon("new/import.png");
	    ImageIcon exp = new ImageIcon("new/export.png");
	   
	    JButton	btExport=new JButton("Export Database",exp);
		JButton btImport=new JButton("Import Database",imp);
		
		
		JLabel dbLogo=new JLabel(new ImageIcon("new/dblogo.png"));
		
		btExport.setActionCommand("export");
		btImport.setActionCommand("import");
		
		btExport.addActionListener(this);
		btImport.addActionListener(this);
		
		dbLogo.setBounds(10,10,150,200);
		btExport.setBounds(450, 50,180, 40);
		btImport.setBounds(200, 50,180, 40);
		
		JPanel jpback=new JPanel(null);
		jpback.setBackground(Color.LIGHT_GRAY);
		jpback.setBounds(100,100,800,200);
	    
		jpback.add(dbLogo);
		jpback.add(btExport);
		//jpback.add(btImport);
		
		jp2.add(jpback);
		
		//End----------------Database Import Export Panel------------------ 
		
		
		//------------------------Salary Import Panel---------------------- 
		 ImageIcon impsal=new ImageIcon("new/impsal.png");
		JButton btimpsal=new JButton("Import Monthly Salary",impsal);
		
		
		JLabel excelLogo=new JLabel(new ImageIcon("new/excel.png"));
		JPanel jpback2=new JPanel(null);
		jpback2.setBackground(Color.LIGHT_GRAY);
		
		excelLogo.setBounds(10,10,150,200);
		jpback2.setBounds(100,400,390,200);
		btimpsal.setBounds(200, 50,180, 40);
		jpback2.add(btimpsal);
		jpback2.add(excelLogo);
		
		jp2.add(jpback2);
		
		btimpsal.setActionCommand("IMPORTSAL");
		
		btimpsal.addActionListener(this);
		
		ImageIcon inactive=new ImageIcon("new/impsal.png");
		JButton inactiveEmployee=new JButton("Active/Inactive Employee",inactive);
		
		
		JLabel emplogo=new JLabel(new ImageIcon("new/emp.png"));
		JPanel jpback3=new JPanel(null);
		jpback3.setBackground(Color.LIGHT_GRAY);
		
		emplogo.setBounds(10,10,150,200);
		jpback3.setBounds(510,400,390,200);
		inactiveEmployee.setBounds(200, 50,180, 40);
		jpback3.add(inactiveEmployee);
		jpback3.add(emplogo);
		
		jp2.add(jpback3);
		
		inactiveEmployee.setActionCommand("INACTIVE");
		inactiveEmployee.addActionListener(this);
		
		
		//End------------------------Salary Import Panel---------------------- 
		
		//	prevSalary.setBounds(30, 470, 100, 30);
		//	nextSalary.setBounds(150, 470,100, 30);
		
		btEdit.setBounds(600, 570,150, 40);
		btEmployeeeList.setBounds(780, 570,150, 40);
		
		//	jp3.add(prevSalary);
		//	jp3.add(nextSalary);
		
		jp3.add(btEdit);
		jp3.add(btEmployeeeList);
		jp3.setBackground(Color.LIGHT_GRAY);
       
       jp3.add(jyear);
       jp3.add(jmonth);
       jp3.add(btfind);
       
       //jmonth.addFocusListener(this);
       //This creates a new button called "Press" and adds it to the second tab
       // JButton test = new JButton("Press");
       // jp2.add(test);

       
       
      

     //------------For Searching in employee table-----------------
       
       rowSorter= new TableRowSorter<>(table.getModel());
 
           table.setRowSorter(rowSorter);

          // JPanel panel = new JPanel(new BorderLayout());
           JLabel jlsearch=new JLabel("Search:");
           jlsearch.setBounds(760, 40, 60, 20);
           jtfFilter.setBounds(830, 40, 130, 20);
           
           jp1.add(jtfFilter);
           jp1.add(jlsearch);

           
      //Panel For Background
           JPanel formBack=new JPanel();
           formBack.setBackground(Color.GRAY);
           formBack.setBounds(740, 30, 240, 40);
           jp1.add(formBack);
          

           jtfFilter.getDocument().addDocumentListener(new DocumentListener(){

               @Override
               public void insertUpdate(DocumentEvent e) {
                   String text = jtfFilter.getText();

                   if (text.trim().length() == 0) {
                       rowSorter.setRowFilter(null);
                   } else {
                       rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                   }
               }

               @Override
               public void removeUpdate(DocumentEvent e) {
                   String text = jtfFilter.getText();

                   if (text.trim().length() == 0) {
                       rowSorter.setRowFilter(null);
                   } else {
                       rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                   }
               }

               @Override
               public void changedUpdate(DocumentEvent e) {
                   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
               }

           });
       
         //------------------------------------- Searching Ends here-----------------------------------------
           
         //This is an Action Listener which reacts to clicking on 
        //the test button called "Press"
        ButtonHandler phandler = new ButtonHandler();
        addEmployee.addActionListener(phandler);
        editEmployee.addActionListener(phandler);
        salaryDetails.addActionListener(phandler);
        editBase.addActionListener(phandler);
        leavesData.addActionListener(phandler);
        salaryMaster.addActionListener(phandler);
        importData.addActionListener(phandler);
        importBaseData.addActionListener(phandler);
               
        jtp.setSelectedIndex(activetabindex);
        frame.setVisible(true); //otherwise you won't "see" it 
    }
    
    //This is the internal class that defines what the above Action Listener
    //will do when the button is pressed.
    class ButtonHandler implements ActionListener{
           public void actionPerformed(ActionEvent e){
                   //JOptionPane.showMessageDialog(null, "I've been pressed", "What happened?", JOptionPane. INFORMATION_MESSAGE);
        	   String str=e.getActionCommand();
        	   if(str=="ADD")
        	   {
        	   new AddEmployee();
        	   frame.setVisible(false);
        	   
        	   }
        	   
        	    if(str=="EDIT"){
        	       int x=table.getSelectedRow();
        	      
        	       x++;
        	       if(x<1)
        	       {
        	    	   JOptionPane.showMessageDialog(null, "Please Select Employee from abouve list to Edit !","Alert..!!!",
        	    			   JOptionPane.OK_CANCEL_OPTION);
	                    
        	       }
        	       else
        	       {
        	       x--;
        	       String emp=table.getValueAt(x, 1).toString();
        		   new EditRecoard(emp);
        		   frame.setVisible(false);
        	       }
        	   }
        	    
        	    
        	    
        	    if(str=="EDITBASE"){
        			int x=table.getSelectedRow();
        			
         	       
         	       if(x<0)
         	       {
         	    	   JOptionPane.showMessageDialog(null, "Please Select Employee from abouve list to Edit Base Data!","Alert..!!!",
         	    			   JOptionPane.OK_CANCEL_OPTION);
                         
         	       }
         	       else
         	       {
         	    	  String emp_code=(String) table.getValueAt(x, 1);
         	    	//  System.out.println(emp_code);
         	    	 frame.setVisible(false);   
         		   new EditBaseData(emp_code);
         		       }
        		}
        	   
        	    if(str=="SALARY"&&table.getSelectedRowCount()>0){

        	    int serial_no=table.getSelectedRow();
        	    String empcode=table.getValueAt(serial_no, 1).toString();
        	    serial_no++; 
        	
        	    		 new Salary_Details(empcode);
        	    		 frame.setVisible(false);
        		 
        	   }
        	    
        	    else if(str=="SALARY"&&table.getSelectedRowCount()<=0){
        	    	
        	    	JOptionPane.showMessageDialog(null, "Please Select Employee from abouve list to View Salary Details !","Alert..!!!",
     	    			   JOptionPane.OK_CANCEL_OPTION);
	                    
        	    }
        	    
        	    if(str=="VIEWLEAVES"){
         	       int x=table.getSelectedRow();
         	       x++;
         	       if(x<1)
         	       {
         	    	   JOptionPane.showMessageDialog(null, "Please Select Employee from abouve list to Edit !","Alert..!!!",
         	    			   JOptionPane.OK_CANCEL_OPTION);
 	                    
         	       }
         	       else
         	       {
         	    	   
         	    	   x--;
         	    	   String emp=table.getValueAt(x, 1).toString();
         	    	   
         	    	   new LeavesRecoard(emp);
         	    	   frame.setVisible(false);
         	       }
         	   }
        	    
        	    if(str=="MASTERGRID"){
        	    	frame.setVisible(false);
          	     new MasterGrid();          	      
          	   }
        	    
        	    if(str=="IMPORT"){
        	    
        	    	//---------------------importing employee details-----------------//
        	    	
        	        try{
        	            Class.forName("com.mysql.jdbc.Driver");
        	            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/salarySlipDatabase","root","yungry");
        	            con.setAutoCommit(false);
        	            PreparedStatement pstm = null ;
        	            jChooser.showOpenDialog(null);
        	            String file = jChooser.getSelectedFile().getAbsolutePath();
        	            FileInputStream input = new FileInputStream(file);
        	            POIFSFileSystem fs = new POIFSFileSystem( input );
        	            HSSFWorkbook wb = new HSSFWorkbook(fs);
        	            HSSFSheet sheet = wb.getSheetAt(0);
        	            HSSFRow row;
        	            int count=0;
        	            for(int i=1; i<=sheet.getLastRowNum(); i++){
        	                row = sheet.getRow(i);
        	                int id = (int) row.getCell((short)0).getNumericCellValue();
        	                
        	                String empid = row.getCell((short)1).getStringCellValue();
        	                
        	                String name = row.getCell((short)2).getStringCellValue();
        	                
        	                String bank = row.getCell((short)3).getStringCellValue();
        	                
        	                String bankAc = row.getCell((short)4).getStringCellValue();
        	                
        	                String desig = row.getCell((short)5).getStringCellValue();
        	                
        	                String panNo = row.getCell((short)6).getStringCellValue();
        	                
        	                String pfNo = row.getCell((short)7).getStringCellValue();
        	                
        	                Date date = row.getCell((short)8).getDateCellValue();
        	                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	          		String sDate= sdf.format(date);
        	          		
        	          		 String compName = row.getCell((short)9).getStringCellValue();
        	          		 compName=compName.toUpperCase();
        	          		 
        	          		 String gen = row.getCell((short)10).getStringCellValue();
        	          		 
        	          		 String email=row.getCell((short)11).getStringCellValue();
        	          		 
        	          		 String contact=row.getCell((short)12).getStringCellValue();
        	          		
        	          		 String checkExcistanceSql="SELECT COUNT(*) AS CNT FROM employee_details where employee_code='"+empid+"'";
        	          		try (Connection connection = DriverManager.getConnection( url, userid, password );
        	        	            Statement stmt = connection.createStatement();){
        	        				ResultSet rs=stmt.executeQuery(checkExcistanceSql);
        	        				if(rs.next()){
        	        				count=rs.getInt("CNT");
        	        
        	        				}
        	        			}
        	        	        catch (SQLException ex)
        	        	        {
        	        	            System.out.println(ex.getMessage());
        	        	        }
        	          		 
        	          		String sql=null;
        	          		 if(count<=0){
        	          			 sql = "INSERT INTO employee_details VALUES("+id+",'"+empid+"','"+name+"','"+bank+"','"+bankAc+"','"+desig+"','"+panNo+"','"+pfNo+"','"+sDate+"','"+compName+"','"+gen+"','"+email+"','"+contact+"','Active','1999-01-01')";
        	          		 
        	          		 }
        	          		 else{
        	          			sql ="SELECT COUNT(*) FROM employee_details";
        	          		 }
        	                pstm = (PreparedStatement) con.prepareStatement(sql);
        	                pstm.execute();
        	                System.out.println("Import rows "+i);
        	            }
        	            con.commit();
        	            pstm.close();
        	            con.close();
        	            input.close();
        	          //  ImageIcon icon = new ImageIcon(login.class.getResource("new/success.jpg"));
	                	JOptionPane.showMessageDialog(null,
	                             new JLabel("Data Imported Successfully!!!", JLabel.LEFT),
	                             "Success...!!",JOptionPane.INFORMATION_MESSAGE);
        	        }catch(ClassNotFoundException ex){
        	            System.out.println(ex);
        	        }catch(SQLException ex){
        	            System.out.println(ex);
        	        }catch(IOException ioe){
        	            System.out.println(ioe);
        	        }
        	    	
        	    	
        	    	
        	    	
        	    frame.setVisible(false);
          	    new menu(0);          	      
          	   }
        	    
        	  //---------------------importing employee details end here-----------------//
        	    if(str=="IMPORTBASE"){
        	    
        	 //---------------------------importing base data -------------------------//
        	    	
        	        try{
        	            Class.forName("com.mysql.jdbc.Driver");
        	            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/salarySlipDatabase","root","yungry");
        	            con.setAutoCommit(false);
        	            PreparedStatement pstm = null ;
        	            jChooser.showOpenDialog(null);
        	            String file = jChooser.getSelectedFile().getAbsolutePath();
        	            FileInputStream input = new FileInputStream(file);
        	            POIFSFileSystem fs = new POIFSFileSystem( input );
        	            HSSFWorkbook wb = new HSSFWorkbook(fs);
        	            HSSFSheet sheet = wb.getSheetAt(0);
        	            HSSFRow row;
        	            int count=0;
        	            for(int i=1; i<=sheet.getLastRowNum(); i++){
        	                row = sheet.getRow(i);
        	                int id = (int) row.getCell((short) 0).getNumericCellValue();
        	                
        	                String empid = row.getCell((short)1).getStringCellValue();
        	                
        	                double bas_sal =row.getCell((short)2).getNumericCellValue();
        	                
        	                double medical_allowance = row.getCell((short)3).getNumericCellValue();
        	                
        	                double medical_allowance_arrear = row.getCell((short)4).getNumericCellValue();
        	                
        	                double conveyance = row.getCell((short)5).getNumericCellValue();
        	                
        	                double special_alowance = row.getCell((short)6).getNumericCellValue();
        	                
        	                double other_aalowance = row.getCell((short)7).getNumericCellValue();
        	                
        	                double executive_allowance = row.getCell((short)8).getNumericCellValue();
        	               
        	                double executive_allowance_arrear = row.getCell((short)9).getNumericCellValue();
        	          		 
        	                double intress_subsidy = row.getCell((short)10).getNumericCellValue();
        	                
        	                double performance_incentive = row.getCell((short)11).getNumericCellValue();
        	                
        	                double lta_paid = row.getCell((short)12).getNumericCellValue();
        	                
        	                double retainers_bonus = row.getCell((short)13).getNumericCellValue();
        	                
        	                double pf = row.getCell((short)14).getNumericCellValue();
        	                
        	                double pf_arrear = row.getCell((short)15).getNumericCellValue();
        	                
        	                double pt = row.getCell((short)16).getNumericCellValue();
        	                
        	                double income_tax = row.getCell((short)17).getNumericCellValue();
        	                
        	                double d1_misc = row.getCell((short)18).getNumericCellValue();
        	                
        	                double d2_misc = row.getCell((short)19).getNumericCellValue();
        	                
        	                double canteen = row.getCell((short)20).getNumericCellValue();
        	                
        	                double sport_club = row.getCell((short)21).getNumericCellValue();
        	                
        	                double telephone = row.getCell((short)22).getNumericCellValue();
        	                
        	                double medical_insurance_deduction=row.getCell((short)23).getNumericCellValue();
        	                
        	          		String checkSql="SELECT COUNT(*) AS CNT FROM basedata where employee_code='"+empid+"'";
        	          		
        	          		try (Connection connection = DriverManager.getConnection( url, userid, password );
        	        	            Statement stmt = connection.createStatement();){
        	        				ResultSet rs=stmt.executeQuery(checkSql);
        	        				if(rs.next()){
        	        				count=rs.getInt("CNT");
        	        
        	        				}
        	        			}
        	        	        catch (SQLException ex)
        	        	        {
        	        	            System.out.println(ex.getMessage());
        	        	        }
        	          				
        	          		String sql=null;
        	          		if(count<=0){
        	                sql = "INSERT INTO basedata VALUES("+id+",'"+empid+"','"+bas_sal+"',"
        	                		+ "'"+medical_allowance+"','"+medical_allowance_arrear+"','"+conveyance+"','"+special_alowance+"',"
        	                		+ "'"+other_aalowance+"','"+executive_allowance+"','"+executive_allowance_arrear+"','"+intress_subsidy+"',"
        	                		+ "'"+performance_incentive+"','"+lta_paid+"','"+retainers_bonus+"','"+pf+"','"+pf_arrear+"','"+pt+"',"
        	                				+ "'"+income_tax+"','"+d1_misc+"','"+d2_misc+"','"+canteen+"','"+sport_club+"','"+telephone+"','"+medical_insurance_deduction+"')";
        	          		}
        	          		else{
        	          			sql="SELECT COUNT(*) FROM basedata";
        	          		}
        	                pstm = (PreparedStatement) con.prepareStatement(sql);
        	                pstm.execute();
        	                System.out.println("Import rows "+i);
        	            }
        	            con.commit();
        	            pstm.close();
        	            con.close();
        	            input.close();
        	           // ImageIcon icon = new ImageIcon(login.class.getResource("new/success.jpg"));
	                	JOptionPane.showMessageDialog(null,
	                             new JLabel("Data Imported Successfully!!!", JLabel.LEFT),
	                             "Success...!!", JOptionPane.INFORMATION_MESSAGE);
        	        }catch(ClassNotFoundException ex){
        	            System.out.println(ex);
        	        }catch(SQLException ex){
        	            System.out.println(ex);
        	        }catch(IOException ioe){
        	            System.out.println(ioe);
        	        }
        	    	
        	    	
        	    	
        	    	
        	             	      
          	   }
        	  //---------------------------importing base data ends here-------------------------//
           }

		
    }

    //example usage
     public static void main (String []args){
    	 
    	
    	 new menu(0);
    	 
    }

	@Override
	public void focusGained(FocusEvent e) {
		
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		
	}

	
	

	@Override
	public void actionPerformed(ActionEvent e) {
//Search Button pressed
		if(e.getActionCommand()=="find"){
			report();
		}

		if(e.getActionCommand()=="list"){
			new menu(0);
			frame.setVisible(false);
		}
		
		if(e.getActionCommand()=="import"){
			
			String dbUserName="root";
			String dbPassword="yungry";
			String source="";
			JFileChooser jch=new JFileChooser();
			jch.showOpenDialog(null);
			source=jch.getSelectedFile().getAbsolutePath();
			System.out.println(source);
 			
			String[] restoreCmd = new String[]{"C:/Program Files/MySQL/MySQL Server 5.6/bin/mysql ", "--user=" + dbUserName, "--password=" + dbPassword, "-e", "source " + source};
			 
	        Process runtimeProcess;
	        try {
	 
	            runtimeProcess = Runtime.getRuntime().exec(restoreCmd);
	            int processComplete = runtimeProcess.waitFor();
	 
	            if (processComplete == 0) {
	            	 JOptionPane.showMessageDialog(null, "Data Restored Successfully !!!","Success !!!",
	     	    			   JOptionPane.INFORMATION_MESSAGE);
	            } else {
	            	JOptionPane.showMessageDialog(null, "Data Cannot be Restored !!!","Error..!!!",
	     	    			   JOptionPane.ERROR_MESSAGE);
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
		}
		
		if(e.getActionCommand()=="export"){
		
			Process p = null;
		    try {
		    	
		    	File file = new File("D:\\SalaryDatabaseBackup");
		    	if (!file.exists()) {
		    		if (file.mkdir()) {
		    			  Runtime runtime = Runtime.getRuntime();
		  		        p = runtime.exec("C:/Program Files/MySQL/MySQL Server 5.6/bin/mysqldump -uroot -pyungry --add-drop-database -B salary -r " + "D://SalaryDatabaseBackup/" + "salaryDatabaseBackup" + ".bak");
		  	
		    		}
		    	}
		        Runtime runtime = Runtime.getRuntime();
		        p = runtime.exec("C:/Program Files/MySQL/MySQL Server 5.6/bin/mysqldump -uroot -pyungry --add-drop-database -B salary -r " + "D://SalaryDatabaseBackup/" + "salaryDatabaseBackup" + ".bak");
	
		        int processComplete = p.waitFor();

		        if (processComplete == 0) {

		            System.out.println("Backup created successfully!");
		            JOptionPane.showMessageDialog(null, "Data Backup Successfull !!!\n Backup File has been Stored At D:/SalaryDatabaseBackup ","Success !!!",
     	    			   JOptionPane.INFORMATION_MESSAGE);
	                    

		        } else {
		        	JOptionPane.showMessageDialog(null, "Could not creat backup !!!","Error..!!!",
     	    			   JOptionPane.ERROR_MESSAGE);
	                    
		        }


		    } catch (Exception ex) {
		        ex.printStackTrace();
		    }
			
			
			
		}
		
		
		
		if(e.getActionCommand()=="INACTIVE"){
			
			frame.setVisible(false);
			new InactiveEmployee();
		}
		
		if(e.getActionCommand()=="IMPORTSAL"){
			
			System.out.println("Importing");
			
	        try{
	            Class.forName("com.mysql.jdbc.Driver");
	            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/salarySlipDatabase","root","yungry");
	            con.setAutoCommit(false);
	            PreparedStatement pstm = null ;
	            jChooser.showOpenDialog(null);
	            String file = jChooser.getSelectedFile().getAbsolutePath();
	            FileInputStream input = new FileInputStream(file);
	            POIFSFileSystem fs = new POIFSFileSystem( input );
	            HSSFWorkbook wb = new HSSFWorkbook(fs);
	            HSSFSheet sheet = wb.getSheetAt(0);
	            HSSFRow row;
	            for(int i=1; i<=sheet.getLastRowNum(); i++){
	            	
	            	int id = 0;
	            	String salId="select MAX(salary_id) AS SALNO from salary_details";
	            	Statement stmt = con.createStatement();
	    				ResultSet rs=stmt.executeQuery(salId);
	    				if(rs.next()){
	    				id=rs.getInt("SALNO");
	    				id++;
	    				}
		            
		                
	                row = sheet.getRow(i);
	              //  int id = (int) row.getCell((short) 0).getNumericCellValue();
	                
	                String empid = row.getCell((short)1).getStringCellValue();
	             	                
	                String monthOfSalary = row.getCell((short)2).getStringCellValue();
	             	                
	                Double basicEarn = row.getCell((short)3).getNumericCellValue();
	                
	                Double basicEarnArrear = row.getCell((short)4).getNumericCellValue();
	                
	                Double payableDays = row.getCell((short)5).getNumericCellValue();
	                
	                Double paidDays = row.getCell((short)6).getNumericCellValue();
	                
	                Double hra = row.getCell((short)7).getNumericCellValue();
	                
	                Double hraArrear = row.getCell((short)8).getNumericCellValue();
	              
	                Double specialAllownce = row.getCell((short)9).getNumericCellValue();
	          		 
	                Double otherAllowance = row.getCell((short)10).getNumericCellValue();
	          		 
	                Double medicalAllowance=row.getCell((short)11).getNumericCellValue();
	                
	                Double medicalAllowanceArrear=row.getCell((short)12).getNumericCellValue();
	          		 
	                Double performanceIncentive=row.getCell((short)13).getNumericCellValue();
	                
	                Double providentFund = row.getCell((short)20).getNumericCellValue();
	          		 
	                Double providentFundArrear = row.getCell((short)21).getNumericCellValue();
	          		 
	                Double p_tax=row.getCell((short)22).getNumericCellValue();
	                
	                Double incomeTax=row.getCell((short)23).getNumericCellValue();
	          		 
	                Double d1misc=row.getCell((short)24).getNumericCellValue();
	          			                
	                Double d2misc = row.getCell((short)25).getNumericCellValue();
	          		 
	                Double canteen=row.getCell((short)26).getNumericCellValue();
	                
	                Double total=row.getCell((short)30).getNumericCellValue();
	                
	                Double totalDeduction=row.getCell((short)31).getNumericCellValue();
	          		 
	                Double netSalary=row.getCell((short)32).getNumericCellValue();
	          		 
	                Double conveyance=row.getCell((short)14).getNumericCellValue();
		                
	                String netSalword = row.getCell((short)33).getStringCellValue();
	                
	                Double executiveAllowance=row.getCell((short)15).getNumericCellValue();
	          		 
	                Double executiveAllowanceArrear=row.getCell((short)16).getNumericCellValue();
	                
	                Double intressSubsidy=row.getCell((short)17).getNumericCellValue();
	          		 
	                Double lta_paid=row.getCell((short)18).getNumericCellValue();
	          		 
	                Double retainersBonus=row.getCell((short)19).getNumericCellValue();
		                
	                Double SportClubDeduction = row.getCell((short)27).getNumericCellValue();
	          		 
	                Double TelephoneDeduction=row.getCell((short)28).getNumericCellValue();
	                
	                Double medicalInsuranceDeduction=row.getCell((short)29).getNumericCellValue();
	                
	                
	                
	                String sql="";
	                
	                String check="select * from salary_details where employee_code='"+empid+"' AND month_of_salary='"+monthOfSalary+"'";
	                try (Connection connection = DriverManager.getConnection( url, userid, password );
	        	            Statement stm = connection.createStatement();){
	        				ResultSet rs1=stm.executeQuery(check);
	        				if(rs1.next())
	        				{
	        				    sql = "UPDATE salary_details set basic_earn='"+basicEarn+"',"
	        			                + "basic_earn_arrear='"+basicEarnArrear+"',payable_days='"+payableDays+"',paid_days='"+paidDays+"',hra='"+hra+"',hra_arrear='"+hraArrear+"',special_allowance='"+specialAllownce+"',"
	        			                + "other_allowance='"+otherAllowance+"',medical_allowance='"+medicalAllowance+"',medical_allowance_arrear='"+medicalAllowanceArrear+"',performance_incentive='"+performanceIncentive+"',providant_fund='"+providentFund+"',"
	        			                + "providant_fund_arrear='"+providentFundArrear+"',p_tax='"+p_tax+"',income_tax='"+incomeTax+"',d1_misc_deduction='"+d1misc+"',d2_misc_deduction='"+d2misc+"',canteen_deduction='"+canteen+"',total='"+total+"',"
	        			                + "total_deduction='"+totalDeduction+"',net_salary='"+netSalary+"',convenyance='"+conveyance+"',net_sal_word='"+netSalword+"',executive_allowance='"+executiveAllowance+"',executive_allowance_arrear='"+executiveAllowanceArrear+"',intress_subsidy='"+intressSubsidy+"',"
	        			                + "lta_paid='"+lta_paid+"',retainers_bonus='"+retainersBonus+"',sport_club_deduction='"+SportClubDeduction+"',telephone_deduction='"+TelephoneDeduction+"',medical_insurance_deduction='"+medicalInsuranceDeduction+"' where employee_code='"+empid+"' AND month_of_salary='"+monthOfSalary+"'";
	        				}
	        				
	        				else{
	        					
	        				    sql = "INSERT INTO salary_details VALUES("+id+",'"+empid+"','"+monthOfSalary+"','"+basicEarn+"',"
	        			                + "'"+basicEarnArrear+"','"+payableDays+"','"+paidDays+"','"+hra+"','"+hraArrear+"','"+specialAllownce+"',"
	        			                + "'"+otherAllowance+"','"+medicalAllowance+"','"+medicalAllowanceArrear+"','"+performanceIncentive+"','"+providentFund+"',"
	        			                + "'"+providentFundArrear+"','"+p_tax+"','"+incomeTax+"','"+d1misc+"','"+d2misc+"','"+canteen+"','"+total+"',"
	        			                + "'"+totalDeduction+"','"+netSalary+"','"+conveyance+"','"+netSalword+"','"+executiveAllowance+"','"+executiveAllowanceArrear+"','"+intressSubsidy+"',"
	        			                + "'"+lta_paid+"','"+retainersBonus+"','"+SportClubDeduction+"','"+TelephoneDeduction+"','"+medicalInsuranceDeduction+"')";
	        					
	        				}
	                }
	                
	    
	                
	            
	                
	                
	                
	                
	                pstm = (PreparedStatement) con.prepareStatement(sql);
	                pstm.execute();
	                System.out.println("Import rows "+i);
	                con.commit();
	                checkYtdDetails(empid, monthOfSalary);
	                SaveYtdDetails(empid, monthOfSalary);
	               
	               
	            }
	            
	           pstm.close();
	           con.close();
	            input.close();
	           // ImageIcon icon = new ImageIcon(login.class.getResource("new/success.jpg"));
            	JOptionPane.showMessageDialog(null,
                         new JLabel("Data Imported Successfully!!!", JLabel.LEFT),
                         "Success...!!", JOptionPane.INFORMATION_MESSAGE);
	        }
	        
	        catch(com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException ex){
	        	
	        	
	        	JOptionPane.showMessageDialog(null, "Please check the Excel File & Confirm that \n The Salary details filled for Employee code's are exist in the Employee Details  !!!","Error In Excel File...!!",
  	    			   JOptionPane.ERROR_MESSAGE);
	        	System.out.println(ex);
	        }
	        catch(ClassNotFoundException ex){
	            System.out.println(ex);
	        }catch(SQLException ex){
	            System.out.println(ex);
	        }catch(IOException ioe){
	            System.out.println(ioe);
	        }
	    	
	     
			
			
		}

		
		
		
		//Add Salary Button Pressed
		if(e.getActionCommand()=="editSalary"){
			
			new MasterGrid();
			frame.setVisible(false);
			/*int x=table2.getSelectedRow();
  	       	int y=x+1;
  	       
  	      
  	       if(y<1)
  	       {
  	    	   JOptionPane.showMessageDialog(null, "Please Select Salary from abouve list to Edit !","Alert..!!!",
  	    			   JOptionPane.OK_CANCEL_OPTION);
                  
  	       }
  	       else
  	       {
  	    	
  	    	 int salid= (int) table2.getValueAt(x,0);
  	    	 System.out.println(salid);
         	
         	String empcode=(String)table2.getValueAt(x,1);
         	 System.out.println(empcode);									
         		//new EditSalaryDetailsFromMonthlyReport(empcode,salid);
         		//frame.setVisible(false);
  	    	   
  		   
  		 //  frame.setVisible(false);
  	       }*/
		}
	}
	
void report(){
		
		dataSalary.clear();
		columnNamesSalary.clear();
		
		
		StringBuffer sbMonthofSalary=new StringBuffer();
		String partyear,partmonth;
		
		partyear=(String) jyear.getSelectedItem();
		sbMonthofSalary.append(partyear);
		
		sbMonthofSalary.append("-");
		
		partmonth=(String) jmonth.getSelectedItem();
		sbMonthofSalary.append(partmonth);
		System.out.println(sbMonthofSalary);
		
		
		String sql = "SELECT * FROM salary_details where month_of_salary='"+sbMonthofSalary+"'";

	       
	       
        try (Connection connection = DriverManager.getConnection( url, userid, password );
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery( sql ))
        {
        	
        	
        	
        	
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            
            for (int i = 1; i <= columns; i++)
            {
                columnNamesSalary.add(md.getColumnName(i));
               
            }

            
            while (rs.next())
            {
                ArrayList row = new ArrayList(columns);

                for (int i = 1; i <= columns; i++)
                {
                    row.add( rs.getObject(i) );
                }
              
                dataSalary.add( row );
            }
        }
        catch (SQLException e)
        {
            System.out.println( e.getMessage() );
        }

       
        Vector columnNamesVector2 = new Vector();
        Vector dataVector2 = new Vector();

        for (int i = 0; i < dataSalary.size(); i++)
        {
            ArrayList subArray = (ArrayList)dataSalary.get(i);
            Vector subVector = new Vector();
            for (int j = 0; j < subArray.size(); j++)
            {
                subVector.add(subArray.get(j));
            }
            dataVector2.add(subVector);
        }

        for (int i = 0; i < columnNamesSalary.size(); i++ )
            columnNamesVector2.add(columnNamesSalary.get(i));

         
        table2 = new JTable(dataVector2, columnNamesVector2)
        {
        	public Class getColumnClass(int column)
            {
                for (int row = 0; row < getRowCount(); row++)
                {
                    Object o = getValueAt(row, column);

                    if (o != null)
                    {
                        return o.getClass();
                    }
                }
                

                return Object.class;
            }
            
            public boolean isCellEditable(int row, int column){ 
            	
	            return true;  
	        }
            
            
           
        };
       // table.getColumn("Button").setCellRenderer(new ButtonRenderer());
       // table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
      

       // bt1.setBounds(200,450,100,40);
     
        table2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
       JScrollPane scrollPane2 = new JScrollPane( table2,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
    		   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        
        
       /* Pagination Commented 
        * 
        * scrollPane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    	ActionListener nav = new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			Rectangle rect = scrollPane2.getVisibleRect();
    			JScrollBar  bar = scrollPane2.getVerticalScrollBar();
    			int blockIncr = scrollPane2.getViewport().getViewRect().height;
    			if (e.getSource() == nextSalary) {
    				bar.setValue(bar.getValue() + blockIncr);
    			} else if (e.getSource() == prevSalary) {
    				bar.setValue(bar.getValue() - blockIncr);
    			}
    			scrollPane2.scrollRectToVisible(rect);
    		}
    	};
    	
    	nextSalary.addActionListener(nav);
    	prevSalary.addActionListener(nav);*/
       	scrollPane2.setBounds(30,150,900,400);
    	btEdit.addActionListener(this);
 	    btEmployeeeList.addActionListener(this);
 	   
 	    
    	jp3.add(scrollPane2);
    	
    	//------------For Searching in employee table-----------------
        
        rowSorter= new TableRowSorter<>(table2.getModel());
  
            table2.setRowSorter(rowSorter);

            jtfFilterForReport.getDocument().addDocumentListener(new DocumentListener(){

                @Override
                public void insertUpdate(DocumentEvent e) {
                    String text = jtfFilterForReport.getText();

                    if (text.trim().length() == 0) {
                        rowSorter.setRowFilter(null);
                    } else {
                        rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                    }
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    String text = jtfFilterForReport.getText();

                    if (text.trim().length() == 0) {
                        rowSorter.setRowFilter(null);
                    } else {
                        rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                    }
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            });
        
          //------------------------------------- Searching Ends here-----------------------------------------
    	
    	
    	
    	
    	
	}


public void SaveYtdDetails(String empid, String monthofSalary2) {
	
	
	StringBuffer currentmon=new StringBuffer();
	currentmon.append(monthofSalary2);
	
	String monarr[]={"APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC","JAN","FEB","MAR",};
	int year =Integer.parseInt( monthofSalary2.substring(0, 4));
	StringBuffer sbmonthofsal=new StringBuffer();
	
	String mon=monthofSalary2.substring(5, 8);
	
	ArrayList ytdsalary=new ArrayList();
	ArrayList salofmon=new ArrayList();
	int ytdbasicearn=0,ytdbasicearnarear=0,ytdhra=0,ytdhraarrear=0,ytdspecialallowance=0,ytdotherallowance=0,ytdmedicalallowance=0,
		ytdmedicalallowancearrear=0,ytdperformanceincentive=0,ytdprovidenetFund=0,ytdprovidenetFundarrear=0,ytdproftax=0,
		ytdincometax=0,ytdd1misc=0,ytdd2misc=0,ytdcanteen=0,ytdconveyance=0,ytdtotal=0,ytdtotaldeduction=0,ytdexecutiveallowance=0,
		ytdexecutiveallowancearrear=0,ytdintressSubsidy=0,ytdltapaid=0,ytdretainersbonus=0,ytdsportclubdeduction=0,ytdtelephonededuction=0,ytdmedicalinsurance=0;
	
	int basicearn= 0,basicearnarear=0,hra=0,hraarrear=0,specialallowance=0,otherallowance=0,medicalallowance=0,medicalallowancearrear=0,
		performanceincentive=0,providenetFund=0,providenetFundarrear=0,proftax=0,incometax=0,d1misc=0,d2misc=0,canteen=0,conveyance=0,
		total=0,totaldeduction=0,executiveallowance=0,executiveallowancearrear=0,intressSubsidy=0,ltapaid=0,retainersbonus=0,
				sportclubdeduction=0,telephonededuction=0,medicalinsurancededuction = 0;
	
	
	if(mon.equals("APR")||mon.equals("MAY")||mon.equals("JUN")||mon.equals("JUL")||mon.equals("AUG")||mon.equals("SEP")||
			mon.equals("OCT")||mon.equals("NOV")||mon.equals("DEC"))
	{	
for(int i=0;i<12;i++){	
	
	if(i>=0&&i<=8)
	{
		sbmonthofsal.append(year);
		sbmonthofsal.append("-");
		sbmonthofsal.append(monarr[i]);
		salofmon=new MasterGridDAO().getSal(sbmonthofsal,empid);
	    
		basicearn=(int) salofmon.get(0);
	    basicearnarear=(int) salofmon.get(1);
	    hra=(int) salofmon.get(2);
	    hraarrear=(int) salofmon.get(3);
	    specialallowance=(int) salofmon.get(4);
	    otherallowance=(int) salofmon.get(5);
	    medicalallowance=(int) salofmon.get(6);
	    medicalallowancearrear=(int) salofmon.get(7);
	    performanceincentive=(int) salofmon.get(8);
	    providenetFund=(int) salofmon.get(9);
	    providenetFundarrear=(int) salofmon.get(10);
	    proftax=(int) salofmon.get(11);
	    incometax=(int) salofmon.get(12);
	    d1misc=(int) salofmon.get(13);
	    d2misc=(int) salofmon.get(14);
	    canteen=(int) salofmon.get(15);
	    conveyance=(int) salofmon.get(16);
	    total=(int) salofmon.get(17);
	    totaldeduction=(int) salofmon.get(18);
	    executiveallowance=(int) salofmon.get(19);
	    executiveallowancearrear=(int) salofmon.get(20);
	    intressSubsidy=(int) salofmon.get(21);
	    ltapaid=(int) salofmon.get(22);
	    retainersbonus=(int) salofmon.get(23);
	    sportclubdeduction=(int) salofmon.get(24);
	    telephonededuction=(int) salofmon.get(25);
	    medicalinsurancededuction=(int)salofmon.get(26);
	    
	    
		System.out.println(sbmonthofsal+" "+basicearn);
    }
	
	if(i>=9&&i<=11)
	{	
		int cyear=year+1;
		sbmonthofsal.append(cyear);
		sbmonthofsal.append("-");
		sbmonthofsal.append(monarr[i]);
		salofmon=new MasterGridDAO().getSal(sbmonthofsal,empid);
		
		basicearn=(int) salofmon.get(0);
		basicearnarear=(int) salofmon.get(1);
		hra=(int) salofmon.get(2);
		hraarrear=(int) salofmon.get(3);
	    specialallowance=(int) salofmon.get(4);
	    otherallowance=(int) salofmon.get(5);
	    medicalallowance=(int) salofmon.get(6);
	    medicalallowancearrear=(int) salofmon.get(7);
	    performanceincentive=(int) salofmon.get(8);
	    providenetFund=(int) salofmon.get(9);
	    providenetFundarrear=(int) salofmon.get(10);
	    proftax=(int) salofmon.get(11);
	    incometax=(int) salofmon.get(12);
	    d1misc=(int) salofmon.get(13);
	    d2misc=(int) salofmon.get(14);
	    canteen=(int) salofmon.get(15);
	    conveyance=(int) salofmon.get(16);
	    total=(int) salofmon.get(17);
	    totaldeduction=(int) salofmon.get(18);
	    executiveallowance=(int) salofmon.get(19);
	    executiveallowancearrear=(int) salofmon.get(20);
	    intressSubsidy=(int) salofmon.get(21);
	    ltapaid=(int) salofmon.get(22);
	    retainersbonus=(int) salofmon.get(23);
	    sportclubdeduction=(int) salofmon.get(24);
	    telephonededuction=(int) salofmon.get(25);
	    medicalinsurancededuction=(int)salofmon.get(26);
	    
		
		System.out.println(sbmonthofsal+" "+basicearn);	
	}
		
ytdbasicearn=ytdbasicearn+basicearn;	
ytdbasicearnarear=ytdbasicearnarear+basicearnarear;
ytdhra=ytdhra+hra;
ytdhraarrear=ytdhraarrear+hraarrear;
ytdspecialallowance=ytdspecialallowance+specialallowance;
ytdotherallowance=ytdotherallowance+otherallowance;
ytdmedicalallowance=ytdmedicalallowance+medicalallowance;
ytdmedicalallowancearrear=ytdmedicalallowancearrear+medicalallowancearrear;
ytdperformanceincentive=ytdperformanceincentive+performanceincentive;
ytdprovidenetFund=ytdprovidenetFund+providenetFund;
ytdprovidenetFundarrear=ytdprovidenetFundarrear+providenetFundarrear;
ytdproftax=ytdproftax+proftax;
ytdincometax=ytdincometax+incometax;
ytdd1misc=ytdd1misc+d1misc;
ytdd2misc=ytdd2misc+d2misc;
ytdcanteen=ytdcanteen+canteen;
ytdconveyance=ytdconveyance+conveyance;
ytdtotal=ytdtotal+total;
ytdtotaldeduction=ytdtotaldeduction+totaldeduction;
ytdexecutiveallowance=ytdexecutiveallowance+executiveallowance;
ytdexecutiveallowancearrear=ytdexecutiveallowancearrear+executiveallowancearrear;
ytdintressSubsidy=ytdintressSubsidy+intressSubsidy;
ytdltapaid=ytdltapaid+ltapaid;
ytdretainersbonus=ytdretainersbonus+retainersbonus;
ytdsportclubdeduction=ytdsportclubdeduction+sportclubdeduction;
ytdtelephonededuction=ytdtelephonededuction+telephonededuction;
ytdmedicalinsurance=ytdmedicalinsurance+medicalinsurancededuction;


if(sbmonthofsal.toString().equals(currentmon.toString())){
	break;
}
sbmonthofsal.delete(0, sbmonthofsal.length());
}


}
	
	else if(mon.equals("JAN")||mon.equals("FEB")||mon.equals("MAR"))
	{	
for(int i=0;i<12;i++){	
	
	if(i>=0&&i<=8)
	{   
		int cyear=year-1;
		sbmonthofsal.append(cyear);
		sbmonthofsal.append("-");
		sbmonthofsal.append(monarr[i]);
		salofmon=new MasterGridDAO().getSal(sbmonthofsal,empid);
		basicearn=(int) salofmon.get(0);
		basicearnarear=(int) salofmon.get(1);
		hra=(int) salofmon.get(2);
		hraarrear=(int) salofmon.get(3);
	    specialallowance=(int) salofmon.get(4);
	    otherallowance=(int) salofmon.get(5);
	    medicalallowance=(int) salofmon.get(6);
	    medicalallowancearrear=(int) salofmon.get(7);
	    performanceincentive=(int) salofmon.get(8);
	    providenetFund=(int) salofmon.get(9);
	    providenetFundarrear=(int) salofmon.get(10);
	    proftax=(int) salofmon.get(11);
	    incometax=(int) salofmon.get(12);
	    d1misc=(int) salofmon.get(13);
	    d2misc=(int) salofmon.get(14);
	    canteen=(int) salofmon.get(15);
	    conveyance=(int) salofmon.get(16);
	    total=(int) salofmon.get(17);
	    totaldeduction=(int) salofmon.get(18);
	    executiveallowance=(int) salofmon.get(19);
	    executiveallowancearrear=(int) salofmon.get(20);
	    intressSubsidy=(int) salofmon.get(21);
	    ltapaid=(int) salofmon.get(22);
	    retainersbonus=(int) salofmon.get(23);
	    sportclubdeduction=(int) salofmon.get(24);
	    telephonededuction=(int) salofmon.get(25);
	    medicalinsurancededuction=(int)salofmon.get(26);
	    
		System.out.println(sbmonthofsal+" "+basicearn);
	
	}
	
	if(i>=9&&i<=11)
	{	
		sbmonthofsal.append(year);
		sbmonthofsal.append("-");
		sbmonthofsal.append(monarr[i]);
		salofmon=new MasterGridDAO().getSal(sbmonthofsal,empid);	
		basicearn=(int) salofmon.get(0);
		basicearnarear=(int) salofmon.get(1);
	    hra=(int) salofmon.get(2);
	    hraarrear=(int) salofmon.get(3);
	    specialallowance=(int) salofmon.get(4);
	    otherallowance=(int) salofmon.get(5);
	    medicalallowance=(int) salofmon.get(6);
	    medicalallowancearrear=(int) salofmon.get(7);
	    performanceincentive=(int) salofmon.get(8);
	    providenetFund=(int) salofmon.get(9);
	    providenetFundarrear=(int) salofmon.get(10);
	    proftax=(int) salofmon.get(11);
	    incometax=(int) salofmon.get(12);
	    d1misc=(int) salofmon.get(13);
	    d2misc=(int) salofmon.get(14);
	    canteen=(int) salofmon.get(15);
	    conveyance=(int) salofmon.get(16);
	    total=(int) salofmon.get(17);
	    totaldeduction=(int) salofmon.get(18);
	    executiveallowance=(int) salofmon.get(19);
	    executiveallowancearrear=(int) salofmon.get(20);
	    intressSubsidy=(int) salofmon.get(21);
	    ltapaid=(int) salofmon.get(22);
	    retainersbonus=(int) salofmon.get(23);
	    sportclubdeduction=(int) salofmon.get(24);
	    telephonededuction=(int) salofmon.get(25);
	    medicalinsurancededuction=(int)salofmon.get(26);
	    
		System.out.println(sbmonthofsal+" "+basicearn);
			
		
	}
ytdbasicearn=ytdbasicearn+basicearn;	
ytdbasicearnarear=ytdbasicearnarear+basicearnarear;
ytdhra=ytdhra+hra;
ytdhraarrear=ytdhraarrear+hraarrear;
ytdspecialallowance=ytdspecialallowance+specialallowance;
ytdotherallowance=ytdotherallowance+otherallowance;
ytdmedicalallowance=ytdmedicalallowance+medicalallowance;
ytdmedicalallowancearrear=ytdmedicalallowancearrear+medicalallowancearrear;
ytdperformanceincentive=ytdperformanceincentive+performanceincentive;
ytdprovidenetFund=ytdprovidenetFund+providenetFund;
ytdprovidenetFundarrear=ytdprovidenetFundarrear+providenetFundarrear;
ytdproftax=ytdproftax+proftax;
ytdincometax=ytdincometax+incometax;
ytdd1misc=ytdd1misc+d1misc;
ytdd2misc=ytdd2misc+d2misc;
ytdcanteen=ytdcanteen+canteen;
ytdconveyance=ytdconveyance+conveyance;
ytdtotal=ytdtotal+total;
ytdtotaldeduction=ytdtotaldeduction+totaldeduction;
ytdexecutiveallowance=ytdexecutiveallowance+executiveallowance;
ytdexecutiveallowancearrear=ytdexecutiveallowancearrear+executiveallowancearrear;
ytdintressSubsidy=ytdintressSubsidy+intressSubsidy;
ytdltapaid=ytdltapaid+ltapaid;
ytdretainersbonus=ytdretainersbonus+retainersbonus;
ytdsportclubdeduction=ytdsportclubdeduction+sportclubdeduction;
ytdtelephonededuction=ytdtelephonededuction+telephonededuction;
ytdmedicalinsurance=ytdmedicalinsurance+medicalinsurancededuction;

if(sbmonthofsal.toString().equals(currentmon.toString())){
	break;
}

sbmonthofsal.delete(0, sbmonthofsal.length());
		}
	}
ytdsalary.add(empid);
ytdsalary.add(monthofSalary2);
ytdsalary.add(ytdbasicearn);
ytdsalary.add(ytdbasicearnarear);
ytdsalary.add(ytdhra);
ytdsalary.add(ytdhraarrear);
ytdsalary.add(ytdspecialallowance);
ytdsalary.add(ytdotherallowance);
ytdsalary.add(ytdmedicalallowance);
ytdsalary.add(ytdmedicalallowancearrear);
ytdsalary.add(ytdperformanceincentive);
ytdsalary.add(ytdprovidenetFund);
ytdsalary.add(ytdprovidenetFundarrear);
ytdsalary.add(ytdproftax);
ytdsalary.add(ytdincometax);
ytdsalary.add(ytdd1misc);
ytdsalary.add(ytdd2misc);
ytdsalary.add(ytdcanteen);
ytdsalary.add(ytdconveyance);
ytdsalary.add(ytdtotal);
ytdsalary.add(ytdtotaldeduction);
ytdsalary.add(ytdexecutiveallowance);
ytdsalary.add(ytdexecutiveallowancearrear);
ytdsalary.add(ytdintressSubsidy);
ytdsalary.add(ytdltapaid);
ytdsalary.add(ytdretainersbonus);
ytdsalary.add(ytdsportclubdeduction);
ytdsalary.add(ytdtelephonededuction);
ytdsalary.add(ytdmedicalinsurance);
	//----------------------------------------------------------

	new MasterGridDAO().saveNewSalary(ytdsalary);
}

public void checkYtdDetails(String empid, String monthofSalary2) {

		new MasterGridDAO().getYtdSalaryStatus(empid, monthofSalary2);

	}



}




