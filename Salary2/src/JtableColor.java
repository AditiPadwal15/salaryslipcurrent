import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
  
public class JtableColor extends JFrame
{
   public JtableColor(JTable table) {
     //  table = new JTable(100, 5);
  
      TableColumn tm = null;
      for(int i=21;i<31;i++)
      {
       tm = table.getColumnModel().getColumn(i);
       tm.setCellRenderer(new ColorColumnRenderer(Color.LIGHT_GRAY, Color.BLACK));
      }
     
      tm = table.getColumnModel().getColumn(32);
      tm.setCellRenderer(new ColorColumnRenderer(Color.LIGHT_GRAY, Color.BLUE));
      
      tm = table.getColumnModel().getColumn(7);
      tm.setCellRenderer(new ColorColumnRenderer(Color.WHITE, Color.MAGENTA));
      
      
      getContentPane().add(new JScrollPane(table));
   
      addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent we) {
            System.exit(0);
         }
      });
   }
  
  /* public static void main(String [] args) {
      JtableColor main = new JtableColor();
      main.setSize(400, 400);
      main.setVisible(true);
   }*/
}
  
/**
* Applied background and foreground color to single column of a JTable
* in order to distinguish it apart from other columns.
*/
class ColorColumnRenderer extends DefaultTableCellRenderer
{
   Color bkgndColor, fgndColor;
    
   
   public ColorColumnRenderer(Color bkgnd, Color foregnd) {
      super();
      bkgndColor = bkgnd;
      fgndColor = foregnd;
   }
     
   
   
   public Component getTableCellRendererComponent
        (JTable table, Object value, boolean isSelected,
         boolean hasFocus, int row, int column)
   {
      Component cell = super.getTableCellRendererComponent
         (table, value, isSelected, hasFocus, row, column);
  
      cell.setBackground( bkgndColor );
      cell.setForeground( fgndColor );
      
      return cell;
   }
   
}