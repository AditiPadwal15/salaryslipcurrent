import java.awt.ActiveEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.border.Border;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;


public class AddEmployee extends AbstractFormatter implements ActionListener{

	
	JFrame f1;
	JPanel jp,jpButton;
	JLabel imageLabelLogo,jlempcode,jlempname,jlbname,jlbac,jldesig,jlpan,jlpf,jljoining,dateformat,jlcompany_name,jlgender,jlemail,jlcontact;
	JTextField jtempcode,jtempname,jtbname,jtbac,jtdesig,jtpan,jtpf,jtjoining,jtcompany_name,jtemail,jtcontact;
	JComboBox gender;
	JButton jbSave,jbBack,jbReset;
	int i=0;
	int cnt;
	String srno,employee_code,employee_name,employee_gender,bank_name,bank_account,designation,pan_no,
		pf_no,date_of_joining,company_name,email,contact;
	StringBuffer btn=new StringBuffer("Button");
	JDatePickerImpl datePicker;
	String []company_name_array={"ENGINEERING CONSULTANCY SERVICES PVT.LTD ","TECHNOLOGIES"};
	String []gen={"Male","Female"};
	JComboBox company_combo;
	
	 private String datePattern = "yyyy-MM-dd";
	 private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
	
	@Override
	public Object stringToValue(String text) throws ParseException {
		return dateFormatter.parseObject(text);
	}

	@Override
	public String valueToString(Object value) throws ParseException {
		 if (value != null) {
	            Calendar cal = (Calendar) value;
	            return dateFormatter.format(cal.getTime());
	        }
	         
	        return "";
	}
	
	public AddEmployee()
	{
		
		f1=new JFrame("Add Employee");
		f1.setLayout(null);
		f1.getContentPane().setBackground(Color.LIGHT_GRAY);
		jp=new JPanel();
		jpButton=new JPanel();
		dateformat=new JLabel("(YYYY-MM-DD)");
		
		
		UtilDateModel model = new UtilDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel, this);
				
		jlcompany_name=new JLabel("Company Name:");
		jlempcode=new JLabel("Employee Code:");
		jlempname=new JLabel("Employee Name:");
		jlgender=new JLabel("Gender");
		jlbname=new JLabel("Bank Name:");
		jlbac=new JLabel("Bank Account No. :");
		jldesig=new JLabel("Designation :");
		jlpan=new JLabel("Pan No. :");
		jlpf=new JLabel("Providant fund No. :");
		jljoining=new JLabel("Date of Joining");
		jlemail=new JLabel("Email Id :");
		jlcontact=new JLabel("Contact No :");
		
		company_combo=new JComboBox(company_name_array);
		jtempcode=new JTextField(45);
		jtempname=new JTextField(45);          	
		jtbname=new JTextField(45);				jtbname.setText("Corporation Bank");
		gender=new JComboBox(gen);
		jtbac=new JTextField(45);
		jtdesig=new JTextField(45);
		jtpan=new JTextField(45);
		jtpf=new JTextField(45);
		jtjoining=new JTextField(45);
		jtemail=new JTextField(45);
		jtcontact=new JTextField(45);
		
		
        jbSave=new JButton("SAVE INFO");
		jbSave.setActionCommand("save");
		
		jbBack=new JButton("BACK");
		jbBack.setActionCommand("back");
		
		jbReset=new JButton("RESET");
		jbReset.setActionCommand("reset");
		
		f1.add(jlcompany_name);
		f1.add(jlbac);
		f1.add(jlbname);
		f1.add(jlgender);
		f1.add(jldesig);
		f1.add(jlempcode);
		f1.add(jlempname);
		f1.add(jljoining);
		f1.add(jlpan);
		f1.add(jlpf);
		f1.add(dateformat);
		f1.add(jlemail);
		f1.add(jlcontact);
		
		f1.add(company_combo);
		f1.add(jtbac);
		f1.add(jtbname);
		f1.add(gender);
		f1.add(jtdesig);
		f1.add(jtempcode);
		f1.add(jtempname);
		f1.add(jtjoining);
		f1.add(jtpan);
		f1.add(jtpf);
		f1.add(jtemail);
		f1.add(jtcontact);
		
		f1.add(jbSave);
		f1.add(jbReset);
		f1.add(jbBack);
		f1.add(datePicker);
		
		f1.add(jp);
		f1.add(jpButton);
		f1.setSize(1024,730);
		f1.setVisible(true);
		
		jp.setBounds(220,85,600,500);
		jp.setBackground(Color.GRAY);
		
		jpButton.setBounds(220,610,600,50);
		jpButton.setBackground(Color.DARK_GRAY);

		jlcompany_name.setBounds(300,100,400,20);
		jlempcode.setBounds		(300,140,400,20);
		jlempname.setBounds		(300,180,400,20);
		jlgender.setBounds		(300,220,400,20);
		jlbname.setBounds		(300,260,400,20);
		jlbac.setBounds			(300,300,400,20);
		jldesig.setBounds		(300,340,320,20);
		jlpan.setBounds			(300,380,400,20);
		jlpf.setBounds			(300,420,400,20);
		jljoining.setBounds		(300,460,400,20);
		jlemail.setBounds       (300,500,400,20);
		jlcontact.setBounds     (300,540,400,20);
		
		
		company_combo.setBounds(450, 100, 350, 20);
		jtempcode.setBounds	(450,140,150,20);
		jtempname.setBounds	(450,180,150,20);
		gender.setBounds	(450,220,150,20);
		jtbname.setBounds	(450,260,150,20);
		jtbac.setBounds		(450,300,150,20);
		jtdesig.setBounds	(450,340,150,20);
		jtpan.setBounds		(450,380,150,20);
		jtpf.setBounds		(450,420,150,25);
	//	jtjoining.setBounds	(250,360,150,20);  dateformat.setBounds	(420,360,150,20); 
		datePicker.setBounds(450,460,150,25);
		jtemail.setBounds(450,500,150,20);
		jtcontact.setBounds(450,540,150,20);
		
		jbSave.setBounds(350,620,100,30);		
		jbSave.addActionListener(this);
		
		jbBack.setBounds(500,620,100,30);		
		jbBack.addActionListener(this);
		
		jbReset.setBounds(650,620,100,30);		
		jbReset.addActionListener(this);
		
		//code for logo & Co.Name
        imageLabelLogo=new JLabel(new ImageIcon("new/logo.png"));
        imageLabelLogo.setBounds(230, 0, 200, 100);
        f1.add(imageLabelLogo);
        f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		
	}

	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getActionCommand()=="save"){
			try {
				savedata();
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(ae.getActionCommand()=="back"){
			new menu(0);
			f1.setVisible(false);
		}
		if(ae.getActionCommand()=="reset"){
			
			jtempcode.setText("");
			jtempname.setText("");
			//jtbname.setText("");
			jtbac.setText("");
			jtdesig.setText("");
			jtpan.setText("");
			jtpf.setText("");
			//jtjoining.setText("");
			jtemail.setText("");
			jtcontact.setText("");
		}
	    	
	}

 
	
	public void savedata() throws ParseException{
		
		String sDate="";
		try{
			
		
		company_name=(String) company_combo.getSelectedItem();
		employee_name=jtempname.getText();
        employee_code=jtempcode.getText();
        employee_gender=gender.getSelectedItem().toString();
        bank_name=jtbname.getText();
        bank_account=jtbac.getText();
        designation=jtdesig.getText();
        pan_no=jtpan.getText();
        pf_no=jtpf.getText();
        email=jtemail.getText();
        contact=jtcontact.getText();
     
        java.util.Date selectedDate = (java.util.Date) datePicker.getModel().getValue();
       
  		
  		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  		sDate= sdf.format(selectedDate);
  		
  		System.out.println(sDate);
		
		
        
  															// SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        													// System.out.println(dateFormat.format(dateFormat.parse(date_of_joining)));
        
  		
  		
  		
        
		String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
        String userid = "root";
        String password = "yungry";
        
        
        employee_code=jtempcode.getText();
		employee_name=jtempname.getText();
		System.out.println(employee_code+" "+employee_name);
		String sql="select COUNT(*) as cno from employee_details";
        
		
		
			
		
        try{
        Connection connection = DriverManager.getConnection( url, userid, password );
        Statement stmt = connection.createStatement();
        ResultSet rs=stmt.executeQuery(sql);
        if(rs.next())
        {
        cnt=rs.getInt("cno");
        cnt++;
       // btn.append(cnt);
        System.out.println(cnt);
        
        }
        
        
        sql="INSERT INTO employee_details VALUES('"+cnt+"','"+employee_code+"','"+employee_name+"','"+bank_name+"',"
        		+ "'"+bank_account+"','"+designation+"','"+pan_no+"','"+pf_no+"','"+sDate+"',"
        		+ "'"+company_name+"','"+employee_gender+"','"+email+"','"+contact+"','Active','1999-01-01')";
        stmt.execute(sql);
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        new AddBaseData(employee_code);
       // new menu(0);
        f1.setVisible(false);
		}
		catch(Exception e)
		{
			  ImageIcon icon = new ImageIcon(login.class.getResource("new/download.jpg"));
          	JOptionPane.showMessageDialog(null,
                       new JLabel("Incorrect data please check the data you Entered...!!!\n & fill all the details", icon, JLabel.LEFT),
                       "Success...!!", JOptionPane.DEFAULT_OPTION);
          				
		}
	}
	
	
	 
	
	
	
	
	
	public static void main(String[] a){
		new AddEmployee();
	}

	
}

class DateValidator {
public Date getlowerBoundDate(){
    String date;
    Date date1=new Date();
    @SuppressWarnings("deprecation")
	Integer year=1900+date1.getYear();
    date="01-Apr-"+year;
    return new Date(date);
}
public Date getupperBoundDate(){
    String date;
    Date date1=new Date();
    Integer year=1900+date1.getYear();
    date="31-Mar-"+(year+1);//you can set date as you wish over here
    return new Date(date);
 }
}