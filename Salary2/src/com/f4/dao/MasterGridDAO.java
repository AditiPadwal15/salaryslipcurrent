package com.f4.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MasterGridDAO {
	
	
	String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
    String userid = "root";
    String password = "yungry";
	
	public void saveLeaves(String monthofSalary,String empid,float payableDays,float leavesTaken,float leavesAdjusted,float paidDays){
		int count=0;
		
		String max="select MAX(leaves_data_id) as maxid from leaves_data";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(max);
				if(rs.next()){
				count=rs.getInt("maxid");
				count++;
				}
			}
	        catch (SQLException e)
	        {
	            System.out.println(e.getMessage());
	        }

		String sql=null;
		
		String check="select * from leaves_data where employee_code='"+empid+"' AND month_of_salary='"+monthofSalary+"'";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(check);
				if(rs.next()){
					sql="UPDATE leaves_data set payable_days="+payableDays+",leaves_taken="+leavesTaken+",leaves_adjusted="+leavesAdjusted+","
							+ "paid_days="+paidDays+" where employee_code='"+empid+"' AND month_of_salary='"+monthofSalary+"'";
				}
				else{
				sql="INSERT INTO leaves_data VALUES("+count+",'"+empid+"','"+monthofSalary+"',"+payableDays+","+leavesTaken+","
						+ ""+leavesAdjusted+","+paidDays+")";
				}
			}
	        catch (SQLException e)
	        {
	            System.out.println(e.getMessage());
	        }
		
		
		
		
		
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				stmt.executeUpdate(sql);
			}
	        catch (SQLException e)
	        {
	            System.out.println( e.getMessage() );
	        }

	}
	
	
	public ArrayList getBaseData(String empid){
		
		ArrayList basicData=new ArrayList();
		
		float baseSal,medicalAllowance,medicalAllowanceArrear,Convayance,specialAllowance,otherAllowance,executiveallowance,executiveallowanceArrear,
		intressSubsidy,performanceIncentive,ltaPaid,retainersBonus,providantFund,providantFundArrear,professionalTax,incomeTax,d1Misc,d2Misc,
		canteenDeduction,sportClub,telephoneDeduction,medicalInsurance;
		
		String sql="select * from basedata where employee_code='"+empid+"'";
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(sql);
				if(rs.next()){
					
					baseSal=Float.parseFloat(rs.getString("basic_salary"));
					medicalAllowance=Float.parseFloat(rs.getString("medical_allowance"));
					medicalAllowanceArrear=Float.parseFloat(rs.getString("medical_allowance_arrear"));
					Convayance=Float.parseFloat(rs.getString("conveyance"));
					specialAllowance=Float.parseFloat(rs.getString("specil_allowance"));
					otherAllowance=Float.parseFloat(rs.getString("other_allowance"));
					executiveallowance=Float.parseFloat(rs.getString("executive_allowance"));
					executiveallowanceArrear=Float.parseFloat(rs.getString("executive_allowance_arrear"));
					intressSubsidy=Float.parseFloat(rs.getString("intres_subsidy"));
					performanceIncentive=Float.parseFloat(rs.getString("performance_incentive"));
					ltaPaid=Float.parseFloat(rs.getString("lta_paid"));
					retainersBonus=Float.parseFloat(rs.getString("retainers_bonus"));
					providantFund=Float.parseFloat(rs.getString("provident_fund"));
					providantFundArrear=Float.parseFloat(rs.getString("provident_fund_arrear"));
					professionalTax=Float.parseFloat(rs.getString("professional_tax"));
					incomeTax=Float.parseFloat(rs.getString("income_tax"));
					d1Misc=Float.parseFloat(rs.getString("d1_misc_deduction"));
					d2Misc=Float.parseFloat(rs.getString("d2_misc_deduction"));
					canteenDeduction=Float.parseFloat(rs.getString("canteen"));
					sportClub=Float.parseFloat(rs.getString("sport_club"));
					telephoneDeduction=Float.parseFloat(rs.getString("teliphone_deduction"));
					medicalInsurance=Float.parseFloat(rs.getString("medical_insurence_deduction"));
					
					basicData.add(baseSal);
					basicData.add(medicalAllowance);
					basicData.add(medicalAllowanceArrear);
					basicData.add(Convayance);
					basicData.add(specialAllowance);
					basicData.add(otherAllowance);
					basicData.add(executiveallowance);
					basicData.add(executiveallowanceArrear);
					basicData.add(intressSubsidy);
					basicData.add(performanceIncentive);
					basicData.add(ltaPaid);
					basicData.add(retainersBonus);
					basicData.add(providantFund);
					basicData.add(providantFundArrear);
					basicData.add(professionalTax);
					basicData.add(incomeTax);
					basicData.add(d1Misc);
					basicData.add(d2Misc);
					basicData.add(canteenDeduction);
				    basicData.add(sportClub);
				    basicData.add(telephoneDeduction);
				    basicData.add(medicalInsurance);
					
				}
			}
			
	        catch (SQLException e)
	        {
	            System.out.println(e.getMessage());
	        }
		return basicData;
	}
	
	
	
	public void saveSalaryData(ArrayList salaryOfMonth){
		String empid,monthofsal,salaryInWords,dateofsal;
		 float basicEarn,basicEarnArrear,payableDays,paidDays,hra,hraArrear,specialAllowance,otherAllowance,medicalAllowance,medicalAllowanceArrear,
		 executiveAllowance,executiveAllowanceArrear,intressSubsidy,performanceIncentive,ltaPaid,retainersBonus,pf,pfArrear,professionaltax,incomeTax,d1misc,d2misc,
		 canteenDeduction,sportClub,telephoneDeduction,totalIncome,totalDeduction,netSalary,conveyance,medicalInsurance;
		 
		 
		 empid=salaryOfMonth.get(0).toString();
		 monthofsal=salaryOfMonth.get(1).toString();
		 basicEarn=Float.parseFloat(salaryOfMonth.get(2).toString());
		 basicEarnArrear=Float.parseFloat(salaryOfMonth.get(3).toString());
		 payableDays=Float.parseFloat(salaryOfMonth.get(4).toString());
		 paidDays=Float.parseFloat(salaryOfMonth.get(5).toString());
		 hra=Float.parseFloat(salaryOfMonth.get(6).toString());
		 hraArrear=Float.parseFloat(salaryOfMonth.get(7).toString());
		 specialAllowance=Float.parseFloat(salaryOfMonth.get(8).toString());
		 otherAllowance=Float.parseFloat(salaryOfMonth.get(9).toString());
		 medicalAllowance=Float.parseFloat(salaryOfMonth.get(10).toString());
		 medicalAllowanceArrear=Float.parseFloat(salaryOfMonth.get(11).toString());
		 executiveAllowance=Float.parseFloat(salaryOfMonth.get(12).toString());
		 executiveAllowanceArrear=Float.parseFloat(salaryOfMonth.get(13).toString());
		 intressSubsidy=Float.parseFloat(salaryOfMonth.get(14).toString());
		 performanceIncentive=Float.parseFloat(salaryOfMonth.get(15).toString());
		 ltaPaid=Float.parseFloat(salaryOfMonth.get(16).toString());
		 retainersBonus=Float.parseFloat(salaryOfMonth.get(17).toString());
		 pf=Float.parseFloat(salaryOfMonth.get(18).toString());
		 pfArrear=Float.parseFloat(salaryOfMonth.get(19).toString());
		 professionaltax=Float.parseFloat(salaryOfMonth.get(20).toString());
		 incomeTax=Float.parseFloat(salaryOfMonth.get(21).toString());
		 d1misc=Float.parseFloat(salaryOfMonth.get(22).toString());
		 d2misc=Float.parseFloat(salaryOfMonth.get(23).toString());
		 canteenDeduction=Float.parseFloat(salaryOfMonth.get(24).toString());
		 sportClub=Float.parseFloat(salaryOfMonth.get(25).toString());
		 telephoneDeduction=Float.parseFloat(salaryOfMonth.get(26).toString());
		 totalIncome=Float.parseFloat(salaryOfMonth.get(27).toString());
		 totalDeduction=Float.parseFloat(salaryOfMonth.get(28).toString());
		 netSalary=Float.parseFloat(salaryOfMonth.get(29).toString());
		 conveyance=Float.parseFloat(salaryOfMonth.get(30).toString());
		 salaryInWords=salaryOfMonth.get(31).toString();
		 medicalInsurance=Float.parseFloat(salaryOfMonth.get(32).toString());
		// dateofsal=salaryOfMonth.get(33).toString();
	
		 
		 int count=0;
		 String max="select MAX(salary_id) as maxid from salary_details";
			try (Connection connection = DriverManager.getConnection( url, userid, password );
		            Statement stmt = connection.createStatement();){
					ResultSet rs=stmt.executeQuery(max);
					if(rs.next()){
					count=rs.getInt("maxid");
					count++;
					
					}
				}
		        catch (SQLException e)
		        {
		            e.printStackTrace();
		        }
			
			
			String insertSalary="",checkRecord;
			
			checkRecord="select * from salary_details where employee_code='"+empid+"'AND month_of_salary='"+monthofsal+"'";
			
			try (Connection connection = DriverManager.getConnection( url, userid, password );
		            Statement stmt = connection.createStatement();){
					ResultSet rs=stmt.executeQuery(checkRecord);
					if(rs.next()){
						
						insertSalary="UPDATE salary_details SET basic_earn="+basicEarn+",basic_earn_arrear="+basicEarnArrear+",payable_days="+payableDays+","
						+ "paid_days="+paidDays+",hra="+hra+",hra_arrear="+hraArrear+",special_allowance="+specialAllowance+",other_allowance="+otherAllowance+","
						+ "medical_allowance="+medicalAllowance+",medical_allowance_arrear="+medicalAllowanceArrear+","
						+ "performance_incentive="+performanceIncentive+",providant_fund="+pf+",providant_fund_arrear="+pfArrear+",p_tax="+professionaltax+","
						+ "income_tax="+incomeTax+",d1_misc_deduction="+d1misc+",d2_misc_deduction="+d2misc+",canteen_deduction="+canteenDeduction+","
						+ "total="+totalIncome+",total_deduction="+totalDeduction+",net_salary="+netSalary+",convenyance="+conveyance+","
						+ "net_sal_word='"+salaryInWords+"',executive_allowance="+executiveAllowance+",executive_allowance_arrear="+executiveAllowanceArrear+","
						+ "intress_subsidy="+intressSubsidy+",lta_paid="+ltaPaid+",retainers_bonus="+retainersBonus+","
						+ "sport_club_deduction="+sportClub+",telephone_deduction="+telephoneDeduction+",medical_insurance_deduction="+medicalInsurance+",date_of_salary='00-00-0000' WHERE employee_code='"+empid+"'AND month_of_salary='"+monthofsal+"'";
						
					
					}
					else{
						 
						insertSalary="INSERT INTO salary_details VALUES("+count+",'"+empid+"','"+monthofsal+"',"+basicEarn+","+basicEarnArrear+","+payableDays+","
						 		+ ""+paidDays+","+hra+","+hraArrear+","+specialAllowance+","+otherAllowance+","+medicalAllowance+","+medicalAllowanceArrear+","
						 		+ ""+performanceIncentive+","+pf+","+pfArrear+","+professionaltax+","+incomeTax+","+d1misc+","+d2misc+","+canteenDeduction+","
						 		+ ""+totalIncome+","+totalDeduction+","+netSalary+","+conveyance+",'"+salaryInWords+"',"+executiveAllowance+","
						 		+ ""+executiveAllowanceArrear+","+intressSubsidy+","+ltaPaid+","+retainersBonus+","+sportClub+","+telephoneDeduction+","+medicalInsurance+",'00-00-0000')";
						 
					}
				}
		        catch (SQLException e)
		        {
		            e.printStackTrace();
		        }
			
			
		 
			
		 try (Connection connection = DriverManager.getConnection( url, userid, password );
		            Statement stmt = connection.createStatement();){
			 stmt.executeUpdate(insertSalary);
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 
		 }


	
	//method to get alredy filled salary details 
	public ArrayList getOldSalary(String empid, String monthofSalary) {
		System.out.println("DB METHOD CALL");
		
		 ArrayList oldSalary=new ArrayList();
		
		String salaryInWords;
		 float basicEarn,basicEarnArrear,payableDays,paidDays,hra,hraArrear,specialAllowance,otherAllowance,medicalAllowance,medicalAllowanceArrear,
		 executiveAllowance,executiveAllowanceArrear,intressSubsidy,performanceIncentive,ltaPaid,retainersBonus,pf,pfArrear,professionaltax,incomeTax,d1misc,d2misc,
		 canteenDeduction,sportClub,telephoneDeduction,totalIncome,totalDeduction,netSalary,conveyance,medicalinsurance;
		
		String getSal="select * from salary_details where employee_code='"+empid+"'AND month_of_salary='"+monthofSalary+"'";
		
		System.out.println("ID="+empid+"  Month="+monthofSalary);
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(getSal);
				if(rs.next()){
					 basicEarn=Float.parseFloat(rs.getString("basic_earn"));
					 basicEarnArrear=Float.parseFloat(rs.getString("basic_earn_arrear"));
					 payableDays=Float.parseFloat(rs.getString("payable_days"));
					 paidDays=Float.parseFloat(rs.getString("paid_days"));
					 hra=Float.parseFloat(rs.getString("hra"));
					 hraArrear=Float.parseFloat(rs.getString("hra_arrear"));
					 specialAllowance=Float.parseFloat(rs.getString("special_allowance"));
					 otherAllowance=Float.parseFloat(rs.getString("other_allowance"));
					 executiveAllowance=Float.parseFloat(rs.getString("executive_allowance"));
					 executiveAllowanceArrear=Float.parseFloat(rs.getString("executive_allowance_arrear"));
					 medicalAllowance=Float.parseFloat(rs.getString("medical_allowance"));
					 medicalAllowanceArrear=Float.parseFloat(rs.getString("medical_allowance_arrear"));
					 intressSubsidy=Float.parseFloat(rs.getString("intress_subsidy"));
					 performanceIncentive=Float.parseFloat(rs.getString("performance_incentive"));
					 ltaPaid=Float.parseFloat(rs.getString("lta_paid"));
					 retainersBonus=Float.parseFloat(rs.getString("retainers_bonus"));
					 pf=Float.parseFloat(rs.getString("providant_fund"));
					 pfArrear=Float.parseFloat(rs.getString("providant_fund_arrear"));
					 professionaltax=Float.parseFloat(rs.getString("p_tax"));
					 incomeTax=Float.parseFloat(rs.getString("income_tax"));
					 d1misc=Float.parseFloat(rs.getString("d1_misc_deduction"));
					 d2misc=Float.parseFloat(rs.getString("d2_misc_deduction"));
					 canteenDeduction=Float.parseFloat(rs.getString("canteen_deduction"));
					 sportClub=Float.parseFloat(rs.getString("sport_club_deduction"));
					 telephoneDeduction=Float.parseFloat(rs.getString("telephone_deduction"));
					 totalIncome=Float.parseFloat(rs.getString("total"));
					 totalDeduction=Float.parseFloat(rs.getString("total_deduction"));
					 netSalary=Float.parseFloat(rs.getString("net_salary"));
					 conveyance=Float.parseFloat(rs.getString("convenyance"));
					 medicalinsurance=Float.parseFloat(rs.getString("medical_insurance_deduction"));
					
					
					 
					
					 oldSalary.add(basicEarn);
					 oldSalary.add(basicEarnArrear);
					 oldSalary.add(payableDays);
					 oldSalary.add(paidDays);
					 oldSalary.add(hra);
					 oldSalary.add(hraArrear);
					 oldSalary.add(specialAllowance);
					 oldSalary.add(otherAllowance);
					 oldSalary.add(executiveAllowance);
					 oldSalary.add(executiveAllowanceArrear);
					 oldSalary.add(medicalAllowance);
					 oldSalary.add(medicalAllowanceArrear);
					 oldSalary.add(intressSubsidy);
					 oldSalary.add(performanceIncentive);
					 oldSalary.add(ltaPaid);
					 oldSalary.add(retainersBonus);
					 oldSalary.add(pf);
					 oldSalary.add(pfArrear);
					 oldSalary.add(professionaltax);
					 oldSalary.add(incomeTax);
					 oldSalary.add(d1misc);
					 oldSalary.add(d2misc);
					 oldSalary.add(canteenDeduction);
					 oldSalary.add(sportClub);
					 oldSalary.add(telephoneDeduction);
					 oldSalary.add(totalIncome);
					 oldSalary.add(totalDeduction);
					 oldSalary.add(netSalary);
					 oldSalary.add(conveyance);
					 oldSalary.add(medicalinsurance);
					 
					 System.out.println("Data filled");
					 return oldSalary;
				}
				
				else{
					System.out.println("NO DATA FOUND"+empid);
				}
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return oldSalary;
		
	}


	//this method fetch current month salary to calculate ytd details
	public ArrayList getCurrentmonthSalary(String empid, String monthofSalary2) {
		

		 ArrayList currentSalary=new ArrayList();
		
		String salaryInWords;
		 float basicEarn,basicEarnArrear,payableDays,paidDays,hra,hraArrear,specialAllowance,otherAllowance,medicalAllowance,medicalAllowanceArrear,
		 executiveAllowance,executiveAllowanceArrear,intressSubsidy,performanceIncentive,ltaPaid,retainersBonus,pf,pfArrear,professionaltax,incomeTax,d1misc,d2misc,
		 canteenDeduction,sportClub,telephoneDeduction,totalIncome,totalDeduction,netSalary,conveyance;
		
		String getSal="select * from salary_details where employee_code='"+empid+"'AND month_of_salary='"+monthofSalary2+"'";
		
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(getSal);
				if(rs.next()){
					 basicEarn=Float.parseFloat(rs.getString("basic_earn"));
					 basicEarnArrear=Float.parseFloat(rs.getString("basic_earn_arrear"));
					// payableDays=Float.parseFloat(rs.getString("payable_days"));
					// paidDays=Float.parseFloat(rs.getString("paid_days"));
					 hra=Float.parseFloat(rs.getString("hra"));
					 hraArrear=Float.parseFloat(rs.getString("hra_arrear"));
					 specialAllowance=Float.parseFloat(rs.getString("special_allowance"));
					 otherAllowance=Float.parseFloat(rs.getString("other_allowance"));
					 executiveAllowance=Float.parseFloat(rs.getString("executive_allowance"));
					 executiveAllowanceArrear=Float.parseFloat(rs.getString("executive_allowance_arrear"));
					 medicalAllowance=Float.parseFloat(rs.getString("medical_allowance"));
					 medicalAllowanceArrear=Float.parseFloat(rs.getString("medical_allowance_arrear"));
					 intressSubsidy=Float.parseFloat(rs.getString("intress_subsidy"));
					 performanceIncentive=Float.parseFloat(rs.getString("performance_incentive"));
					 ltaPaid=Float.parseFloat(rs.getString("lta_paid"));
					 retainersBonus=Float.parseFloat(rs.getString("retainers_bonus"));
					 pf=Float.parseFloat(rs.getString("providant_fund"));
					 pfArrear=Float.parseFloat(rs.getString("providant_fund_arrear"));
					 professionaltax=Float.parseFloat(rs.getString("p_tax"));
					 incomeTax=Float.parseFloat(rs.getString("income_tax"));
					 d1misc=Float.parseFloat(rs.getString("d1_misc_deduction"));
					 d2misc=Float.parseFloat(rs.getString("d2_misc_deduction"));
					 canteenDeduction=Float.parseFloat(rs.getString("canteen_deduction"));
					 sportClub=Float.parseFloat(rs.getString("sport_club_deduction"));
					 telephoneDeduction=Float.parseFloat(rs.getString("telephone_deduction"));
					 totalIncome=Float.parseFloat(rs.getString("total"));
					 totalDeduction=Float.parseFloat(rs.getString("total_deduction"));
					 netSalary=Float.parseFloat(rs.getString("net_salary"));
					 conveyance=Float.parseFloat(rs.getString("convenyance"));
					
					
					 
					
					 currentSalary.add(basicEarn);
					 currentSalary.add(basicEarnArrear);
					// currentSalary.add(payableDays);
					// currentSalary.add(paidDays);
					 currentSalary.add( hra);
					 currentSalary.add( hraArrear);
					 currentSalary.add( specialAllowance);
					 currentSalary.add( otherAllowance);
					 currentSalary.add( executiveAllowance);
					 currentSalary.add( executiveAllowanceArrear);
					 currentSalary.add( medicalAllowance);
					 currentSalary.add( medicalAllowanceArrear);
					 currentSalary.add( intressSubsidy);
					 currentSalary.add( performanceIncentive);
					 currentSalary.add(ltaPaid);
					 currentSalary.add(retainersBonus);
					 currentSalary.add(pf);
					 currentSalary.add( pfArrear);
					 currentSalary.add( professionaltax);
					 currentSalary.add( incomeTax);
					 currentSalary.add(d1misc);
					 currentSalary.add( d2misc);
					 currentSalary.add( canteenDeduction);
					 currentSalary.add( sportClub);
					 currentSalary.add( telephoneDeduction);
					 currentSalary.add(totalIncome);
					 currentSalary.add(totalDeduction);
					 currentSalary.add(netSalary);
					 currentSalary.add(conveyance);
					 
					
					 return currentSalary;
				}
				
				else{
					System.out.println("NO DATA FOUND"+empid);
				}
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return currentSalary;
			
		
	}

	
	
	//method to get Ytd salary to calculate new YTD salary

	public ArrayList getYtdSalary(String empid, String monthofSalary2) {
		
ArrayList ytdSalary=new ArrayList();
		
		String salaryInWords;
		 float ytd_basicEarn,ytd_basicEarnArrear,ytd_hra,ytd_hraArrear,ytd_specialAllowance,ytd_otherAllowance,ytd_medicalAllowance,ytd_medicalAllowanceArrear,
		 ytd_executiveAllowance,ytd_executiveAllowanceArrear,ytd_intressSubsidy,ytd_performanceIncentive,ytd_ltaPaid,ytd_retainersBonus,ytd_pf,ytd_pfArrear,
		 ytd_professionaltax,ytd_incomeTax,ytd_d1misc,ytd_d2misc,ytd_canteenDeduction,ytd_sportClub,ytd_telephoneDeduction,ytd_totalIncome,
		 ytd_totalDeduction,ytd_conveyance;
		
		String getSal="select * from ytd_salary_details where employee_code='"+empid+"'";
		
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(getSal);
				if(rs.next()){
					rs.last();
					ytd_basicEarn=Float.parseFloat(rs.getString("ytd_basic_earn"));
					ytd_basicEarnArrear=Float.parseFloat(rs.getString("ytd_basic_earn_arrear"));
					ytd_hra=Float.parseFloat(rs.getString("ytd_hra"));
					ytd_hraArrear=Float.parseFloat(rs.getString("ytd_hra_arrear"));
					ytd_specialAllowance=Float.parseFloat(rs.getString("ytd_special_allowance"));
					ytd_otherAllowance=Float.parseFloat(rs.getString("ytd_other_allowance"));
					ytd_executiveAllowance=Float.parseFloat(rs.getString("ytd_executive_allowance"));
					ytd_executiveAllowanceArrear=Float.parseFloat(rs.getString("ytd_executive_allowance_arrear"));
					ytd_medicalAllowance=Float.parseFloat(rs.getString("ytd_medical_allowance"));
					ytd_medicalAllowanceArrear=Float.parseFloat(rs.getString("ytd_medical_allowance_arrear"));
					ytd_intressSubsidy=Float.parseFloat(rs.getString("ytd_intress_subsidy"));
					ytd_performanceIncentive=Float.parseFloat(rs.getString("ytd_performance_incentive"));
					ytd_ltaPaid=Float.parseFloat(rs.getString("ytd_lta_paid"));
					ytd_retainersBonus=Float.parseFloat(rs.getString("ytd_retainers_bonus"));
					ytd_pf=Float.parseFloat(rs.getString("ytd_providant_fund"));
					ytd_pfArrear=Float.parseFloat(rs.getString("ytd_providant_fund_arrear"));
					ytd_professionaltax=Float.parseFloat(rs.getString("ytd_p_tax"));
					ytd_incomeTax=Float.parseFloat(rs.getString("ytd_income_tax"));
					ytd_d1misc=Float.parseFloat(rs.getString("ytd_d1_misc_deduction"));
					ytd_d2misc=Float.parseFloat(rs.getString("ytd_d2_misc_deduction"));
					ytd_canteenDeduction=Float.parseFloat(rs.getString("ytd_canteen_deduction"));
					ytd_sportClub=Float.parseFloat(rs.getString("ytd_sports_club"));
					ytd_telephoneDeduction=Float.parseFloat(rs.getString("ytd_telephone_deduction"));
					ytd_totalIncome=Float.parseFloat(rs.getString("ytd_total_earning"));
					ytd_totalDeduction=Float.parseFloat(rs.getString("ytd_total_deduction"));
					ytd_conveyance=Float.parseFloat(rs.getString("ytd_convenyance"));
					
					
					 
					
					 ytdSalary.add(ytd_basicEarn);
					 ytdSalary.add(ytd_basicEarnArrear);
					 ytdSalary.add(ytd_hra);
					 ytdSalary.add(ytd_hraArrear);
					 ytdSalary.add(ytd_specialAllowance);
					 ytdSalary.add(ytd_otherAllowance);
					 ytdSalary.add(ytd_executiveAllowance);
					 ytdSalary.add(ytd_executiveAllowanceArrear);
					 ytdSalary.add(ytd_medicalAllowance);
					 ytdSalary.add(ytd_medicalAllowanceArrear);
					 ytdSalary.add(ytd_intressSubsidy);
					 ytdSalary.add(ytd_performanceIncentive);
					 ytdSalary.add(ytd_ltaPaid);
					 ytdSalary.add(ytd_retainersBonus);
					 ytdSalary.add(ytd_pf);
					 ytdSalary.add(ytd_pfArrear);
					 ytdSalary.add(ytd_professionaltax);
					 ytdSalary.add(ytd_incomeTax);
					 ytdSalary.add(ytd_d1misc);
					 ytdSalary.add(ytd_d2misc);
					 ytdSalary.add(ytd_canteenDeduction);
					 ytdSalary.add(ytd_sportClub);
					 ytdSalary.add(ytd_telephoneDeduction);
					 ytdSalary.add(ytd_totalIncome);
					 ytdSalary.add(ytd_totalDeduction);
					 ytdSalary.add(ytd_conveyance);
					 
					
				return ytdSalary;
				}
				
				else{
					System.out.println("NO DATA FOUND"+empid);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 ytdSalary.add(0);
					 
				return ytdSalary;
				}
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return ytdSalary;
	}

	

	public void saveNewSalary(ArrayList newYtdSal) {
		
		
	

		float ytd_basicEarn,ytd_basicEarnArrear,ytd_hra,ytd_hraArrear,ytd_specialAllowance,ytd_otherAllowance,ytd_medicalAllowance,ytd_medicalAllowanceArrear,
		ytd_executiveAllowance,ytd_executiveAllowanceArrear,ytd_intressSubsidy,ytd_performanceIncentive,ytd_ltaPaid,ytd_retainersBonus,ytd_pf,ytd_pfArrear,
		ytd_professionaltax,ytd_incomeTax,ytd_d1misc,ytd_d2misc,ytd_canteenDeduction,ytd_sportClub,ytd_telephoneDeduction,ytd_totalIncome,ytd_totalDeduction,ytd_conveyance,ytdmedicalInsuranceDeduction;
		
		
		
		String empid=newYtdSal.get(0).toString();
		String month_of_sal=newYtdSal.get(1).toString();
		
		ytd_basicEarn=Float.parseFloat(newYtdSal.get(2).toString());
		ytd_basicEarnArrear=Float.parseFloat(newYtdSal.get(3).toString());
		ytd_hra=Float.parseFloat(newYtdSal.get(4).toString());
		ytd_hraArrear=Float.parseFloat(newYtdSal.get(5).toString());;
		ytd_medicalAllowance=Float.parseFloat(newYtdSal.get(8).toString());
		ytd_medicalAllowanceArrear=Float.parseFloat(newYtdSal.get(9).toString());
		ytd_conveyance=Float.parseFloat(newYtdSal.get(18).toString());
		ytd_specialAllowance=Float.parseFloat(newYtdSal.get(6).toString());
		ytd_otherAllowance=Float.parseFloat(newYtdSal.get(7).toString());
		ytd_executiveAllowance=Float.parseFloat(newYtdSal.get(21).toString());
		ytd_executiveAllowanceArrear=Float.parseFloat(newYtdSal.get(22).toString());
		ytd_intressSubsidy=Float.parseFloat(newYtdSal.get(23).toString());
		ytd_performanceIncentive=Float.parseFloat(newYtdSal.get(10).toString());
		ytd_ltaPaid=Float.parseFloat(newYtdSal.get(24).toString());
		ytd_retainersBonus=Float.parseFloat(newYtdSal.get(25).toString());
		ytd_pf=Float.parseFloat(newYtdSal.get(11).toString());
		ytd_pfArrear=Float.parseFloat(newYtdSal.get(12).toString());
		ytd_professionaltax=Float.parseFloat(newYtdSal.get(13).toString());
		ytd_incomeTax=Float.parseFloat(newYtdSal.get(14).toString());
		ytd_d1misc=Float.parseFloat(newYtdSal.get(15).toString());
		ytd_d2misc=Float.parseFloat(newYtdSal.get(16).toString());
		ytd_canteenDeduction=Float.parseFloat(newYtdSal.get(17).toString());
		ytd_sportClub=Float.parseFloat(newYtdSal.get(26).toString());
		ytd_telephoneDeduction=Float.parseFloat(newYtdSal.get(27).toString());
		ytd_totalIncome=Float.parseFloat(newYtdSal.get(19).toString());
		ytd_totalDeduction=Float.parseFloat(newYtdSal.get(20).toString());
		ytdmedicalInsuranceDeduction=Float.parseFloat(newYtdSal.get(28).toString());
	
		
		int count=0;
		 String max="select MAX(ytd_id) as maxid from ytd_salary_details";
			try (Connection connection = DriverManager.getConnection( url, userid, password );
		            Statement stmt = connection.createStatement();){
					ResultSet rs=stmt.executeQuery(max);
					if(rs.next()){
					count=rs.getInt("maxid");
					count++;
					
					}
				}
		        catch (SQLException e)
		        {
		            e.printStackTrace();
		        }
		
		
		String saveNewYtd="INSERT INTO ytd_salary_details VALUES("+count+",'"+empid+"',"+ytd_basicEarn+","+ytd_basicEarnArrear+","+ytd_hra+","
				+ ""+ytd_hraArrear+","+ytd_specialAllowance+","+ytd_otherAllowance+","+ytd_medicalAllowance+","+ytd_medicalAllowanceArrear+","
				+ ""+ytd_conveyance+","+ytd_performanceIncentive+","+ytd_pf+","+ytd_pfArrear+","+ytd_professionaltax+","+ytd_incomeTax+","
				+ ""+ytd_d1misc+","+ytd_d2misc+","+ytd_canteenDeduction+","+ytd_totalIncome+","+ytd_totalDeduction+",'"+month_of_sal+"',"+ytd_executiveAllowance+","
				+ ""+ytd_executiveAllowanceArrear+","+ytd_intressSubsidy+","+ytd_ltaPaid+","+ytd_retainersBonus+","+ytd_sportClub+","+ytd_telephoneDeduction+","+ytdmedicalInsuranceDeduction+")";
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				stmt.executeUpdate(saveNewYtd);				
			}
		
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
	}

	
//This method is used for deleteing the excisting record of ytd salary to prevent 
//excess ytd salary calculation And  Duplication of records;
	public void getYtdSalaryStatus(String empid, String monthofSalary2) {
		
		//This query is only for initialization and to prevent the empty query exception
		//This query is never been executed & has no effect on project execution
		String deleteRecord="delete from ytd_salary_details where month_of_salary='"+empid+"'";
		
		
		String check="select * from ytd_salary_details where employee_code='"+empid+"'AND month_of_salary='"+monthofSalary2+"'";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(check);
				if(rs.next()){
					
					deleteRecord="delete from ytd_salary_details where employee_code='"+empid+"'AND month_of_salary='"+monthofSalary2+"'";
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				stmt.executeUpdate(deleteRecord);
				System.out.println("Deleted record for "+empid+" & "+monthofSalary2);
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		
	}


	public String getJoiningDate(String empid) {
		String date="select date_of_joining from employee_details where employee_code='"+empid+"'";
		String dateOfJoining="";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(date);
				if(rs.next()){
					dateOfJoining=rs.getString("date_of_joining");
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		return dateOfJoining;
	}


	public String getGender(String id) {
		String emp_gender="";
		String gen="select gender from employee_details where employee_code='"+id+"'";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(gen);
				if(rs.next()){
					emp_gender=rs.getString("gender");
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		return emp_gender;
	}
	
	
	
	public ArrayList getSal(StringBuffer mon,String empid){
		
		String sql="select * from salary_details where month_of_salary='"+mon+"' AND employee_code='"+empid+"'";
		System.out.println(sql);
		ArrayList monthlysal=new ArrayList();
		
		
		int basic_earn=0,basicearnarear=0,hra=0,hraarrear=0,specialallowance=0,otherallowance=0,medicalallowance=0,medicalallowancearrear=0,
				performanceincentive=0,providenetFund=0,providenetFundarrear=0,proftax=0,incometax=0,d1misc=0,d2misc=0,canteen=0,conveyance=0,
				total=0,totaldeduction=0,executiveallowance=0,executiveallowancearrear=0,intressSubsidy=0,ltapaid=0,retainersbonus=0,
				sportclubdeduction=0,telephonededuction=0,medicalInsurance=0;
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
			
				ResultSet rs=stmt.executeQuery(sql);
				if(rs.next()){
					basic_earn=rs.getInt("basic_earn");
					basicearnarear=rs.getInt("basic_earn_arrear");
					hra=rs.getInt("hra");
					hraarrear=rs.getInt("hra_arrear");
					specialallowance=rs.getInt("special_allowance");
					otherallowance=rs.getInt("other_allowance");
					medicalallowance=rs.getInt("medical_allowance");
					medicalallowancearrear=rs.getInt("medical_allowance_arrear");
					performanceincentive=rs.getInt("performance_incentive");
					providenetFund=rs.getInt("providant_fund");
					providenetFundarrear=rs.getInt("providant_fund_arrear");
					proftax=rs.getInt("p_tax");
					incometax=rs.getInt("income_tax");
					d1misc=rs.getInt("d1_misc_deduction");
					d2misc=rs.getInt("d2_misc_deduction");
					canteen=rs.getInt("canteen_deduction");
					conveyance=rs.getInt("convenyance");
					total=rs.getInt("total");
					totaldeduction=rs.getInt("total_deduction");
					executiveallowance=rs.getInt("executive_allowance");
					executiveallowancearrear=rs.getInt("executive_allowance_arrear");
					intressSubsidy=rs.getInt("intress_subsidy");
					ltapaid=rs.getInt("lta_paid");
					retainersbonus=rs.getInt("retainers_bonus");
					sportclubdeduction=rs.getInt("sport_club_deduction");
					telephonededuction=rs.getInt("telephone_deduction");
					medicalInsurance=rs.getInt("medical_insurance_deduction");
					
					
					
					
					monthlysal.add(basic_earn);
					monthlysal.add(basicearnarear);
					monthlysal.add(hra);
					monthlysal.add(hraarrear);
					monthlysal.add(specialallowance);
					monthlysal.add(otherallowance);
					monthlysal.add(medicalallowance);
					monthlysal.add(medicalallowancearrear);
					monthlysal.add(performanceincentive);
					monthlysal.add(providenetFund);
					monthlysal.add(providenetFundarrear);
					monthlysal.add(proftax);
					monthlysal.add(incometax);
					monthlysal.add(d1misc);
					monthlysal.add(d2misc);
					monthlysal.add(canteen);
					monthlysal.add(conveyance);
					monthlysal.add(total);
					monthlysal.add(totaldeduction);
					monthlysal.add(executiveallowance);
					monthlysal.add(executiveallowancearrear);
					monthlysal.add(intressSubsidy);
					monthlysal.add(ltapaid);
					monthlysal.add(retainersbonus);
					monthlysal.add(sportclubdeduction);
					monthlysal.add(telephonededuction);
					monthlysal.add(medicalInsurance);
				}
				
				else{
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
					monthlysal.add(0);
				}
			}
	        catch (SQLException e)
	        {
	            System.out.println(e.getMessage());
	        }
		
		
		return monthlysal;
		
	}


	public String getEmail(String empcode) {
		
		String email="";
		String gen="select email from employee_details where employee_code='"+empcode+"'";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(gen);
				if(rs.next()){
					email=rs.getString("email");
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		
		return email;
	}


	public String getName(String empcode) {
		
		String name="";
		String nameofemail="select employee_name from employee_details where employee_code='"+empcode+"'";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(nameofemail);
				if(rs.next()){
					name=rs.getString("employee_name");
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		
		return name;
		
		
	}


	public void saveBeforePrint(ArrayList newSalary) {
		// TODO Auto-generated method stub
		System.out.println("Saving before printing");
		
		String empid=newSalary.get(0).toString();
		float basicearn=Float.parseFloat(newSalary.get(1).toString());
		float hra=Float.parseFloat(newSalary.get(2).toString());
		float medicalAllowance=Float.parseFloat(newSalary.get(3).toString());
		float medicalAllowanceArrear=Float.parseFloat(newSalary.get(4).toString());
		float conveyance=Float.parseFloat(newSalary.get(5).toString());
		float specialAllowance=Float.parseFloat(newSalary.get(6).toString());
		float otherAllowance=Float.parseFloat(newSalary.get(7).toString());
		float executiveAllowance=Float.parseFloat(newSalary.get(8).toString());
		float executiveAllowanceArrear=Float.parseFloat(newSalary.get(9).toString());
		float intressSubsidy=Float.parseFloat(newSalary.get(10).toString());
		float performanceIncentive=Float.parseFloat(newSalary.get(11).toString());
		float ltaPaid=Float.parseFloat(newSalary.get(12).toString());
		float retainersBonus=Float.parseFloat(newSalary.get(13).toString());
		
		float pf=Float.parseFloat(newSalary.get(14).toString());
		float pfArrear=Float.parseFloat(newSalary.get(15).toString());
		float professionalTax=Float.parseFloat(newSalary.get(16).toString());
		float incomeTax=Float.parseFloat(newSalary.get(17).toString());
		float d1Misc=Float.parseFloat(newSalary.get(18).toString());
		float d2Misc=Float.parseFloat(newSalary.get(19).toString());
		float canteen=Float.parseFloat(newSalary.get(20).toString());
		float sportClub=Float.parseFloat(newSalary.get(21).toString());
		float telephoneDeduction=Float.parseFloat(newSalary.get(22).toString());
		
		float totalIncome=Float.parseFloat(newSalary.get(23).toString());
		float totalDeduction=Float.parseFloat(newSalary.get(24).toString());
		float netIncome=Float.parseFloat(newSalary.get(25).toString());
		
		String monthOfSal=newSalary.get(26).toString();
		
		float medicalInsurance=Float.parseFloat(newSalary.get(27).toString());
		
		String salaryInWords=newSalary.get(28).toString();
		
		String dateofSalary=newSalary.get(29).toString();
		
		
		String saveUpdatedSalary="update salary_details set basic_earn='"+basicearn+"',hra='"+hra+"',medical_allowance='"+medicalAllowance+"',"+ "medical_allowance_arrear='"+medicalAllowanceArrear+"',convenyance='"+conveyance+"',special_allowance='"+specialAllowance+"',"
				+ "other_allowance='"+otherAllowance+"',executive_allowance='"+executiveAllowance+"',executive_allowance_arrear='"+executiveAllowanceArrear+"',"
				+ "intress_subsidy='"+intressSubsidy+"',performance_incentive='"+performanceIncentive+"',lta_paid='"+ltaPaid+"',retainers_bonus='"+retainersBonus+"',"
				+ "providant_fund='"+pf+"',providant_fund_arrear='"+pfArrear+"',p_tax='"+professionalTax+"',income_tax='"+incomeTax+"',d1_misc_deduction='"+d1Misc+"',"
				+ "d2_misc_deduction='"+d2Misc+"',canteen_deduction='"+canteen+"',sport_club_deduction='"+sportClub+"',telephone_deduction='"+telephoneDeduction+"',"
				+ "total='"+totalIncome+"',total_deduction='"+totalDeduction+"',net_salary='"+netIncome+"',medical_insurance_deduction='"+medicalInsurance+"',net_sal_word='"+salaryInWords+"',date_of_salary='"+dateofSalary+"' where employee_code='"+empid+"' AND month_of_salary='"+monthOfSal+"'";
		
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				stmt.executeUpdate(saveUpdatedSalary);
				
				System.out.println("Saved before printing");
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		
		
	}


	public String getStatus(String empid) {
		// TODO Auto-generated method stub
		String currentStatus="select status from employee_details where employee_code='"+empid+"'";
		String status="";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(currentStatus);
				if(rs.next()){
					status=rs.getString("status");
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		return status;
		
	}


	public String getInactiveDate(String empid) {
		// TODO Auto-generated method stub
		String getInactiveDate="select Inactive_date from employee_details where employee_code='"+empid+"'";
		String inactiveDate="";
		try (Connection connection = DriverManager.getConnection( url, userid, password );
	            Statement stmt = connection.createStatement();){
				ResultSet rs=stmt.executeQuery(getInactiveDate);
				if(rs.next()){
					inactiveDate=rs.getString("Inactive_date");
				}
			}
	        catch (SQLException e)
	        {
	            e.printStackTrace();
	        }
		
		return inactiveDate;
		}
	


}
