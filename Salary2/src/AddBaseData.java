import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;



public class AddBaseData implements ActionListener{
	
	
	
	JFrame f1;
	JPanel jp;
	JPanel jpformBack,jpButtons;
	JLabel jlemp_code,jlemp_name,jlbasicSalary,jlmedical_Allowance,jlmedical_Allowance_Arrear,jlConveyance,jlspecial_Allowance,jlOther_Allowance,jlexecutive_allowance,
	jlexecutive_allowance_arrear,jlintress_subsidy,jlperformance_incentive,jllta_paid,jlretainers_bonus,jlprovident_fund,jlprovident_fund_arrear,
	jlp_tax,jlincome_tax,jld1_misc_deduction,jld2_misc_deduction,jlcanteen_deduction,jlsport_club,jl_teliphone_bill,jlmedical_insurance;
	
	JLabel imageLabelLogo,income,deduction;
	
	JTextField jtemp_code,jtemp_name,jtbasicSalary,jtmedical_Allowance,jtmedical_Allowance_Arrear,jtConveyance,jtspecial_Allowance,jtOther_Allowance,jtexecutive_allowance,
	jtexecutive_allowance_arrear,jtintress_subsidy,jtperformance_incentive,jtlta_paid,jtretainers_bonus,jtprovident_fund,jtprovident_fund_arrear,
	jtp_tax,jtincome_tax,jtd1_misc_deduction,jtd2_misc_deduction,jtcanteen_deduction,jtsport_club,jt_teliphone_bill,jtmedical_insurance;
	
	//for deductions
	JTextField jtmedical_Allowance_deduction,jtmedical_Allowance_Arrear_deduction,jtConveyance_deduction,jtspecial_Allowance_deduction,jtOther_Allowance_deduction,jtexecutive_allowance_deduction,
	jtexecutive_allowance_arrear_deduction,jtintress_subsidy_deduction,jtperformance_incentive_deduction,jtlta_paid_deduction,jtretainers_bonus_deduction,jtprovident_fund_deduction,jtprovident_fund_arrear_deduction,
	jtp_tax_deduction,jtincome_tax_deduction,jtd1_misc_deduction_deduction,jtd2_misc_deduction_deduction,jtcanteen_deduction_deduction,jtsport_club_deduction,jt_teliphone_bill_deduction,jtmedical_insurance_deduction;
	
	
	String emp_code,emp_name,basicSalary,medical_Allowance,medical_Allowance_Arrear,Conveyance,special_Allowance,Other_Allowance,executive_allowance,
	executive_allowance_arrear,intress_subsidy,performance_incentive,lta_paid,retainers_bonus,provident_fund,provident_fund_arrear,
	p_tax,income_tax,d1_misc_deduction,d2_misc_deduction,canteen_deduction,sport_club,teliphone_bill,medical_insurance;

	JButton jbSave,jbBack,jbReset;
	
	String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
    String userid = "root";
    String password = "yungry";
	
	public AddBaseData(String emp_code) {

		f1=new JFrame("Add Base Data");
		jp=new JPanel(){
			@Override
			public Dimension getPreferredSize(){
				return new Dimension(600,1100);
			}
		};
		jp.setLayout(null);
		jp.setBackground(Color.LIGHT_GRAY);
		
		jpformBack=new JPanel();
		jpformBack.setBackground(Color.GRAY);
		jpformBack.setBounds(230, 80, 600, 980);
		
		jpButtons=new JPanel();
		jpButtons.setBackground(Color.DARK_GRAY);
		jpButtons.setBounds(230, 1070, 600, 50);
		
		
		JScrollPane scrPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jp.add(scrPane);
		
		
		jbSave=new JButton("SAVE");
		jbSave.setActionCommand("save");
		
		
		jbBack=new JButton("BACK");
		jbBack.setActionCommand("back");
		
		
		jbReset=new JButton("RESET");
		jbReset.setActionCommand("reset");
		
		
		income=new JLabel("Incomes");
		deduction=new JLabel("Deductions");
		
		
		jlemp_code=new JLabel("Employee Code :");
		jlemp_name=new JLabel("Employee Name :");
		jlbasicSalary=new JLabel("Basic Salary :");
		jlmedical_Allowance=new JLabel("Medical Allowance :");
		jlmedical_Allowance_Arrear=new  JLabel("Medical Allowance Arrear:");
		jlConveyance=new JLabel("Conveyance :");
		jlspecial_Allowance=new JLabel("Special Allowance :");
		jlOther_Allowance=new JLabel("Other Allowance :");
		jlexecutive_allowance=new JLabel("Executrive Allowance");
		jlexecutive_allowance_arrear=new JLabel("Executrive Allowance Arrear");
		jlintress_subsidy=new JLabel("Intress Subsidy");
		jlperformance_incentive=new JLabel("Performance Incentive :");
		jllta_paid=new JLabel("LTA Paid :");
		jlretainers_bonus=new JLabel("Retainers Bonus :");
		jlprovident_fund=new JLabel("Provident fund :");
		jlprovident_fund_arrear=new JLabel("Provident fund Arrear");
		jlp_tax=new JLabel("Professional Tax:");
		jlincome_tax=new JLabel("Income Tax:");
		jld1_misc_deduction=new JLabel("D1 MISC Deduction:");
		jld2_misc_deduction=new JLabel("D1 MISC Deduction:");
		jlcanteen_deduction=new JLabel("Canteen Deduction:");
		jlsport_club=new JLabel("Sport Club:");
		jl_teliphone_bill=new JLabel("Telephone Deduction:");
		jlmedical_insurance=new JLabel("Medical Insurance");
		
		jtemp_code=new JTextField(45); 					jtemp_code.disable();
		jtemp_name=new JTextField(45);					jtemp_name.disable();
		jtbasicSalary=new JTextField(45);				jtbasicSalary.setText("0");
		jtmedical_Allowance=new JTextField(45);			jtmedical_Allowance.setText("0");
		jtmedical_Allowance_Arrear=new JTextField(45);	jtmedical_Allowance_Arrear.setText("0");
		jtConveyance=new JTextField(45);				jtConveyance.setText("0");
		jtspecial_Allowance=new JTextField(45);			jtspecial_Allowance.setText("0");
		jtOther_Allowance=new JTextField(45);			jtOther_Allowance.setText("0");
		jtexecutive_allowance=new JTextField(45);		jtexecutive_allowance.setText("0");
		jtexecutive_allowance_arrear=new JTextField(45);jtexecutive_allowance_arrear.setText("0");
		jtintress_subsidy=new JTextField(45);			jtintress_subsidy.setText("0");
		jtperformance_incentive=new JTextField(45);		jtperformance_incentive.setText("0");
		jtlta_paid=new JTextField(45);					jtlta_paid.setText("0");
		jtretainers_bonus=new JTextField(45);			jtretainers_bonus.setText("0");
		jtprovident_fund=new JTextField(45);			jtprovident_fund.setText("0");
		jtprovident_fund_arrear=new JTextField(45);		jtprovident_fund_arrear.setText("0");
		jtp_tax=new JTextField(45);						jtp_tax.setText("0");
		jtincome_tax=new JTextField(45);				jtincome_tax.setText("0");
		jtd1_misc_deduction=new JTextField(45);			jtd1_misc_deduction.setText("0");
		jtd2_misc_deduction=new JTextField(45);			jtd2_misc_deduction.setText("0");
		jtcanteen_deduction=new JTextField(45);			jtcanteen_deduction.setText("0");
		jtsport_club=new JTextField(45);				jtsport_club.setText("0");
		jt_teliphone_bill=new JTextField(45);			jt_teliphone_bill.setText("0");
		jtmedical_insurance=new JTextField(45);			jtmedical_insurance.setText("0");
		
		
		jtmedical_Allowance_deduction=new JTextField(45);			jtmedical_Allowance_deduction.setText("0");
		jtmedical_Allowance_Arrear_deduction=new JTextField(45);	jtmedical_Allowance_Arrear_deduction.setText("0");
		jtConveyance_deduction=new JTextField(45);					jtConveyance_deduction.setText("0");
		jtspecial_Allowance_deduction=new JTextField(45);			jtspecial_Allowance_deduction.setText("0");
		jtOther_Allowance_deduction=new JTextField(45);				jtOther_Allowance_deduction.setText("0");
		jtexecutive_allowance_deduction=new JTextField(45);			jtexecutive_allowance_deduction.setText("0");
		jtexecutive_allowance_arrear_deduction=new JTextField(45);	jtexecutive_allowance_arrear_deduction.setText("0");
		jtintress_subsidy_deduction=new JTextField(45);				jtintress_subsidy_deduction.setText("0");
		jtperformance_incentive_deduction=new JTextField(45);		jtperformance_incentive_deduction.setText("0");
		jtlta_paid_deduction=new JTextField(45);					jtlta_paid_deduction.setText("0");
		jtretainers_bonus_deduction=new JTextField(45);				jtretainers_bonus_deduction.setText("0");
		jtprovident_fund_deduction=new JTextField(45);				jtprovident_fund_deduction.setText("0");
		jtprovident_fund_arrear_deduction=new JTextField(45);		jtprovident_fund_arrear_deduction.setText("0");
		jtp_tax_deduction=new JTextField(45);						jtp_tax_deduction.setText("0");
		jtincome_tax_deduction=new JTextField(45);					jtincome_tax_deduction.setText("0");
		jtd1_misc_deduction_deduction=new JTextField(45);			jtd1_misc_deduction_deduction.setText("0");
		jtd2_misc_deduction_deduction=new JTextField(45);			jtd2_misc_deduction_deduction.setText("0");
		jtcanteen_deduction_deduction=new JTextField(45);			jtcanteen_deduction_deduction.setText("0");
		jtsport_club_deduction=new JTextField(45);					jtsport_club_deduction.setText("0");
		jt_teliphone_bill_deduction=new JTextField(45);				jt_teliphone_bill_deduction.setText("0");
		jtmedical_insurance_deduction=new JTextField(45);			jtmedical_insurance_deduction.setText("0");
		
		
		
		jtmedical_Allowance_deduction.disable();
		jtmedical_Allowance_Arrear_deduction.disable();
		jtConveyance_deduction.disable();
		jtspecial_Allowance_deduction.disable();
		jtOther_Allowance_deduction.disable();
		jtexecutive_allowance_deduction.disable();
		jtexecutive_allowance_arrear_deduction.disable();
		jtintress_subsidy_deduction.disable();
		jtperformance_incentive_deduction.disable();
		jtlta_paid_deduction.disable();
		jtretainers_bonus_deduction.disable();
		jtprovident_fund_deduction.disable();
		jtprovident_fund_arrear_deduction.disable();
		jtp_tax_deduction.disable();
		jtincome_tax_deduction.disable();
		jtd1_misc_deduction_deduction.disable();
		jtd2_misc_deduction_deduction.disable();
		jtcanteen_deduction_deduction.disable();
		jtsport_club_deduction.disable();		
		jt_teliphone_bill_deduction.disable();
		jtmedical_insurance_deduction.disable();
		
		
		 jp.add(jlemp_code);
		 jp.add(jlemp_name);
		 jp.add(jlbasicSalary);
		 jp.add(jlmedical_Allowance);
		 jp.add(jlmedical_Allowance_Arrear);
		 jp.add(jlConveyance);
		 jp.add(jlspecial_Allowance);
		 jp.add(jlOther_Allowance);
		 jp.add(jlexecutive_allowance);
		 jp.add(jlexecutive_allowance_arrear);
		 jp.add(jlintress_subsidy);
		 jp.add(jlperformance_incentive);
		 jp.add(jllta_paid);
		 jp.add(jlretainers_bonus);
		 jp.add(jlprovident_fund);
		 jp.add(jlprovident_fund_arrear);
		 jp.add(jlp_tax);
		 jp.add(jlincome_tax);
		 jp.add(jld1_misc_deduction);
		 jp.add(jld2_misc_deduction);
		 jp.add(jlcanteen_deduction);
		 jp.add(jlsport_club);
		 jp.add(jl_teliphone_bill);
		 jp.add(jlmedical_insurance);
		
		 jp.add(jtemp_code);
		 jp.add(jtemp_name);
		 jp.add(jtbasicSalary);
		 jp.add(jtmedical_Allowance);
		 jp.add(jtmedical_Allowance_Arrear);
		 jp.add(jtConveyance);
		 jp.add(jtspecial_Allowance);
		 jp.add(jtOther_Allowance);
		 jp.add(jtexecutive_allowance);
		 jp.add(jtexecutive_allowance_arrear);
		 jp.add(jtintress_subsidy);
		 jp.add(jtperformance_incentive);
		 jp.add(jtlta_paid);
		 jp.add(jtretainers_bonus);
		 jp.add(jtprovident_fund);
		 jp.add(jtprovident_fund_arrear);
		 jp.add(jtp_tax);
		 jp.add(jtincome_tax);
		 jp.add(jtd1_misc_deduction);
		 jp.add(jtd2_misc_deduction);
		 jp.add(jtcanteen_deduction);
		 jp.add(jtsport_club);
		 jp.add(jt_teliphone_bill);
		 jp.add(jtmedical_insurance);
		 
		 //for deductions
		 jp.add(jtmedical_Allowance_deduction);
		 jp.add(jtmedical_Allowance_Arrear_deduction);
		 jp.add(jtConveyance_deduction);
		 jp.add(jtspecial_Allowance_deduction);
		 jp.add(jtOther_Allowance_deduction);
		 jp.add(jtexecutive_allowance_deduction);
		 jp.add(jtexecutive_allowance_arrear_deduction);
		 jp.add(jtintress_subsidy_deduction);
		 jp.add(jtperformance_incentive_deduction);
		 jp.add(jtlta_paid_deduction);
		 jp.add(jtretainers_bonus_deduction);
		 jp.add(jtprovident_fund_deduction);
		 jp.add(jtprovident_fund_arrear_deduction);
		 jp.add(jtp_tax_deduction);
		 jp.add(jtincome_tax_deduction);
		 jp.add(jtd1_misc_deduction_deduction);
		 jp.add(jtd2_misc_deduction_deduction);
		 jp.add(jtcanteen_deduction_deduction);
		 jp.add(jtsport_club_deduction);
		 jp.add(jt_teliphone_bill_deduction);
		 jp.add(jtmedical_insurance_deduction);
		 
		 jp.add(income);
		 jp.add(deduction);
		 jp.add(jbSave);
		// jp.add(jbBack);
		 jp.add(jbReset);
		 jp.add(jpformBack);
		 jp.add(jpButtons);
		 
		//code for logo & Co.Name
	        imageLabelLogo=new JLabel(new ImageIcon("new/logo.png"));
	        imageLabelLogo.setBounds(235, 0, 200, 100);
	        jp.add(imageLabelLogo);
	        
	       
		//Set Bouns for All form Controls

			jlemp_code.setBounds(250, 90, 150, 20);  			jtemp_code.setBounds(450, 90, 150, 20);
			
			jlemp_name.setBounds(250, 130, 150, 20);				jtemp_name.setBounds(450, 130, 150, 20);
			
			jlbasicSalary.setBounds(250, 170, 150, 20); 			jtbasicSalary.setBounds(450, 170, 150, 20);
			
																	income.setBounds(500, 200, 150, 20); deduction.setBounds(700, 200, 150, 20);
			
			jlmedical_Allowance.setBounds(250, 230, 150, 20); 	jtmedical_Allowance.setBounds(450, 230, 150, 20);	jtmedical_Allowance_deduction.setBounds(650, 230, 150, 20);
			
			jlmedical_Allowance_Arrear.setBounds(250, 270, 150, 20); 	jtmedical_Allowance_Arrear.setBounds(450, 270, 150, 20);	jtmedical_Allowance_Arrear_deduction.setBounds(650, 270, 150, 20);
			
			jlConveyance.setBounds(250, 310, 150, 20); 			jtConveyance.setBounds(450, 310, 150, 20);		jtConveyance_deduction.setBounds(650, 310, 150, 20);
			
			jlspecial_Allowance.setBounds(250, 350, 150, 20); 	jtspecial_Allowance.setBounds(450, 350, 150, 20);	jtspecial_Allowance_deduction.setBounds(650, 350, 150, 20);
			
			jlOther_Allowance.setBounds(250, 390, 150, 20); 		jtOther_Allowance.setBounds(450, 390, 150, 20);	jtOther_Allowance_deduction.setBounds(650, 390, 150, 20);
			
			jlexecutive_allowance.setBounds(250, 430, 150, 20);					jtexecutive_allowance.setBounds(450, 430, 150, 20);	jtexecutive_allowance_deduction.setBounds(650, 430, 150, 20);
			
			jlexecutive_allowance_arrear.setBounds(250, 470, 150, 20);			jtexecutive_allowance_arrear.setBounds(450, 470, 150, 20);	jtexecutive_allowance_arrear_deduction.setBounds(650, 470, 150, 20);
			
			jlintress_subsidy.setBounds(250, 510, 150, 20);						jtintress_subsidy.setBounds(450, 510, 150, 20);	jtintress_subsidy_deduction.setBounds(650, 510, 150, 20);
			
			jlperformance_incentive.setBounds(250, 550, 150, 20);				jtperformance_incentive.setBounds(450, 550, 150, 20);		jtperformance_incentive_deduction.setBounds(650, 550, 150, 20);
			
			jllta_paid.setBounds(250, 590, 150, 20);								jtlta_paid.setBounds(450, 590, 150, 20);		jtlta_paid_deduction.setBounds(650, 590, 150, 20);
			
			jlretainers_bonus.setBounds(250, 630, 150, 20);						jtretainers_bonus.setBounds(450, 630, 150, 20);		jtretainers_bonus_deduction.setBounds(650, 630, 150, 20);
			
			jlprovident_fund.setBounds(250, 670, 150, 20);						jtprovident_fund.setBounds(450, 670, 150, 20);		jtprovident_fund_deduction.setBounds(650, 670, 150, 20);
			
			jlprovident_fund_arrear.setBounds(250, 710, 150, 20);				jtprovident_fund_arrear.setBounds(450,710, 150, 20);	jtprovident_fund_arrear_deduction.setBounds(650,710, 150, 20);
			
			jlp_tax.setBounds(250,750, 150, 20);									jtp_tax.setBounds(450, 750, 150, 20);		jtp_tax_deduction.setBounds(650, 750, 150, 20);
			
			jlincome_tax.setBounds(250, 790, 150, 20);							jtincome_tax.setBounds(450, 790, 150, 20);		jtincome_tax_deduction.setBounds(650, 790, 150, 20);
			
			jld1_misc_deduction.setBounds(250, 830, 150, 20);					jtd1_misc_deduction.setBounds(450, 830, 150, 20);	jtd1_misc_deduction_deduction.setBounds(650, 830, 150, 20);
			
			jld2_misc_deduction.setBounds(250, 870, 150, 20);					jtd2_misc_deduction.setBounds(450, 870, 150, 20);		jtd2_misc_deduction_deduction.setBounds(650, 870, 150, 20);
			
			jlcanteen_deduction.setBounds(250, 910, 150, 20);					jtcanteen_deduction.setBounds(450, 910, 150, 20);		jtcanteen_deduction_deduction.setBounds(650, 910, 150, 20);
			
			jlsport_club.setBounds(250, 950, 150, 20);							jtsport_club.setBounds(450, 950, 150, 20);		jtsport_club_deduction.setBounds(650, 950, 150, 20);
			
			jl_teliphone_bill.setBounds(250, 990, 150, 20);						jt_teliphone_bill.setBounds(450, 990, 150, 20);		jt_teliphone_bill_deduction.setBounds(650, 990, 150, 20);
			
			jlmedical_insurance.setBounds(250, 1030, 150, 20);					jtmedical_insurance.setBounds(450, 1030, 150, 20);	jtmedical_insurance_deduction.setBounds(650, 1030, 150, 20);
			
			jbBack.setBounds(300,1080, 100, 30);
			jbSave.setBounds(430,1080, 100, 30);
			jbReset.setBounds(560,1080, 100, 30);
		
		
		//jp.setBackground(Color.GRAY);
		jp.add(scrPane);
	
		f1.add(jp);
		
		f1.setSize(1024, 750);
		jbSave.addActionListener(this);
		jbReset.addActionListener(this);
		jbBack.addActionListener(this);
		
		f1.getContentPane().add(new JScrollPane(jp),BorderLayout.CENTER);
		fillData(emp_code);
		
		 Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	        int x=dim.width/2-f1.getSize().width/2;
	        int y=dim.height/2-f1.getSize().height/2;
	        
	        f1.setLocation(x,0);
	        f1.setVisible(true);
	        f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String args[]){
		new AddBaseData("SK4092");
	}
	
	
	void fillData(String empCode){
		
			
	        String sql = "SELECT * FROM employee_details where employee_code='"+empCode+"'";
		 try {
	        	Connection connection = DriverManager.getConnection( url, userid, password );
	       
	                Statement stmt = connection.createStatement();
	                ResultSet rs = stmt.executeQuery( sql );
	                
	                while(rs.next()){
	                	
	                	
	                	emp_code=rs.getString("employee_code");
	                	emp_name=rs.getString("employee_name");
	                	
	                	jtemp_code.setText(emp_code);
	                	jtemp_name.setText(emp_name); 	
	                }
	}
	           
	            catch (SQLException e)
	            {
	                System.out.println( e.getMessage() );
	            }
	        
	        
	}
	
	
	void saveData(){
		
		
		emp_code=jtemp_code.getText();
		basicSalary=jtbasicSalary.getText();
		medical_Allowance=jtmedical_Allowance.getText();
		medical_Allowance_Arrear=jtmedical_Allowance_Arrear.getText();
		Conveyance=jtConveyance.getText();
		special_Allowance=jtspecial_Allowance.getText();
		Other_Allowance=jtOther_Allowance.getText();
		executive_allowance=jtexecutive_allowance.getText();
		executive_allowance_arrear=jtexecutive_allowance_arrear.getText();
		intress_subsidy=jtintress_subsidy.getText();
		performance_incentive=jtperformance_incentive.getText();
		lta_paid=jtlta_paid.getText();
		retainers_bonus=jtretainers_bonus.getText();
		provident_fund=jtprovident_fund.getText();
		provident_fund_arrear=jtprovident_fund_arrear.getText();
		p_tax=jtp_tax.getText();
		income_tax=jtincome_tax.getText();
		d1_misc_deduction=jtd1_misc_deduction.getText();
		d2_misc_deduction=jtd2_misc_deduction.getText();
		canteen_deduction=jtcanteen_deduction.getText();
		sport_club=jtsport_club.getText();
		teliphone_bill=jt_teliphone_bill.getText();
		medical_insurance=jtmedical_insurance.getText();
		
		
		
		
		
		
		
			String dataCount="select COUNT(*) as cnt from basedata"; //counting previous records
			int cnt=0;
			 try {
		        	Connection connection = DriverManager.getConnection( url, userid, password );
		       
		                Statement stmt = connection.createStatement();
		                ResultSet rs = stmt.executeQuery( dataCount );
		                
		                if(rs.next()){
		                	cnt=rs.getInt("cnt");     
		                	System.out.println(cnt);
		                	cnt++;
		                	}
		                //inserting new record
		                String sql = "INSERT INTO basedata VALUES("+cnt+",'"+emp_code+"',"
		                		+ ""+basicSalary+","+medical_Allowance+","+medical_Allowance_Arrear+","+Conveyance+","+special_Allowance+","
		                		+ ""+Other_Allowance+","+executive_allowance+","+executive_allowance_arrear+","+intress_subsidy+","
		                		+ ""+performance_incentive+","+lta_paid+","+retainers_bonus+","+provident_fund+","
		                		+ ""+provident_fund_arrear+","+p_tax+","+income_tax+","+d1_misc_deduction+","+d2_misc_deduction+","
		                		+ ""+canteen_deduction+","+sport_club+","+teliphone_bill+",'"+medical_insurance+"')";     
		                
		                stmt.executeUpdate(sql);
		}
		           
		            catch (SQLException e)
		            {
		                System.out.println( e.getMessage() );
		            }
		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		if(ae.getActionCommand()=="save"){
			
			saveData();
			new menu(0);
			f1.setVisible(false);
		}
		
		
		if(ae.getActionCommand()=="reset"){
			
			
			jtbasicSalary.setText("0");
			jtmedical_Allowance.setText("0");
			jtmedical_Allowance_Arrear.setText("0");
			jtConveyance.setText("0");
			jtspecial_Allowance.setText("0");
			jtOther_Allowance.setText("0");
			jtexecutive_allowance.setText("0");
			jtexecutive_allowance_arrear.setText("0");
			jtintress_subsidy.setText("0");
			jtperformance_incentive.setText("0");
			jtlta_paid.setText("0");
			jtretainers_bonus.setText("0");
			jtprovident_fund.setText("0");
			jtprovident_fund_arrear.setText("0");
			jtp_tax.setText("0");
			jtincome_tax.setText("0");
			jtd1_misc_deduction.setText("0");
			jtd2_misc_deduction.setText("0");
			jtcanteen_deduction.setText("0");
			jtsport_club.setText("0");
			jt_teliphone_bill.setText("0");
			jtmedical_insurance.setText("0");
			
			jtmedical_Allowance_deduction.setText("0");
			jtmedical_Allowance_Arrear_deduction.setText("0");
			jtConveyance_deduction.setText("0");
			jtspecial_Allowance_deduction.setText("0");
			jtOther_Allowance_deduction.setText("0");
			jtexecutive_allowance_deduction.setText("0");
			jtexecutive_allowance_arrear_deduction.setText("0");
			jtintress_subsidy_deduction.setText("0");
			jtperformance_incentive_deduction.setText("0");
			jtlta_paid_deduction.setText("0");
			jtretainers_bonus_deduction.setText("0");
			jtprovident_fund_deduction.setText("0");
			jtprovident_fund_arrear_deduction.setText("0");
			jtp_tax_deduction.setText("0");
			jtincome_tax_deduction.setText("0");
			jtd1_misc_deduction_deduction.setText("0");
			jtd2_misc_deduction_deduction.setText("0");
			jtcanteen_deduction_deduction.setText("0");
			jtsport_club_deduction.setText("0");
			jt_teliphone_bill_deduction.setText("0");
			jtmedical_insurance_deduction.setText("0");
		}
	}
	
	
	
	
}