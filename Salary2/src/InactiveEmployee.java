import java.awt.Color;
import java.awt.Menu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;


public class InactiveEmployee implements ActionListener{
	
	
	ArrayList columnNames;
    ArrayList data ;
    JFrame frame;
    String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
    String userid = "root";
    String password = "yungry";
    JTable table;
    JButton changeStatus,back;
	
    public InactiveEmployee() {
    	frame=new JFrame("Inactive/Active Employee");
    	columnNames= new ArrayList();
        data= new ArrayList();
        JPanel jp=new JPanel(null);
        changeStatus=new JButton("Change Status");
        back=new JButton("Employee List");
        
	
	 String sql = "SELECT * FROM employee_details order by sr_no";

     
     
     try (Connection connection = DriverManager.getConnection( url, userid, password );
         Statement stmt = connection.createStatement();
         ResultSet rs = stmt.executeQuery( sql ))
     {
    	 System.out.println(rs);
         ResultSetMetaData md = rs.getMetaData();
         int columns = md.getColumnCount();

         
         for (int i = 1; i <= columns; i++)
         {
             columnNames.add( md.getColumnName(i) );
         }

         
         while (rs.next())
         {
             ArrayList row = new ArrayList(columns);

             for (int i = 1; i <= columns; i++)
             {
                 row.add( rs.getObject(i) );
             }

             data.add( row );
         }
     }
     catch (SQLException e)
     {
         System.out.println( e.getMessage() );
     }

    
     Vector columnNamesVector = new Vector();
     Vector dataVector = new Vector();

     for (int i = 0; i < data.size(); i++)
     {
         ArrayList subArray = (ArrayList)data.get(i);
         Vector subVector = new Vector();
         for (int j = 0; j < subArray.size(); j++)
         {
             subVector.add(subArray.get(j));
         }
         dataVector.add(subVector);
     }

     for (int i = 0; i < columnNames.size(); i++ )
         columnNamesVector.add(columnNames.get(i));

      
     table = new JTable(dataVector, columnNamesVector)
     {
         public Class getColumnClass(int column)
         {
             for (int row = 0; row < getRowCount(); row++)
             {
                 Object o = getValueAt(row, column);

                 if (o != null)
                 {
                     return o.getClass();
                 }
             }
             

             return Object.class;
         }
         
         public boolean isCellEditable(int row, int column){ 
         	
	            return false;  
	        }
         
         
        
     };

  
     table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
     JScrollPane scrollPane = new JScrollPane( table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
     		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
     
 	scrollPane.setBounds(30,80,950,400);
 	
 	changeStatus.setBounds(830,580,150,40);
 	changeStatus.setActionCommand("CHANGE");
 	changeStatus.addActionListener(this);
 	
 	back.setBounds(630,580,150,40);
 	back.setActionCommand("BACK");
 	back.addActionListener(this);
 
      
    jp.add(scrollPane);
    jp.add(changeStatus);
    jp.add(back);
    frame.add(jp);
    frame.setSize(1024,730);
    jp.setBackground(Color.LIGHT_GRAY);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getActionCommand()=="CHANGE"){
			int x=table.getSelectedRow();
			String empid=table.getValueAt(x, 1).toString();
			String nameofemp=table.getValueAt(x, 2).toString();
			//System.out.println(empid);
			frame.setVisible(false);
			new ChangeStatusForm(empid,nameofemp);
		}
		
		if(e.getActionCommand()=="BACK"){
			frame.setVisible(false);
			new menu(0);
		}
		
	}
}
