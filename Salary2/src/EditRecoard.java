
import javax.swing.*;

import net.sourceforge.jdatepicker.impl.UtilDateModel;

import java.awt.event.*;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EditRecoard implements ActionListener
 
    {
	JFrame f1;
	JPanel jp,jpButton;
	JLabel imageLabelLogo,jlempcode,jlempname,jlbname,jlbac,jldesig,jlpan,jlpf,jljoining,jlcompany_name,jlgender,jlemail,jlcontact;
	JTextField jtempcode,jtempname,jtbname,jtbac,jtdesig,jtpan,jtpf,jtjoining,jtcompany_name,jtemail,jtcontact;
	JComboBox gender;
	JButton jbSave,jbBack,jbReset;
	int i=0;
	String srno,employee_code,employee_name,employee_gender,bank_name,bank_account,designation,pan_no,pf_no,
		date_of_joining,company_name,email,contact;
	
	
	String []company_name_array={"ENGINEERING CONSULTANCY SERVICES PVT.LTD ","TECHNOLOGIES"};
	String []gen={"Male","Female"};
	JComboBox company_combo;
	public EditRecoard(String x)
	{
		
		f1=new JFrame("Edit Employee");
		f1.setLayout(null);
		f1.getContentPane().setBackground(Color.LIGHT_GRAY);
		jp=new JPanel();
		jpButton=new JPanel();
		
		jlcompany_name=new JLabel("Company Name:");
		jlempcode=new JLabel("Employee Code:");
		jlempname=new JLabel("Employee Name:");
		jlgender=new JLabel("Gender");
		jlbname=new JLabel("Bank Name:");
		jlbac=new JLabel("Bank Account No. :");
		jldesig=new JLabel("Designation :");
		jlpan=new JLabel("Pan No. :");
		jlpf=new JLabel("Providant fund No. :");
		jljoining=new JLabel("Date of Joining :");
		jlemail=new JLabel("Email Id :");
		jlcontact=new JLabel("Contact No :");
		
		
		company_combo=new JComboBox(company_name_array);
		jtempcode=new JTextField(45);
		jtempname=new JTextField(45); 
		gender=new JComboBox(gen);
		jtbname=new JTextField(45);
		jtbac=new JTextField(45);
		jtdesig=new JTextField(45);
		jtpan=new JTextField(45);
		jtpf=new JTextField(45);
		jtjoining=new JTextField(45);
		jtemail=new JTextField(45);
		jtcontact=new JTextField(45);
		
		
        jbSave=new JButton("Save");
		jbSave.setActionCommand("save");
		
		jbReset=new JButton("RESET");
		jbReset.setActionCommand("reset");
		
		jbBack=new JButton("BACK");
		jbBack.setActionCommand("back");
		
		f1.add(jlcompany_name);
		f1.add(jlbac);
		f1.add(jlbname);
		f1.add(jlgender);
		f1.add(jldesig);
		f1.add(jlempcode);
		f1.add(jlempname);
		f1.add(jljoining);
		f1.add(jlpan);
		f1.add(jlpf);
		f1.add(jlemail);
		f1.add(jlcontact);
		
		f1.add(company_combo);
		f1.add(jtbac);
		f1.add(jtbname);
		f1.add(gender);
		f1.add(jtdesig);
		f1.add(jtempcode);
		f1.add(jtempname);
		f1.add(jtjoining);
		f1.add(jtpan);
		f1.add(jtpf);
		f1.add(jtemail);
		f1.add(jtcontact);
		
		f1.add(jbSave);
		f1.add(jbBack);
		//f1.add(jbReset);
	
		f1.add(jp);
		f1.add(jpButton);

		
		f1.setSize(1024,730);
		f1.setVisible(true);
		f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		jp.setBounds(250,85,600,500);
		jp.setBackground(Color.GRAY);
		
		jpButton.setBounds(250,610,600,50);
		jpButton.setBackground(Color.DARK_GRAY);
		
		

		jlcompany_name.setBounds(300, 100, 400, 20);
		jlempcode.setBounds(300,140,400,20);
		jlempname.setBounds(300,180,400,20);
		jlgender.setBounds(300,220,400,20);
		jlbname.setBounds(300,260,400,20);
		jlbac.setBounds(300,300,400,20);
		jldesig.setBounds(300,340,320,20);
		jlpan.setBounds(300,380,400,20);
		jlpf.setBounds(300,420,400,20);
		jljoining.setBounds(300,460,400,20);
		jlemail.setBounds(300,500,400,20);
		jlcontact.setBounds(300,540,400,20);
		
		company_combo.setBounds(450,100,350,20);
		jtempcode.setBounds	(450,140,150,20);
		jtempname.setBounds	(450,180,150,20);
		gender.setBounds	(450,220,150,20);
		jtbname.setBounds	(450,260,150,20);
		jtbac.setBounds		(450,300,150,20);
		jtdesig.setBounds	(450,340,150,20);
		jtpan.setBounds		(450,380,150,20);
		jtpf.setBounds		(450,420,150,25);
		jtjoining.setBounds	(450,460,150,20);
		jtemail.setBounds	(450,500,150,20);
		jtcontact.setBounds	(450,540,150,20);

		jbSave.setBounds(400,620,100,30);
		jbBack.setBounds(550,620,100,30);
		jbReset.setBounds(450,620,100,30);
		
		jbSave.addActionListener(this);
		jbReset.addActionListener(this);
		jbBack.addActionListener(this);
		
		//code for logo & Co.Name
        imageLabelLogo=new JLabel(new ImageIcon("new/logo.png"));
        imageLabelLogo.setBounds(250, 0, 200, 100);
        f1.add(imageLabelLogo);
        
      
		//db
		
		 String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
	        String userid = "root";
	        String password = "yungry";
	        String sql = "SELECT * FROM employee_details where employee_code='"+x+"'";
	        
	        
	        
	        try {
	        	Connection connection = DriverManager.getConnection( url, userid, password );
	       
	                Statement stmt = connection.createStatement();
	                ResultSet rs = stmt.executeQuery( sql );
	                
	                while(rs.next()){
	                	
	                		company_name=rs.getString("company_name");
	                		srno=rs.getString("sr_no");
	                		employee_code=rs.getString("employee_code");
	                		employee_name=rs.getString("employee_name");
	                		employee_gender=rs.getString("gender");
	                		bank_name=rs.getString("bank_name");
	                		bank_account=rs.getString("bank_account");
	                		designation=rs.getString("designation");
	                		pan_no=rs.getString("pan_number");
	                		pf_no=rs.getString("pf_number");
	                		date_of_joining=rs.getString("date_of_joining");
	                		email=rs.getString("email");
	                		contact=rs.getString("contact");
	                		
	                		
	                		
	                		
	                		
	                		company_combo.setSelectedItem(company_name);
	                		jtempname.setText(employee_name);
	                		jtempcode.setText(employee_code);
	                		jtbname.setText(bank_name);
	                		jtbac.setText(bank_account);
	                		jtdesig.setText(designation);
	                		jtpan.setText(pan_no);
	                		jtpf.setText(pf_no);
	                		jtjoining.setText(date_of_joining);
	                		jtemail.setText(email);
	                		jtcontact.setText(contact);
	                		
	                		if(employee_gender.equals("Male"));
	                		{
	                			gender.setSelectedIndex(0);
	                		}
	                		if(employee_gender.equals("Female")) {
	                			gender.setSelectedIndex(1);
	                		}
						
	                }
	}
	           
	            catch (SQLException e)
	            {
	                System.out.println( e.getMessage() );
	            }
	        
	        
		
		
		
	}

	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getActionCommand()=="save"){
			updatedata();
		}
		
		if(ae.getActionCommand()=="back"){
			new menu(0);
			f1.setVisible(false);
		}
		
		if(ae.getActionCommand()=="reset"){
			
			jtempcode.setText("");
			jtempname.setText("");
			jtbname.setText("");
			jtbac.setText("");
			jtdesig.setText("");
			jtpan.setText("");
			jtpf.setText("");
			jtjoining.setText("");
			
		}
		
		
		
	    	
	}

  /* public static void main(String []args)
	{
	  new EditRecoard(1);
	}*/
	
	public void updatedata(){
		
		
		String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
        String userid = "root";
        String password = "yungry";
        
        company_name=(String) company_combo.getSelectedItem();
        employee_code=jtempcode.getText();
		employee_name=jtempname.getText();
		employee_gender=gender.getSelectedItem().toString();
		bank_name=jtbname.getText();
		bank_account=jtbac.getText();
		designation=jtdesig.getText();
		pan_no=jtpan.getText();
		pf_no=jtpf.getText();
		date_of_joining=jtjoining.getText();
		email=jtemail.getText();
		contact=jtcontact.getText();
		
		System.out.println(employee_code+" "+employee_name);
		
        String sql="update employee_details set employee_code='"+employee_code+"',employee_name='"+employee_name+"',bank_name='"+bank_name+"',bank_account='"+bank_account+"',designation='"+designation+"',pan_number='"+pan_no+"',pf_number='"+pf_no+"',date_of_joining='"+date_of_joining+"',company_name='"+company_name+"',gender='"+employee_gender+"',email='"+email+"',contact='"+contact+"' where sr_no='"+srno+"'";
        try{
        Connection connection = DriverManager.getConnection( url, userid, password );
        Statement stmt = connection.createStatement();
        stmt.executeUpdate(sql);
        }
        catch(Exception e){
        	System.out.println(e);
        }
        
        new menu(0);
        f1.setVisible(false);
		
	}
	
	public static void main(String[] a){
		new EditRecoard("SK4092");
	}
	
 }
