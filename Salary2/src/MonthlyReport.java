

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import java.awt.Component;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

public class MonthlyReport implements FocusListener {
	
	ArrayList columnNames;
    ArrayList data ;
    JFrame frame;
    JButton bt1;
    JTextField jtxt;
    JTable table;
    String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
    String userid = "root";
    String password = "yungry";
    JComboBox jmonth,jyear;
    
    String []month={"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEPT","OCT","NOV","DEC"};
	String []year={"2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025"};
	
	
    public MonthlyReport() {
    	System.out.println("Monthly Report");
    	
    	columnNames= new ArrayList();
        data= new ArrayList();
        frame=new JFrame("Monthly Report");
        bt1=new JButton("EDIT");
        jtxt=new JTextField();
        
        jmonth=new JComboBox(month);
        jyear=new JComboBox(year);
        
        String monthReceived=findMonth();
     
        String sql = "SELECT * FROM salary_details where month_of_salary='"+monthReceived+"'";

        bt1.setActionCommand("Edit");
       
        try (Connection connection = DriverManager.getConnection( url, userid, password );
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery( sql ))
        {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            
            for (int i = 1; i <= columns; i++)
            {
                columnNames.add( md.getColumnName(i) );
            }

            
            while (rs.next())
            {
                ArrayList row = new ArrayList(columns);

                for (int i = 1; i <= columns; i++)
                {
                    row.add( rs.getObject(i) );
                }

                data.add( row );
            }
        }
        catch (SQLException e)
        {
            System.out.println( e.getMessage() );
        }

       
        Vector columnNamesVector = new Vector();
        Vector dataVector = new Vector();

        for (int i = 0; i < data.size(); i++)
        {
            ArrayList subArray = (ArrayList)data.get(i);
            Vector subVector = new Vector();
            for (int j = 0; j < subArray.size(); j++)
            {
                subVector.add(subArray.get(j));
            }
            dataVector.add(subVector);
        }

        for (int i = 0; i < columnNames.size(); i++ )
            columnNamesVector.add(columnNames.get(i));

         
        table = new JTable(dataVector, columnNamesVector)
        {
            public Class getColumnClass(int column)
            {
                for (int row = 0; row < getRowCount(); row++)
                {
                    Object o = getValueAt(row, column);

                    if (o != null)
                    {
                        return o.getClass();
                    }
                }
                

                return Object.class;
            }
            
            public boolean isCellEditable(int row, int column){ 
            	
	            return false;  
	        }
            
            
           
        };
     
     

        JScrollPane scrollPane = new JScrollPane( table );
        scrollPane.setBounds(30,40,900,400);
        
        JScrollPane scrollPane2 = new JScrollPane( table );
        scrollPane2.setBounds(30,40,900,400);
         
       
       frame.setSize(1000, 800);
       frame.setVisible(true);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
     //  bt1.addActionListener(new ButtonClickListener());
    	
    	
    //////////////////////////////////////////////////////////////////////////////////////
    	ImageIcon icon = new ImageIcon("images/AMBIANCE.gif","this is a caption");
       
    	//This will create the title you see in the upper left of the window    
        frame.setTitle("Tabbed Pane"); 
       // frame.setSize(900,700); //set size so the user can "see" it
        
        //Here we are creating the object
        JTabbedPane jtp = new JTabbedPane();

        //This creates the template on the windowed application that we will be using
       frame.getContentPane().add(jtp);
       JPanel jp1 = new JPanel();//This will create the first tab
       JPanel jp2 = new JPanel();//This will create the second tab
       JPanel jp3=new JPanel();
       jp1.setLayout(null);
       jp2.setLayout(null);
       jp3.setLayout(null);
       
       
         //This creates a non-editable label, sets what the label will read
        //and adds the label to the first tab
      JLabel label1 = new JLabel();
     
      JButton editEmployee=new JButton("Edit Employee");
      editEmployee.setActionCommand("EDIT");
      JButton salaryDetails=new JButton("Salary Details");
      salaryDetails.setActionCommand("SALARY");
      
      
      editEmployee.setBounds(780, 500, 150, 40);
      salaryDetails.setBounds(420, 500, 150, 40);
     
      
      //Combo box
      frame.add(jyear);
      frame.add(jmonth);
      
      
  
      // label1.setText("This is Tab 1");
      	
      	jp1.add(editEmployee);
      	jp1.add(salaryDetails);
       	//jp1.add(label1);
       	jp1.add(scrollPane);
       	
       	
       	
       	//jpanel 3
       	jp3.add(scrollPane2);
       	jp3.add(jyear);
       	jp3.add(jmonth);
       	
       	jyear.setBounds(400, 10, 100, 20);
       	jyear.setBounds(500, 10, 100, 20);

       //This adds the first and second tab to our tabbed pane object and names it
       jtp.addTab("Employee",icon, jp1);
       jtp.addTab("Reports",icon, jp2);
       jtp.addTab("Monthly report",icon,jp3);
       

        //This creates a new button called "Press" and adds it to the second tab
       JButton test = new JButton("Press");
       jp2.add(test);

        //This is an Action Listener which reacts to clicking on 
        //the test button called "Press"
        ButtonHandler phandler = new ButtonHandler();
       
        editEmployee.addActionListener(phandler);
        salaryDetails.addActionListener(phandler);
        
        frame.setVisible(true); //otherwise you won't "see" it 
    }
    
    //This is the internal class that defines what the above Action Listener
    //will do when the test button is pressed.
    class ButtonHandler implements ActionListener{
           public void actionPerformed(ActionEvent e){
                   //JOptionPane.showMessageDialog(null, "I've been pressed", "What happened?", JOptionPane. INFORMATION_MESSAGE);
        	   String str=e.getActionCommand();
        	   if(str=="ADD")
        	   {
        	   new AddEmployee();
        	   frame.setVisible(false);
        	   
        	   }
        	   
        	    if(str=="EDIT"){
        	       int x=table.getSelectedRow();
        	       String emp=table.getValueAt(x, 1).toString();
        	       x++;
        	       System.out.println("Selected Row "+x);
        		   new EditRecoard(emp);
        		   frame.setVisible(false);
        	   }
        	   
        	    if(str=="SALARY"&&table.getSelectedRowCount()>0){
        	    int serial_no=table.getSelectedRow();
        	    serial_no++; 
        	   // System.out.println("Serial no"+serial_no);
        	   // System.out.println(serial_no);
        	    try (Connection connection = DriverManager.getConnection( url, userid, password );
        	            Statement stmt = connection.createStatement();
        	            ResultSet rs = stmt.executeQuery( "select employee_code from employee_details where sr_no='"+serial_no+"'" )){
        	    	
        	    	if(rs.next()){
        	    		String empcode=rs.getString("employee_code");
        	    		new Salary_Details(empcode);
        	    		frame.setVisible(false);
        	    	}
        	    }
        	    catch (Exception e2) {
					e2.printStackTrace();
				}
        	    
        	    
        		 
        	   }
           }

		
    }
    
    
    
    String findMonth(){
    	
    	
    	
    	return "2015-02-25";
    }

    //example usage
     public static void main (String []args){
    	 new MonthlyReport();
    	 
    }

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

}


