import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class login extends JFrame
{
	    JLabel l_name,l_pass,logo;
	    JTextField t_name;
	    JPasswordField t_pass;     
	    JButton button;
	    Container c;
	    JPanel jpBackGround;
	   
	    handler handle;	 
	    DBConnect db;
	 
	    login()
	    {
	        super("Login form");
	 
	        c=getContentPane();
	        c.setLayout(null);
	        c.setBackground(Color.LIGHT_GRAY);
	 
	        db=new DBConnect();
	        handle =new handler();
	   
	        l_name=new JLabel("Username");
	        l_pass=new JLabel("Password");
	        t_name=new JTextField(10);
	        t_pass=new JPasswordField(10);
	        button=new JButton("Login");
	        button.addActionListener(handle);
	 
	        l_name.setBounds(50, 80, 180, 20);
	        t_name.setBounds(170, 80, 180, 20);
	        l_pass.setBounds(50, 130, 180, 20);
	        t_pass.setBounds(170, 130, 180, 20);
	        button.setBounds(250, 170, 100, 30);
	       
	        c.add(l_name);
	        c.add(t_name);
	        c.add(l_pass);
	        c.add(t_pass);
	        c.add(button);
	        
	     	logo=new JLabel(new ImageIcon("new/logo.png"));
	        logo.setBounds(35, 0, 200, 80);
	        c.add(logo);
	        
	        jpBackGround=new JPanel();
	        jpBackGround.setBackground(Color.GRAY);
	        jpBackGround.setBounds(30, 70, 350, 140);
	        c.add(jpBackGround);
	      
	        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	        int x=dim.width/2-this.getSize().width/2-200;
	        int y=dim.height/2-this.getSize().height/2-150;
	        
	        this.setLocation(x,y);
	        setVisible(true);
	        
	     
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setSize(420,270);
	 
	    }
	    public static void main(String args[])
	    {
	        login login=new login();
	    }
	 
	   
	    class handler implements ActionListener
	    {
	       
	        public void actionPerformed(ActionEvent ae)
	        {
	        
	            if(ae.getSource()==button)
	            {
	                char[] temp_pwd=t_pass.getPassword();
	                String pwd=null;
	                pwd=String.copyValueOf(temp_pwd);
	                System.out.println("Username,Pwd:"+t_name.getText()+","+pwd);
	 
	              
	                if(db.checkLogin(t_name.getText(), pwd))
	                {
	                 
	                  //  JOptionPane.showMessageDialog(null, "You have logged in successfully","Success",JOptionPane.INFORMATION_MESSAGE);
	                	/*  ImageIcon icon = new ImageIcon(login.class.getResource("new/success.jpg"));
	                	JOptionPane.showMessageDialog(null,
	                             new JLabel("SuccessFully Loged In!", icon, JLabel.LEFT),
	                             "Success...!!", JOptionPane.DEFAULT_OPTION);
	                    					 
*/
	                     
	                    //Redirection
	                    new menu(0);
	                    setVisible(false);
	                  
	                    
	                }
	                else
	                {
	                    
	                    //JOptionPane.showMessageDialog(null, "Login failed!","Failed!!",
	                      //                  JOptionPane.ERROR_MESSAGE);
	                    ImageIcon icon = new ImageIcon(login.class.getResource("new/fail.jpg"));
	                	 JOptionPane.showMessageDialog(null,
	                             new JLabel("Login failed !!! Check Username & Password", icon, JLabel.LEFT),
	                             "Failed!!", JOptionPane.DEFAULT_OPTION);
	                    					
	                }
	            }
	        }
	 
	    }
	}