import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import org.w3c.dom.css.Rect;

import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

public class Salary_Details {
	
	ArrayList columnNames;
    ArrayList data ;
    JLabel logo;
    JLabel lemp_name,lemp_name2,lemp_code,lemp_code2,lemp_desig,lemp_desig2,lbank,lbank2,lpan,lpan2; 
    JFrame frame;
    
    JTextField jtxt;
    JTable table;
    String name,designation,emp_code,bank_name,pan_no;
    
	
    public Salary_Details(String code) {
    	
    	
    	
    	emp_code=code;
    	columnNames= new ArrayList();
        data= new ArrayList();
        frame=new JFrame("Salary Details");
        
        
        jtxt=new JTextField();
        
        lemp_name=new JLabel();
        lemp_code=new JLabel();
        lemp_desig=new JLabel();
        lbank=new JLabel();
        lpan=new JLabel();
        lemp_name2=new JLabel("Employee Name :-");
        lemp_code2=new JLabel("Employee Id :-");
        lemp_desig2=new JLabel("Designation :-");
        lbank2=new JLabel("Bank Name :-");
        lpan2=new JLabel("Pan Number :-");
        
        String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
        String userid = "root";
        String password = "yungry";
        String sql = "SELECT * FROM salary_details WHERE employee_code='"+code+"' order by month_of_salary";
        String sql2="SELECT * FROM employee_details WHERE employee_code='"+code+"'";
        
        

        
       
        try (Connection connection = DriverManager.getConnection( url, userid, password );
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery( sql ))
        {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            
            for (int i = 1; i <= columns; i++)
            {
                columnNames.add( md.getColumnName(i) );
            }

            
            while (rs.next())
            {
                ArrayList row = new ArrayList(columns);

                for (int i = 1; i <= columns; i++)
                {
                    row.add( rs.getObject(i) );
                }

                data.add( row );
            }
        }
        catch (SQLException e)
        {
            System.out.println( e.getMessage() );
        }
       
       
        Vector columnNamesVector = new Vector();
        Vector dataVector = new Vector();

        for (int i = 0; i < data.size(); i++)
        {
            ArrayList subArray = (ArrayList)data.get(i);
            Vector subVector = new Vector();
            for (int j = 0; j < subArray.size(); j++)
            {
                subVector.add(subArray.get(j));
            }
            dataVector.add(subVector);
        }

        for (int i = 0; i < columnNames.size(); i++ )
            columnNamesVector.add(columnNames.get(i));

         
        table = new JTable(dataVector, columnNamesVector)
        {
            public Class getColumnClass(int column)
            {
                for (int row = 0; row < getRowCount(); row++)
                {
                    Object o = getValueAt(row, column);

                    if (o != null)
                    {
                        return o.getClass();
                    }
                }
                

                return Object.class;
            }
            
            public boolean isCellEditable(int row, int column){ 
            	
	            return false;  
	        }
            
            
           
        };
        
       
        
       // table.getColumn("Button").setCellRenderer(new ButtonRenderer());
       // table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
        
        
        
        try (Connection connection = DriverManager.getConnection( url, userid, password );
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery( sql2 ))
            {
                
               if(rs.next())
                {
            	   name=rs.getString("employee_name");
            	   designation=rs.getString("designation");
            	   bank_name=rs.getString("bank_name");
            	   pan_no=rs.getString("pan_number");
                }
            }
            catch (SQLException e)
            {
                System.out.println( e.getMessage() );
            }
      

      
     
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        scrollPane.setBounds(50,160,900,400);
        
        
        
        
        
        //frame.add(bt1);
      
        //frame.add( scrollPane );
         
       
       frame.setSize(1024,730);
       frame.setVisible(true);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
     //  bt1.addActionListener(new ButtonClickListener());
    	
    	
    //////////////////////////////////////////////////////////////////////////////////////
    	ImageIcon icon = new ImageIcon("images/AMBIANCE.gif","this is a caption");
       
    	//This will create the title you see in the upper left of the window    
        frame.setTitle("Tabbed Pane"); 
       // frame.setSize(900,700); //set size so the user can "see" it
        
        //Here we are creating the object
        JTabbedPane jtp = new JTabbedPane();

        //This creates the template on the windowed application that we will be using
       frame.getContentPane().add(jtp);
       JPanel jp1 = new JPanel();//This will create the first tab
       jp1.setBackground(Color.LIGHT_GRAY);
       JPanel jp2 = new JPanel();//This will create the second tab
       jp1.setLayout(null);
       jp2.setLayout(null);
       
       
       
         //This creates a non-editable label, sets what the label will read
        //and adds the label to the first tab
      JLabel label1 = new JLabel();
      
      JButton addSalary=new JButton("ADD SALARY DETAILS");
      addSalary.setActionCommand("ADD");
      
      JButton editSalary=new JButton("EDIT SALARY DETAILS");
      editSalary.setActionCommand("EDIT");
      
      JButton employeeList=new JButton("EMPLOYEE LIST");
      employeeList.setActionCommand("LIST");
      
      employeeList.setBounds(770, 580, 150, 40);
      addSalary.setBounds(590, 580, 150, 40);
      editSalary.setBounds(410, 580, 150, 40);
    
      JPanel jpButton=new JPanel();
      jpButton.setBounds(50, 570, 900, 60);
      jpButton.setBackground(Color.DARK_GRAY);
      
      JPanel jpForm=new JPanel();
      jpForm.setBounds(50, 100, 900, 55);
      jpForm.setBackground(Color.GRAY);
     
     
  
      // label1.setText("This is Tab 1");
      	jp1.add(addSalary);
      //	jp1.add(editSalary);
      	jp1.add(employeeList);
      	
       	//jp1.add(label1);
       	jp1.add(scrollPane);
       	jp1.add(lemp_code);
       	jp1.add(lemp_desig);
       	jp1.add(lemp_name);
       	jp1.add(lbank);
       	jp1.add(lpan);
       	
    	jp1.add(lemp_code2);
       	jp1.add(lemp_desig2);
       	jp1.add(lemp_name2);
       	jp1.add(lbank2);
       	jp1.add(lpan2);
       	
       	lemp_code.setText(code);
       	lemp_desig.setText(designation);
       	lemp_name.setText(name);
       	lbank.setText(bank_name);
       	lpan.setText(pan_no);
       	
       	logo=new JLabel(new ImageIcon("new/logo.png"));
        logo.setBounds(50, 0, 200, 100);
        jp1.add(logo);
       	
       	lemp_code2.setBounds(60, 100, 100, 30);
       	lemp_code.setBounds(170, 100, 100, 30);
       	
       	lemp_name2.setBounds(290, 100, 120, 30);
       	lemp_name.setBounds(430, 100, 400, 30);
       	
    	lemp_desig2.setBounds(690, 100, 100, 30);
    	lemp_desig.setBounds(790, 100, 150, 30);
    	
       	lbank2.setBounds(60, 130, 100, 30);
       	lbank.setBounds(170, 130, 400, 30);
       	
       	lpan2.setBounds(290, 130, 100, 30);
       	lpan.setBounds(430, 130, 400, 30);
       	
       	//lemp_name.setBounds(200,30 , 100, 30);
       	
       	
       	
       	
       	

       //This adds the first and second tab to our tabbed pane object and names it
       jtp.addTab("Employee",icon, jp1);
      // jtp.addTab("Reports",icon, jp2);
       

        //This creates a new button called "Press" and adds it to the second tab
       JButton test = new JButton("Press");
       jp2.add(test);

        //This is an Action Listener which reacts to clicking on 
        //the test button called "Press"
        ButtonHandler phandler = new ButtonHandler();
        addSalary.addActionListener(phandler);
        editSalary.addActionListener(phandler);
        employeeList.addActionListener(phandler);
        
       
        jp1.add(jpButton);
        jp1.add(jpForm);
        frame.setVisible(true); //otherwise you won't "see" it 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    //This is the internal class that defines what the above Action Listener
    //will do when the test button is pressed.
    class ButtonHandler implements ActionListener{
           public void actionPerformed(ActionEvent e){
                   //JOptionPane.showMessageDialog(null, "I've been pressed", "What happened?", JOptionPane. INFORMATION_MESSAGE);
        	   String str=e.getActionCommand();
        	   if(str=="ADD")
        	   {
        		   new MasterGrid();
        		   frame.setVisible(false);
        		   int a=table.getRowCount();	//getting last
        		   a--;							//row of table
        		   if(a<0){
        			 //  new AddSalaryDetails(emp_code,0);  
        			 //  frame.setVisible(false);
        		   }
        		   else{
        			   int salid= (int) table.getValueAt(a,0);
              		//   new AddSalaryDetails(emp_code,salid);
                	//   frame.setVisible(false);   
        		   }
        		   
        	   
        	   }
        	   
        	    if(str=="EDIT"){
        	    	//geting Employee code
        	    	int a=table.getSelectedRow();
        	    	if(a<0){
        	    		JOptionPane.showMessageDialog(null, "Please Select Salary from abouve list to Edit !","Alert..!!!",
         	    			   JOptionPane.OK_CANCEL_OPTION);
        	    	}
        	    	else{
       	         		String ecode=(String) table.getValueAt(a,1);
       	         		
       	         	
       	         		a=table.getSelectedRow();
       	         		int salid= (int) table.getValueAt(a,0);
       	         		System.out.println(salid);
       	         		
       	         		String monthselected=(String) table.getValueAt(a,2);
       	         		System.out.println(monthselected);
       	         	
       	         		//new EditSalaryDetails(ecode,salid,monthselected);
       	         		//frame.setVisible(false);
        	    	}
        	   }
        	    
        	    if(str=="LIST"){
         		   
         		   new menu(0);
         		   frame.setVisible(false);
         	   }
        	   
        	    
           }

		
    }

    //example usage
     public static void main (String []args){
    	 new Salary_Details("SK4092");
    	 
    }

}


