import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;


public class ChangeStatusForm extends AbstractFormatter implements ActionListener{

		
		JFrame f1;
		JPanel jp,jpButton;
		JLabel imageLabelLogo,jlempcode,jlempname,jlstatus,dateformat,jlinactiveDate;
		JTextField jtempcode,jtempname;
		JComboBox status_combo;
		JButton jbSave,jbBack;
	
		String employee_code,employee_name,status;
		StringBuffer btn=new StringBuffer("Button");
		JDatePickerImpl datePicker;
		String []statusArr={"Active","Inactive"};
		
		 private String datePattern = "yyyy-MM-dd";
		 private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);
		
		@Override
		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		@Override
		public String valueToString(Object value) throws ParseException {
			 if (value != null) {
		            Calendar cal = (Calendar) value;
		            return dateFormatter.format(cal.getTime());
		        }
		         
		        return "";
		}
		
		public ChangeStatusForm(String empid,String empName)
		{
			
			f1=new JFrame("Change Status");
			f1.setLayout(null);
			f1.getContentPane().setBackground(Color.LIGHT_GRAY);
			jp=new JPanel();
			jpButton=new JPanel();
			dateformat=new JLabel("(YYYY-MM-DD)");
			
			
			UtilDateModel model = new UtilDateModel();
			JDatePanelImpl datePanel = new JDatePanelImpl(model);
			datePicker = new JDatePickerImpl(datePanel, this);
			
			
			
			
			
		
			jlempcode=new JLabel("Employee Code:");
			jlempname=new JLabel("Employee Name:");
			jlstatus=new JLabel("Status");
			jlinactiveDate=new JLabel("Inactive Date");
			
			
			jtempcode=new JTextField(45); jtempcode.setText(empid);
			jtempname=new JTextField(45); jtempname.setText(empName);        			
			status_combo=new JComboBox(statusArr);
			
			
	        jbSave=new JButton("SAVE INFO");
			jbSave.setActionCommand("save");
			
			jbBack=new JButton("BACK");
			jbBack.setActionCommand("back");
		
			
		
			f1.add(jlempcode);
			f1.add(jlempname);
			f1.add(jlstatus);
			f1.add(jlinactiveDate);
			
		
			f1.add(jtempcode);
			f1.add(jtempname);
			f1.add(status_combo);
			
			f1.add(jbSave);
			f1.add(jbBack);
			f1.add(datePicker);
			
			f1.add(jp);
			f1.add(jpButton);
			f1.setSize(1024,730);
			f1.setVisible(true);
			f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			jp.setBounds(220,85,600,500);
			jp.setBackground(Color.GRAY);
			
			jpButton.setBounds(220,610,600,50);
			jpButton.setBackground(Color.DARK_GRAY);

		
			jlempcode.setBounds		(300,140,400,20);
			jlempname.setBounds		(300,180,400,20);
			jlstatus.setBounds		(300, 220, 400, 20);
			jlinactiveDate.setBounds(300, 260, 400, 20);
			
		
			jtempcode.setBounds	(450,140,150,20);
			jtempname.setBounds	(450,180,150,20);
			status_combo.setBounds(450, 220, 150, 20);
			datePicker.setBounds(450,260,150,25);
			
			
			jbSave.setBounds(350,620,100,30);		
			jbSave.addActionListener(this);
			
			jbBack.setBounds(500,620,100,30);		
			jbBack.addActionListener(this);
			

			
			//code for logo & Co.Name
	        imageLabelLogo=new JLabel(new ImageIcon("new/logo.png"));
	        imageLabelLogo.setBounds(230, 0, 200, 100);
	        f1.add(imageLabelLogo);
	       
		
			
		}

		public void actionPerformed(ActionEvent ae)
		{
			if(ae.getActionCommand()=="save"){
				try {
					savedata();
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ae.getActionCommand()=="back"){
				new InactiveEmployee();
				f1.setVisible(false);
			}
		    	
		}

	 
		
		public void savedata() throws ParseException{
			
			String sDate="";
			try{
				
			
			employee_name=jtempname.getText();
	        employee_code=jtempcode.getText();
	        status=status_combo.getSelectedItem().toString();
	        
	        java.util.Date selectedDate = (java.util.Date) datePicker.getModel().getValue();
	       
	  		
	  		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	  		sDate= sdf.format(selectedDate);
	  		
	  		System.out.println("Date is"+sDate);
			
	  		// SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	        // System.out.println(dateFormat.format(dateFormat.parse(date_of_joining)));
	        String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
	        String userid = "root";
	        String password = "yungry";
	        
	        String sql="update employee_details set status='"+status+"',Inactive_date='"+sDate+"' where employee_code='"+employee_code+"'";
	       try{
	    	   Connection con=(Connection) DriverManager.getConnection(url,userid,password);
	    	   Statement stmt = con.createStatement();
	    	   stmt.executeUpdate(sql);
	    	 
		        
	       }
	       catch(Exception e){
	    	   e.printStackTrace();
	    	   
	       }
	        
	        
	        f1.setVisible(false);
	        new InactiveEmployee();
			}
			catch(Exception e)
			{
				  ImageIcon icon = new ImageIcon(login.class.getResource("new/download.jpg"));
	          	JOptionPane.showMessageDialog(null,
	                       new JLabel("Incorrect data please check the data you Entered...!!!\n & fill all the details", icon, JLabel.LEFT),
	                       "Success...!!", JOptionPane.DEFAULT_OPTION);
			}
		}
		
		
		
		public static void main(String[] a){
			new InactiveEmployee();
		}

		
	}

/*	class DateValidator {
	public Date getlowerBoundDate(){
	    String date;
	    Date date1=new Date();
	    @SuppressWarnings("deprecation")
		Integer year=1900+date1.getYear();
	    date="01-Apr-"+year;
	    return new Date(date);
	}
	public Date getupperBoundDate(){
	    String date;
	    Date date1=new Date();
	    Integer year=1900+date1.getYear();
	    date="31-Mar-"+(year+1);//you can set date as you wish over here
	    return new Date(date);
	 }
	

}*/
