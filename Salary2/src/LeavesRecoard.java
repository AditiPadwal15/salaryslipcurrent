
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;


public class LeavesRecoard implements ActionListener{
	
		JFrame frame;
		String url = "jdbc:mysql://localhost:3306/salarySlipDatabase";
	    String userid = "root";
	    String password = "yungry";
	    ArrayList columnNames;
	    ArrayList data ;
	    JTable table;
	    JButton backtoList,masterGrid;
	    JPanel jp,jp2,jpButton;
	    JLabel totalLeaves,totalLeavescount,remainigLeaves,remainigLeavesCount,excessLeaves,excessLeavesCount;
	    JLabel imageLabelLogo; 

	public LeavesRecoard(String emp) {
		
		frame=new JFrame("Leaves Details");
		jp=new JPanel(null);
		jp.setBackground(Color.LIGHT_GRAY);
		jp2=new JPanel(null);
		jp2.setBackground(Color.GRAY);
		jpButton=new JPanel();
		jpButton.setBackground(Color.DARK_GRAY);
		columnNames= new ArrayList();
        data= new ArrayList();
                
		totalLeaves=new JLabel("Number of Total Leaves Taken");
		totalLeavescount=new JLabel();
		
		remainigLeaves=new JLabel("Number of Remaining Leaves");
		remainigLeavesCount=new JLabel("0");
		
		excessLeaves=new JLabel("Number Of Excess Leaves");
		excessLeavesCount=new JLabel("0");
		
		backtoList=new JButton("Employee List");
		backtoList.setActionCommand("BACK");
		backtoList.addActionListener(this);
		
		masterGrid=new JButton("Salary Master");
		masterGrid.setActionCommand("MASTER");
		masterGrid.addActionListener(this);
		
		
		
		int currentYear,previousyear,nextyear,month;   
		currentYear=Calendar.getInstance().get(Calendar.YEAR);
		previousyear=currentYear-1;
		nextyear=currentYear+1;
		month=Calendar.getInstance().get(Calendar.MONTH)+1;
		String sql="";
		if(month>=1&&month<=3){
			sql = "SELECT * FROM leaves_data WHERE employee_code='"+emp+"' AND month_of_salary IN ('"+previousyear+"-APR','"+previousyear+"-MAY','"+previousyear+"-JUN',"
	        		+ "'"+previousyear+"-JUL','"+previousyear+"-AUG','"+previousyear+"-SEP','"+previousyear+"-OCT','"+previousyear+"-NOV','"+previousyear+"-DEC',"
	        		+ "'"+currentYear+"-JAN','"+currentYear+"-FEB','"+currentYear+"-MAR')";
		}
		
		if(month>=4&&month<=12){
			sql = "SELECT * FROM leaves_data WHERE employee_code='"+emp+"' AND month_of_salary IN ('"+currentYear+"-APR','"+currentYear+"-MAY','"+currentYear+"-JUN',"
	        		+ "'"+currentYear+"-JUL','"+currentYear+"-AUG','"+currentYear+"-SEP','"+currentYear+"-OCT','"+currentYear+"-NOV','"+currentYear+"-DEC',"
	        		+ "'"+nextyear+"-JAN','"+nextyear+"-FEB','"+nextyear+"-MAR')";
		}
		
		
        
        
        
        try (Connection connection = DriverManager.getConnection( url, userid, password );
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery( sql ))
        {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            
            for (int i = 1; i <= columns; i++)
            {
                columnNames.add( md.getColumnName(i) );
            }

            
            while (rs.next())
            {
                ArrayList row = new ArrayList(columns);

                for (int i = 1; i <= columns; i++)
                {
                    row.add( rs.getObject(i) );
                }

                data.add( row );
            }
        }
        catch (SQLException e)
        {
            System.out.println( e.getMessage() );
        }

       
        Vector columnNamesVector = new Vector();
        Vector dataVector = new Vector();

        for (int i = 0; i < data.size(); i++)
        {
            ArrayList subArray = (ArrayList)data.get(i);
            Vector subVector = new Vector();
            for (int j = 0; j < subArray.size(); j++)
            {
                subVector.add(subArray.get(j));
            }
            dataVector.add(subVector);
        }

        for (int i = 0; i < columnNames.size(); i++ )
            columnNamesVector.add(columnNames.get(i));

         
        table = new JTable(dataVector, columnNamesVector)
        {
            public Class getColumnClass(int column)
            {
                for (int row = 0; row < getRowCount(); row++)
                {
                    Object o = getValueAt(row, column);

                    if (o != null)
                    {
                        return o.getClass();
                    }
                }
                

                return Object.class;
            }
            
            public boolean isCellEditable(int row, int column){ 
        
	            return false;  
	        }
            
            
           
        };
       // table.getColumn("Button").setCellRenderer(new ButtonRenderer());
       // table.getColumn("Button").setCellEditor(new ButtonEditor(new JCheckBox()));
      

       // bt1.setBounds(200,450,100,40);
     
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane scrollPane = new JScrollPane( table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        
        
        
        
        
        
        
        
        
        
        
        
        int count=table.getRowCount();
        float leavestaken=0;
        for(int i=0;i<count;i++){
        	float currentLeave=Float.parseFloat(table.getValueAt(i, 4).toString());
        	leavestaken=leavestaken+currentLeave;	
        }
        
        float remainigLeave=24-leavestaken;
        totalLeavescount.setText(""+leavestaken);
        
        if(remainigLeave>=0){
        	remainigLeavesCount.setText(""+remainigLeave);
        }
        else{
        	remainigLeave=remainigLeave*-1;
        	excessLeavesCount.setText(""+remainigLeave);
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      //code for logo & Co.Name
        imageLabelLogo=new JLabel(new ImageIcon("new/logo.png"));
        imageLabelLogo.setBounds(50, 0, 200,100);
        frame.add(imageLabelLogo);
        
       
        
		jp.add(scrollPane);
		jp2.add(totalLeaves);
		jp2.add(totalLeavescount);
		jp2.add(remainigLeaves);
		jp2.add(remainigLeavesCount);
		jp2.add(excessLeaves);
		jp2.add(excessLeavesCount);
		jp.add(backtoList);
		jp.add(masterGrid);
		
		scrollPane.setBounds(50, 100, 500, 350);
		
		totalLeaves.setBounds(20, 20, 200, 20); totalLeavescount.setBounds(240, 20, 200, 20);
		remainigLeaves.setBounds(20, 60, 200, 20); remainigLeavesCount.setBounds(240, 60, 200, 20);
		excessLeaves.setBounds(20, 100, 200, 20); excessLeavesCount.setBounds(240, 100, 200, 20);
		
		jp2.setBounds(50, 455, 500, 130);
		jpButton.setBounds(50, 590, 500, 60);
		
		backtoList.setBounds(330, 600, 150, 40);
		masterGrid.setBounds(130, 600, 150, 40);
		
		jp.add(jp2);
		jp.add(jpButton);
        frame.add(jp);
        frame.setSize(630, 730);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        

        
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand()=="BACK"){
			frame.setVisible(false);
			new menu(0);
		}
		
		if(e.getActionCommand()=="MASTER"){
			frame.setVisible(false);
			new MasterGrid();
		}
		
	}

}
